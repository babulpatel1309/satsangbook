package us.gurukul.satsangbooks.screens.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.model.Book;
import com.folioreader.model.sqlite.BookTable;

import java.util.ArrayList;

import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.adapter.BooksAdapter;
import us.gurukul.satsangbooks.screens.listener.RecyclerViewClickListener;

public class FavouriteBookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_favourite_book);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Favourite Books");
        }

        final ArrayList<Book> books = new ArrayList<>(BookTable.getFavBooks());

        if (books.isEmpty()) {
            findViewById(R.id.fav_warning).setVisibility(View.VISIBLE);
        } else {
            final BooksAdapter booksAdapter = new BooksAdapter(this, books);
            RecyclerView rv_fav_books = findViewById(R.id.rv_fav_books);
            rv_fav_books.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
            rv_fav_books.setAdapter(booksAdapter);
            booksAdapter.setListener(new RecyclerViewClickListener() {
                @Override
                public void onItemClick(Book book) {
                    Intent intent = new Intent();
                    intent.putExtra("bookItem", book);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }

                @Override
                public void onItemRemoveFav(int pos) {
                    books.remove(pos);
                    booksAdapter.notifyDataSetChanged();

                    if (books.isEmpty())
                        findViewById(R.id.fav_warning).setVisibility(View.VISIBLE);
                }

                @Override
                public void onItemDelete(int pos) {
                    books.remove(pos);
                    booksAdapter.notifyDataSetChanged();
                    if (books.isEmpty())
                        findViewById(R.id.fav_warning).setVisibility(View.VISIBLE);
                }

                @Override
                public void onExtraItem(String url) {
                    if (!url.startsWith("http://") && !url.startsWith("https://"))
                        url = "http://" + url;
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    } catch (ActivityNotFoundException e) {
                        Log.e("TAG", "Something wrong in url or browser not in device.");
                    }
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
