package com.folioreader;

public class Color {
    private String name;
    private int drawble;

    public Color(String name, int drawble) {
        this.name = name;
        this.drawble = drawble;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawble() {
        return drawble;
    }

    public void setDrawble(int drawble) {
        this.drawble = drawble;
    }
}
