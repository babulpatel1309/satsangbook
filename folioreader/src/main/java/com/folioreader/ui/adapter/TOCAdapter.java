package com.folioreader.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.folioreader.Config;
import com.folioreader.R;
import com.folioreader.model.TOCLinkWrapper;
import com.folioreader.util.MultiLevelExpIndListAdapter;
import com.folioreader.util.UiUtil;

import java.util.ArrayList;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import static com.folioreader.model.sqlite.SeenChapterTable.getSeenChapterIfNotExists;

public class TOCAdapter extends MultiLevelExpIndListAdapter {

    private static final int LEVEL_ONE_PADDING_PIXEL = 15;

    private TOCCallback callback;
    private final Context mContext;
    private String selectedHref;
    private Config mConfig;
    private String mBookTitle;

    public TOCAdapter(Context context, String bTitlte, ArrayList<TOCLinkWrapper> tocLinkWrappers, String selectedHref, Config config) {
        super(tocLinkWrappers);
        mContext = context;
        this.selectedHref = selectedHref;
        this.mConfig = config;
        this.mBookTitle = bTitlte;
    }

    public void setCallback(TOCCallback callback) {
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TOCRowViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_table_of_contents, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TOCRowViewHolder viewHolder = (TOCRowViewHolder) holder;
        TOCLinkWrapper tocLinkWrapper = (TOCLinkWrapper) getItemAt(position);

        if (tocLinkWrapper.getChildren() == null || tocLinkWrapper.getChildren().isEmpty()) {
            viewHolder.children.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.children.setVisibility(View.VISIBLE);
        }
        viewHolder.sectionTitle.setText(tocLinkWrapper.getTocLink().getTitle());

        int id = getSeenChapterIfNotExists(mBookTitle, tocLinkWrapper.getTocLink().getHref());
        if (id != -1) {
            //Log.e("Chapter", "true");
            viewHolder.seen_img.setImageResource(R.drawable.ic_remove_red_eye_black_24dp);
        } else {
            //Log.e("Chapter", "false");
            viewHolder.seen_img.setImageResource(R.drawable.ic_visibility_off_black_24dp);

        }

        if (mConfig.isNightMode()) {
            if (tocLinkWrapper.isGroup()) {
                viewHolder.children.setImageResource(R.drawable.ic_plus_white_24dp);
            } else {
                viewHolder.children.setImageResource(R.drawable.ic_minus_white_24dp);
            }
            UiUtil.setColorIntToDrawable(ContextCompat.getColor(mContext, R.color.app_gray), viewHolder.children.getDrawable());
            UiUtil.setColorIntToDrawable(ContextCompat.getColor(mContext, R.color.app_gray), viewHolder.seen_img.getDrawable());
        } else {
            if (tocLinkWrapper.isGroup()) {
                viewHolder.children.setImageResource(R.drawable.ic_plus_black_24dp);
            } else {
                viewHolder.children.setImageResource(R.drawable.ic_minus_black_24dp);
            }
            //UiUtil.setColorIntToDrawable(ContextCompat.getColor(mContext, R.color.app_gray), viewHolder.children.getDrawable());
        }

        int leftPadding = getPaddingPixels(mContext, LEVEL_ONE_PADDING_PIXEL) * (tocLinkWrapper.getIndentation());
        viewHolder.view.setPadding(leftPadding, 0, 0, 0);

        // set color to each indentation level
        if (tocLinkWrapper.getIndentation() == 0) {
            viewHolder.view.setBackgroundColor(Color.WHITE);
            viewHolder.sectionTitle.setTextColor(Color.BLACK);
        } else if (tocLinkWrapper.getIndentation() == 1) {
            viewHolder.view.setBackgroundColor(Color.parseColor("#f7f7f7"));
            viewHolder.sectionTitle.setTextColor(Color.BLACK);
        } else if (tocLinkWrapper.getIndentation() == 2) {
            viewHolder.view.setBackgroundColor(Color.parseColor("#b3b3b3"));
            viewHolder.sectionTitle.setTextColor(Color.WHITE);
        } else if (tocLinkWrapper.getIndentation() == 3) {
            viewHolder.view.setBackgroundColor(Color.parseColor("#f7f7f7"));
            viewHolder.sectionTitle.setTextColor(Color.BLACK);
        }

        if (tocLinkWrapper.getChildren() == null || tocLinkWrapper.getChildren().isEmpty()) {
            viewHolder.children.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.children.setVisibility(View.VISIBLE);
        }

        if (mConfig.isNightMode()) {
            viewHolder.container.setBackgroundColor(ContextCompat.getColor(mContext, R.color.night));
            //viewHolder.children.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
            viewHolder.sectionTitle.setTextColor(ContextCompat.getColor(mContext, R.color.app_gray));
        } else {
            //viewHolder.container.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
            viewHolder.container.setBackgroundResource(R.drawable.rounded_bg_shape);
            //viewHolder.children.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
            viewHolder.sectionTitle.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }
        if (tocLinkWrapper.getTocLink().getHref().equals(selectedHref)) {
            viewHolder.sectionTitle.setTextColor(mConfig.getThemeColor());
        }
    }

    public interface TOCCallback {
        void onTocClicked(int position);

        void onExpanded(int position);
    }

    public class TOCRowViewHolder extends RecyclerView.ViewHolder {
        public ImageView children;
        TextView sectionTitle;
        private RelativeLayout container;
        private View view;
        public ImageView seen_img;

        TOCRowViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            children = itemView.findViewById(R.id.children);
            seen_img = itemView.findViewById(R.id.seen_img);
            container = itemView.findViewById(R.id.container);
            children.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null) callback.onExpanded(getAdapterPosition());
                }
            });

            sectionTitle = itemView.findViewById(R.id.section_title);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null) callback.onTocClicked(getAdapterPosition());
                }
            });
        }
    }

    private static int getPaddingPixels(Context context, int dpValue) {
        // Get the screen's density scale
        final float scale = context.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (dpValue * scale + 0.5f);
    }
}
