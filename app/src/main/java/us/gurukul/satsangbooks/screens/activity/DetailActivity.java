package us.gurukul.satsangbooks.screens.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.downloader.PRDownloader;
import com.downloader.Status;
import com.folioreader.Config;
import com.folioreader.Constants;
import com.folioreader.FolioReader;
import com.folioreader.model.Book;
import com.folioreader.model.locators.ReadLocator;
import com.folioreader.model.sqlite.BookTable;
import com.folioreader.model.sqlite.DbAdapter;
import com.folioreader.model.sqlite.ReadLocatorTable;
import com.folioreader.util.AppUtil;
import com.folioreader.util.ReadLocatorListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.readium.r2.shared.Locations;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import us.gurukul.satsangbooks.screens.Common;
import us.gurukul.satsangbooks.screens.NetworkHelper;
import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.service.MyDownloadService;

import static us.gurukul.satsangbooks.screens.Common.fav_fileDir;
import static us.gurukul.satsangbooks.screens.Common.fileDir;

public class DetailActivity extends AppCompatActivity implements ReadLocatorListener, FolioReader.OnClosedListener, EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks {

    private static final String LOG_TAG = DetailActivity.class.getSimpleName();
    public static int REQ_HOME = 100;
    private Book item;
    private ProgressBar progressBar;
    private Button btn_download;
    private FolioReader folioReader;
    private String bookFileName;
    private Button btn_update;
    private int downloadIdOne = 0;
    private IntentFilter mIntentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_detail);

        //
        DbAdapter.initialize(this);

        //
        PRDownloader.initialize(getApplicationContext());

        folioReader = FolioReader.get()
                .setReadLocatorListener(this)
                .setOnClosedListener(this);

        if (getIntent().getBooleanExtra("isBookmark", false)) {
            bookFileName = getIntent().getStringExtra("book_file");
            int isKavya = getIntent().getIntExtra("book_isKavya", 0);
            openEpubBook(new File(fileDir, bookFileName), true, isKavya);
            finish();
            return;
        }

        if (getIntent().getBooleanExtra("isNotify", false)) {
            item = getIntent().getParcelableExtra("bookItem");
        } else {
            try {
                int itemId = Integer.parseInt(getIntent().getStringExtra("bookItemId"));
                Log.e(LOG_TAG, "bookItemId-> " + itemId);
                item = BookTable.getBook(itemId);
            } catch (NumberFormatException e) {
                AlertDialog.Builder a1 = new AlertDialog.Builder(this);
                a1.setCancelable(false);
                a1.setTitle("Warning Book!!!");
                a1.setMessage("Book is Empty or Invalid book");
                a1.setPositiveButton("close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int button1) {
                                finish();
                            }
                        });

                // Showing Alert Message
                AlertDialog alertDialog = a1.create();
                if (!isFinishing())
                    alertDialog.show();
            }
        }

        if (getIntent().getBooleanExtra("isNotify", false)) {
            String query = "SELECT * FROM " + BookTable.TABLE_NAME + " WHERE " + BookTable.COL_BOOK_ID + " = '" + item.getId() + "'";
            Cursor c = DbAdapter.getIdForBookData(query);
            String id = "";
            String update_date = "";
            String book_update_date = "";
            int isDownloaded = 0;
            int isFav = 0;
            while (c.moveToNext()) {
                id = c.getString(c.getColumnIndex(BookTable.COL_BOOK_ID));
                update_date = c.getString(c.getColumnIndex(BookTable.COL_UPDATED_AT));
                book_update_date = c.getString(c.getColumnIndex(BookTable.COL_BOOK_UPDATED_AT));
                isDownloaded = c.getInt(c.getColumnIndex(BookTable.COL_DOWNLOADED));
                isFav = c.getInt(c.getColumnIndex(BookTable.COL_FAV));
            }
            if (id.equals("")) {
                DbAdapter.saveBook(BookTable.getBookContentValues(item));
            } else {
                if (!item.getBook_updated_at().equals(book_update_date)) {
                    item.setIs_update_book(1);
                } else
                    item.setIs_update_book(0);
                item.setIs_downloaded(isDownloaded);
                item.setIs_fav(isFav);
                if (!item.getUpdated_at().equals(update_date))
                    DbAdapter.updateBook(BookTable.getBookContentValues(item), id);
            }
        }

        bookFileName = item.getBooks_file();

        initActionBar(item.getTitle());
        initControll(item);

    }

    private void initActionBar(String title) {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle(title);
        }
    }

    private void initControll(final Book item) {
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(MyDownloadService.PROGRESS);
        mIntentFilter.addAction(MyDownloadService.RESULT);
        mIntentFilter.addAction(MyDownloadService.FAILED);

        btn_update = findViewById(R.id.btn_update_book);
        if (item.getIs_update_book() == 1 && item.getIs_downloaded() == 1) {
            btn_update.setVisibility(View.VISIBLE);
            btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (NetworkHelper.isOnline(DetailActivity.this)) {
                        bookUpdatedAlert(item);
                    } else
                        Toast.makeText(getApplicationContext(), "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            });
        }

        ((TextView) findViewById(R.id.wrier_name)).setText(item.getAuthor());

        WebView webView = findViewById(R.id.webView);
        String data = item.getDescription().replace("\n", "<br>");
        String htmlData = String.format(getString(R.string.details), data);
        webView.loadData(htmlData, "text/html", "UTF-8");

        if (!fileDir.exists())
            fileDir.mkdirs();

        isPermissionGranted();

        progressBar = findViewById(R.id.progressBar);

        /*Glide.with(this).load(imgPath + "320x420/" + item.getFront_img())
                .into((ImageView) findViewById(R.id.book_image));*/

        Picasso.get()
                .load(Common.imgPath + "320x420/" + item.getFront_img())
                .placeholder(R.drawable.white_bg) // can also be a drawable
                .into((ImageView) findViewById(R.id.book_image), new Callback() {
                    @Override
                    public void onSuccess() {
                        // once the image is loaded, load the next image
                        Log.e("TAG", "onSuccess");
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("TAG", "onError->" + e.getMessage());
                        Picasso.get()
                                .load(Common.imgPath + "320x420/" + item.getFront_img())
                                .placeholder(R.drawable.white_bg) // but don't clear the imageview or set a placeholder; just leave the previous image in until the new one is ready
                                .into((ImageView) findViewById(R.id.book_image));
                    }
                });

        btn_download = findViewById(R.id.btn_download_read);
        if (item.getIs_downloaded() == 1)
            btn_download.setText(R.string.label_read);
        else
            btn_download.setText(R.string.label_download);

        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getIs_downloaded() == 1) {
                    // call to audio View
                    Log.d("openEpubBook", "-> 1");
                    findViewById(R.id.openProgressBar).setVisibility(View.VISIBLE);
                    btn_download.setEnabled(false);
                    openEpubBook(new File(fileDir, bookFileName), false, item.getIs_book_kavya());
                    Log.d("openEpubBook", "-> 2");
                } else {
                    if (NetworkHelper.isOnline(DetailActivity.this)) {
                        if (fileDir.exists()) {
                            if (PRDownloader.getStatus(downloadIdOne) == Status.RUNNING) {
                                Toast.makeText(getApplicationContext(), "Already Book is downloading, please wait sometime.", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            btn_download.setEnabled(false);
                            btn_download.setText(R.string.label_downloading);
                            progressBar.setIndeterminate(true);
                            progressBar.setVisibility(View.VISIBLE);
                            downloadFile();
                        } else {
                            Log.e("File", "Dir dose not exists");
                            isPermissionGranted();
                            if (fileDir.mkdir())
                                Log.e("File", "Dir mkdir");
                            else
                                Log.e("File", "Dir dose not mkdir");
                        }
                    } else
                        Toast.makeText(getApplicationContext(), "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void bookUpdatedAlert(final Book book) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Do you want to update this Book?");
        builder.setMessage("if yes, your favourite, bookmark and highlight data will be deleted.");

        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Then Re-download audio
                if (NetworkHelper.isOnline(DetailActivity.this)) {
                    if (fileDir.exists()) {
                        File file = new File(fileDir, book.getBooks_file());
                        File src = new File(fav_fileDir, book.getBooks_file());
                        if (src.exists()) {
                            if (src.delete())
                                Log.e("TAG", "deleted file");
                            else
                                Log.e("TAG", "can not delete file");
                        }

                        if (file.exists()) {
                            if (file.delete())
                                Log.e("TAG", "file deleted");
                            else
                                Log.e("TAG", "file not deleted");
                        }

                        DbAdapter.removeAllBookmarkChapter(book.getBooks_file());
                        DbAdapter.removeAllSeenChapter(book.getBooks_file());
                        DbAdapter.removeAllReadLocator(book.getBooks_file());
                        //For highlight
                        String fileName = file.getName();
                        int pos = fileName.lastIndexOf(".");
                        if (pos > 0 && pos < (fileName.length() - 1)) { // If '.' is not the first or last character.
                            fileName = fileName.substring(0, pos);
                        }
                        DbAdapter.removeAllHighLight(fileName);

                        btn_update.setVisibility(View.INVISIBLE);
                        btn_download.setEnabled(false);
                        btn_download.setText(R.string.label_downloading);
                        progressBar.setIndeterminate(true);
                        progressBar.setVisibility(View.VISIBLE);
                        downloadFile();
                    } else {
                        Log.e("File", "Dir dose not exists");
                        isPermissionGranted();
                        if (fileDir.mkdir())
                            Log.e("File", "Dir mkdir");
                        else
                            Log.e("File", "Dir dose not mkdir");
                    }
                } else
                    Toast.makeText(getApplicationContext(), "No Internet Connection.", Toast.LENGTH_SHORT).show();

            }
        });

        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        Dialog sortDialog = builder.create();
        sortDialog.show();
    }

    private boolean hasStoragePermission() {
        return EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private void isPermissionGranted() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            Log.e("Permission", "All granted");
        } else {
            // Do not have permissions, request them now
            Log.e("Permission", "Don't All granted");
            EasyPermissions.requestPermissions(this, getString(R.string.app_name) + " " + getString(R.string.rationale_storage),
                    111, perms);
        }
    }

    private void openEpubBook(File file, boolean b, int isKavya) {
        //Constants.b_file_name = file;
        ReadLocator readPosition = getLastReadPosition(file.getName());
        Log.e("DetailActivity", "-> ReadPosition = " + readPosition.toJson());
        Log.d("openEpubBook", "-> 3");
        Config config = AppUtil.getSavedConfig(getApplicationContext());
        if (config == null)
            config = new Config();
        config.setAllowedDirection(Config.AllowedDirection.VERTICAL_AND_HORIZONTAL);
        Log.d("openEpubBook", "-> 4");
        if (b) {
            String href = getIntent().getStringExtra("book_href");
            readPosition = new ReadLocator("", href, 0, new Locations());
        }
        config.setIs_kavya(isKavya);
        folioReader.setReadLocator(readPosition)
                .setConfig(config, true)
                .openBook(file.getPath(), b);
        Log.d("openEpubBook", "-> 6");
    }

    private ReadLocator getLastReadPosition(String bookFile) {
        String jsonString = loadAssetTextAsString("Locators/LastReadLocators/last_read_locator_1.json");
        String string = ReadLocatorTable.getReadLocator(bookFile);
        if (string.equals(""))
            return ReadLocator.fromJson(jsonString);
        else
            return ReadLocator.fromJson(string);
    }

    private String loadAssetTextAsString(String name) {
        BufferedReader in = null;
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = getAssets().open(name);
            in = new BufferedReader(new InputStreamReader(is));

            String str;
            boolean isFirst = true;
            while ((str = in.readLine()) != null) {
                if (isFirst)
                    isFirst = false;
                else
                    buf.append('\n');
                buf.append(str);
            }
            return buf.toString();
        } catch (IOException e) {
            Log.e("HomeActivity", "Error opening asset " + name);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e("HomeActivity", "Error closing asset " + name);
                }
            }
        }
        return null;
    }

    private void downloadFile() {
        try {
            Intent myServiceIntent = new Intent(this, MyDownloadService.class);
            myServiceIntent.putExtra("bookItem", item);
            startService(myServiceIntent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.e("Permission", "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Log.e("Permission", "onPermissionsDenied:" + requestCode + ":" + perms.size());
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            Log.e("Permission", "onPermissionsDenied: " + perms.get(0));
            new AppSettingsDialog.Builder(this)
                    .setRationale(getString(R.string.app_name) + " " + getString(R.string.rationale_storage)).build().show();
        }
    }

    @Override
    public void onRationaleAccepted(int requestCode) {
        Log.e("Permission", "onRationaleAccepted:" + requestCode);
    }

    @Override
    public void onRationaleDenied(int requestCode) {
        Log.e("Permission", "onRationaleDenied:" + requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            String yes = getString(R.string.yes);
            String no = getString(R.string.no);

            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(this,
                    getString(R.string.returned_from_app_settings_to_activity, hasStoragePermission() ? yes : no),
                    Toast.LENGTH_SHORT).show();
        } else if (resultCode == RESULT_OK && requestCode == REQ_HOME) {

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.isHome) {//goto home to back from activity
            Constants.isHome = false;
            finish();
        }
        Log.e(LOG_TAG, "-> onResume -> ");
        registerReceiver(receiver, mIntentFilter);
        findViewById(R.id.openProgressBar).setVisibility(View.INVISIBLE);
        btn_download.setEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(LOG_TAG, "-> onPause -> ");
        unregisterReceiver(receiver);
        //findViewById(R.id.openProgressBar).setVisibility(View.INVISIBLE);
        //btn_download.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(LOG_TAG, "-> onDestroy -> ");
        //Constants.b_file_name = null;
        //unregisterReceiver(myBroadCastReceiver);
        //unregisterReceiver(receiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void saveReadLocator(ReadLocator readLocator) {
        Log.e(LOG_TAG, "-> saveReadLocator -> " + readLocator.toJson());
        ReadLocatorTable.saveReadLocatorIfNotExists(bookFileName, readLocator.toJson());
    }

    @Override
    public void onFolioReaderClosed() {

    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            //Log.d("Download", "receiver-> 1");
            if (bundle != null) {
                //Log.d("Download", "receiver-> 2");
                downloadIdOne = bundle.getInt(MyDownloadService.ID);
                String file = bundle.getString(MyDownloadService.FILENAME);
                if (file != null && file.equals(item.getBooks_file())) {
                    //Log.d("Download", "receiver-> 3");
                    if (intent.getAction().equals(MyDownloadService.PROGRESS)) {
                        //Log.d("Download", "Progress " + intent.getIntExtra(MyDownloadService.PROGRESS, 0));
                        if (Status.RUNNING == PRDownloader.getStatus(downloadIdOne)) {
                            int progress = intent.getIntExtra(MyDownloadService.PROGRESS, 0);
                            if (progress > 0) {
                                progressBar.setVisibility(View.VISIBLE);
                                btn_download.setText(R.string.label_downloading);
                                progressBar.setIndeterminate(false);
                                progressBar.setProgress(progress);
                            }
                        }
                    } else if (intent.getAction().equals(MyDownloadService.RESULT)) {
                        Log.d("Download", "Completed");
                        btn_download.setEnabled(true);
                        progressBar.setVisibility(View.INVISIBLE);
                        btn_download.setText(R.string.label_read);
                        item.setIs_downloaded(1);
                    } else if (intent.getAction().equals(MyDownloadService.FAILED)) {
                        Log.d("DetailActivity", "Download->  Failed");
                        btn_download.setEnabled(true);
                        btn_download.setText(R.string.label_download);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }
    };

}
