package us.gurukul.satsangbooks.screens.listener;

import com.folioreader.model.Book;

public interface RecyclerViewClickListener {
    void onItemClick(Book book);

    void onItemRemoveFav(int pos);

    void onItemDelete(int pos);

    void onExtraItem(String url);
}