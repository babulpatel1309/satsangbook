package com.folioreader.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.folioreader.Color;
import com.folioreader.Config;
import com.folioreader.R;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ViewHolder> {

    private ArrayList<Color> mColors;
    private ColorAdapterCallback adapterCallback;
    private Config config;

    public ColorAdapter(Config config, ArrayList<Color> colors, ColorAdapterCallback callback) {
        mColors = colors;
        adapterCallback = callback;
        this.config = config;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_color, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        Color item = mColors.get(i);
        viewHolder.mSelected.setVisibility(i == config.getFontColor() ? View.VISIBLE : View.INVISIBLE);

        viewHolder.mName.setBackgroundResource(item.getDrawble());
        viewHolder.mName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterCallback.onItemClick(i);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mColors != null ? mColors.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ViewGroup mName;
        private ImageView mSelected;

        public ViewHolder(View v) {
            super(v);
            mName = v.findViewById(R.id.name);
            mSelected = v.findViewById(R.id.selected);
        }
    }

    public interface ColorAdapterCallback {
        void onItemClick(int pos);
    }
}
