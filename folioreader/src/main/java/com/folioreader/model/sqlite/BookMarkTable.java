package com.folioreader.model.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.folioreader.model.BookmarkImpl;

import java.util.ArrayList;

public class BookMarkTable {
    public static final String TABLE_NAME = "bookmark_table";
    public static final String ID = "_id";
    public static final String COL_BOOK_ID = "bookId";
    public static final String COL_BOOK_FILE = "bookFile";
    public static final String COL_HREF = "href";
    public static final String COL_VALUE = "value";
    public static final String COL_IS_KAVYA = "isKavya";

    public static final String SQL_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " + ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT" + ","
            + COL_BOOK_ID + " TEXT" + ","
            + COL_BOOK_FILE + " TEXT" + ","
            + COL_HREF + " TEXT" + ","
            + COL_VALUE + " TEXT" + ","
            + COL_IS_KAVYA + " INTEGER" + ")";

    public static ContentValues getHighlightContentValues(BookmarkImpl readPosition) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_BOOK_ID, readPosition.getBookId());
        contentValues.put(COL_BOOK_FILE, readPosition.getBookFile());
        contentValues.put(COL_HREF, readPosition.getChapterHref());
        contentValues.put(COL_VALUE, readPosition.getValue());
        contentValues.put(COL_IS_KAVYA, readPosition.getIsKavya());
        return contentValues;
    }

    public static void saveBookmarkIfNotExists(BookmarkImpl readPosition) {
        String query = "SELECT " + ID + " FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" + readPosition.getBookId() + "'" + " and " + COL_HREF + " = '" + readPosition.getChapterHref() + "'";
        int id = DbAdapter.getIdForQuery(query);
        if (id == -1) {
            DbAdapter.saveBookmarkChapter(getHighlightContentValues(readPosition));
        } else {
            DbAdapter.removeBookmarkChapter(getHighlightContentValues(readPosition), String.valueOf(id));
        }
    }

    public static int getBookmarkIfNotExists(BookmarkImpl readPosition) {
        String query = "SELECT " + ID + " FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" + readPosition.getBookId() + "'" + " and " + COL_HREF + " = '" + readPosition.getChapterHref() + "'";
        return DbAdapter.getIdForQuery(query);//if not exist return -1, otherwise id number
    }

    public static ArrayList<BookmarkImpl> getAllBookmark(String s) {
        String query = "SELECT * FROM " + TABLE_NAME;
        if (!s.isEmpty())
            query += " where " + COL_BOOK_ID + " = '" + s + "'";
        Log.e("Query", query);
        Cursor c = DbAdapter.getAllBookmarkChapter(query);
        ArrayList<BookmarkImpl> rangyList = new ArrayList<>();
        while (c.moveToNext()) {
            rangyList.add(new BookmarkImpl(
                    c.getString(c.getColumnIndex(COL_BOOK_ID)),
                    c.getString(c.getColumnIndex(COL_BOOK_FILE)),
                    c.getString(c.getColumnIndex(COL_HREF)),
                    c.getString(c.getColumnIndex(COL_VALUE)),
                    c.getInt(c.getColumnIndex(COL_IS_KAVYA)))
            );
        }
        c.close();
        return rangyList;
    }


}
