package com.folioreader.service

interface SongListener {
    fun onSongPrepared(boolean: Boolean)
    fun onSongPlayPrev()
    fun onSongPlayNext()

    fun onSongPlayPause()

    fun onSongFailed()
}
