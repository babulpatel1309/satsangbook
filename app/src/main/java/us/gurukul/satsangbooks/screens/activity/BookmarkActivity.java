package us.gurukul.satsangbooks.screens.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.model.Book;
import com.folioreader.model.BookmarkImpl;
import com.folioreader.model.sqlite.BookMarkTable;

import java.util.ArrayList;

import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.adapter.BookmarkAdapter;
import us.gurukul.satsangbooks.screens.listener.RecyclerViewClickListener;

public class BookmarkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_bookmark);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Bookmark List");
        }

        //RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rv_bookmark);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        ArrayList<BookmarkImpl> bookmarks = BookMarkTable.getAllBookmark("");
        if (bookmarks.isEmpty()) {
            findViewById(R.id.bookmark_warning).setVisibility(View.VISIBLE);
        } else
            findViewById(R.id.bookmark_warning).setVisibility(View.INVISIBLE);

        final BookmarkAdapter bookmarkAdapter = new BookmarkAdapter(this, bookmarks);
        recyclerView.setAdapter(bookmarkAdapter);
        bookmarkAdapter.setListener(new RecyclerViewClickListener() {
            @Override
            public void onItemClick(Book book) {

            }

            @Override
            public void onItemRemoveFav(int pos) {
                //For Go to chapter of bookmark
                Intent intent = new Intent();
                intent.putExtra("bookmark", bookmarkAdapter.getItem(pos));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }

            @Override
            public void onItemDelete(int pos) {
                BookMarkTable.saveBookmarkIfNotExists(bookmarkAdapter.getItem(pos));//Save book mark otherwise remove it
                bookmarkAdapter.remove(pos);
                if (bookmarkAdapter.getItemCount() == 0)
                    findViewById(R.id.bookmark_warning).setVisibility(View.VISIBLE);
            }

            @Override
            public void onExtraItem(String url) {

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
