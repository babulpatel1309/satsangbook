package com.folioreader.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;

import com.folioreader.R;
import com.folioreader.ui.activity.FolioActivity;

import java.io.IOException;

import androidx.core.app.NotificationCompat;

public class MusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    //notification id
    private static final int NOTIFY_ID = 101;
    private int bufferPercent = 100;
    private final IBinder musicBind = new MusicBinder();
    private MediaPlayer player;
    private String songTitle = "";
    private String songArtist = "";
    private String songUrl = "";
    private SongListener mCallback;
    private boolean b = false;
    private boolean isPing = false;
    private boolean isNotifyPlay = false;

    public static final String NOTIFY_PLAY_TOGGLE = "NOTIFY_PLAY_TOGGLE";
    public static final String NOTIFY_PREVIOUS = "NOTIFY_PREVIOUS";
    public static final String NOTIFY_NEXT = "NOTIFY_NEXT";
    public static final String NOTIFY_DELETE = "NOTIFY_DELETE";
    private RemoteViews mContentViewBig, mContentViewSmall;
    private NotificationManager mNotificationManager;

    private static final String CHANNEL_ID = "com.shreejiapp.kirtanbhakti.MUSIC_CHANNEL_ID";
    private PhoneStateListener mPhoneListener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String incomingNumber) {
            try {
                switch (state) {
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (player.isPlaying()) {
                            pausePlayer();
                            b = true;
                        } else {
                            b = false;
                        }
                        Log.d("MUSIC PLAYER", "CALL_STATE_RINGING-> " + b);
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        if (player.isPlaying()) {
                            pausePlayer();
                            b = true;
                        } /*else
                            b = false;*/
                        Log.d("MUSIC PLAYER", "CALL_STATE_OFFHOOK-> " + b);
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        if (b)
                            playPlayer();

                        Log.d("MUSIC PLAYER", "CALL_STATE_IDLE-> " + b);
                        break;
                    default:
                }
            } catch (Exception ignored) {

            }
        }
    };


    @Override
    public void onCreate() {
        super.onCreate();
        player = new MediaPlayer();
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);

        initMusicPlayer();
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        assert telephonyManager != null;
        telephonyManager.listen(mPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (NOTIFY_PLAY_TOGGLE.equals(action)) {
                if (player.isPlaying()) {
                    player.pause();
                } else {
                    player.start();
                }
            }
        }
        return START_NOT_STICKY;
    }

    //Activity will bind to service
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }


    private void initMusicPlayer() {
        //set player properties
        player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //set listeners
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
        player.setOnPreparedListener(this);
        player.setOnBufferingUpdateListener(this);
    }

    public void setCallbacks(SongListener callbacks) {
        mCallback = callbacks;
    }

    public void setPlayData(String title, String artist, String url) {
        Log.d("MUSIC PLAYER", "setPlayData-" + true);
        songTitle = title;
        songArtist = artist;
        songUrl = url;
    }

    public void setSongUrl(String url){
        songUrl = url;
    }

    //play a song
    public void playSong() {
        if (mCallback != null) {
            mCallback.onSongPrepared(true);
        }

        //Log.d("MUSIC PLAYER", "playSong-" + true);
        try {
            player.reset();
            //set the data source using uri
            player.setDataSource(getApplicationContext(), Uri.parse(songUrl));
            player.prepareAsync();
        } catch (IllegalArgumentException e) {
            Log.e("MUSIC SERVICE", "IllegalArgumentException- ", e);
        } catch (IllegalStateException e) {
            Log.e("MUSIC SERVICE", "IllegalStateException- ", e);
        } catch (SecurityException e) {
            Log.e("MUSIC SERVICE", "SecurityException- ", e);
        } catch (IOException e) {
            Log.e("MUSIC SERVICE", "IOException- ", e);
        } catch (Exception e) {
            Log.e("MUSIC SERVICE", "Exception- ", e);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (mp != null) {
            mp.reset();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d("MUSIC PLAYER", "Playback Error " + what);
        if (mCallback != null) {
            mCallback.onSongFailed();
        }
        mp.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d("MUSIC PLAYER", "onPrepared-" + isPing);
        if (isPing)
            mp.start();

        if (mCallback != null) {
            mCallback.onSongPrepared(false);
        }

        showNotification();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.d("MUSIC PLAYER", "bufferPercent-" + percent);
        bufferPercent = percent;
    }

    @Override
    public void onDestroy() {
        relesePlayer();
        stopForeground(true);
        Log.e("Music", "Destroy1");
    }

    public void relesePlayer() {
        player.stop();
        player.release();
    }

    //Playback Methods
    public void playPlayer() {
        player.start();
        showNotification();
    }

    public void pausePlayer() {
        player.pause();
        showNotification();
    }

    public void pausePlayerPlayingNext() {
        player.pause();
    }

    public int getPosn() {
        return player.getCurrentPosition();
    }

    public int getDur() {
        return player.getDuration();
    }

    public boolean isPng() {
        try {
            if (player != null) {
                return player.isPlaying();
            }
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean getWasPlaying() {
        return isPing;
    }

    public void setWasPlaying(boolean b) {
        isPing = b;
    }

    public void seek(int posn) {
        player.seekTo(posn);
    }

    //skip to previous track
    public void playPrev() {
        if (mCallback != null) {
            mCallback.onSongPlayPrev();
        }
    }

    //skip to next
    public void playNext() {
        if (mCallback != null) {
            mCallback.onSongPlayNext();
        }
    }

    public int getBuffered() {
        return bufferPercent;
    }

    //---------------
    public void showNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (mNotificationManager.getNotificationChannel(CHANNEL_ID) == null) {
                NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_LOW);
                notificationChannel.setDescription(getString(R.string.app_name));
                notificationChannel.setLightColor(Color.TRANSPARENT);
                notificationChannel.enableVibration(false);
                notificationChannel.setShowBadge(false);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
        }

        Intent notificationIntent = new Intent(getApplicationContext(), FolioActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);


        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentIntent(pendingIntent)
                //.setColor(0)
                .setSmallIcon(R.mipmap.ic_launcher)  // the status icon
                .setCustomContentView(getSmallContentView())
                .setCustomBigContentView(getBigContentView())
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(NotificationCompat.PRIORITY_LOW);

        startForeground(NOTIFY_ID, notification.build());
    }

    private RemoteViews getSmallContentView() {
        if (mContentViewSmall == null) {
            mContentViewSmall = new RemoteViews(getPackageName(), R.layout.remote_view_music_player_small);
            setUpRemoteView(mContentViewSmall);
        }
        updateRemoteViews(mContentViewSmall);
        return mContentViewSmall;
    }

    private RemoteViews getBigContentView() {
        if (mContentViewBig == null) {
            mContentViewBig = new RemoteViews(getPackageName(), R.layout.remote_view_music_player);
            setUpRemoteView(mContentViewBig);
        }
        updateRemoteViews(mContentViewBig);
        return mContentViewBig;
    }

    private void setUpRemoteView(RemoteViews remoteView) {
        remoteView.setOnClickPendingIntent(R.id.button_play_toggle, getPendingIntent(NOTIFY_PLAY_TOGGLE));
        remoteView.setOnClickPendingIntent(R.id.button_play_next, getPendingIntent(NOTIFY_NEXT));
        remoteView.setOnClickPendingIntent(R.id.button_play_last, getPendingIntent(NOTIFY_PREVIOUS));
        remoteView.setOnClickPendingIntent(R.id.button_close, getPendingIntent(NOTIFY_DELETE));
    }

    private void updateRemoteViews(RemoteViews remoteView) {
        remoteView.setTextViewText(R.id.text_view_name, songTitle);
        remoteView.setTextViewText(R.id.text_view_artist, songArtist);

        if (isNotifyPlay) {
            remoteView.setImageViewResource(R.id.image_view_play_toggle, R.drawable.ic_remote_view_pause);
        } else {
            try {
                remoteView.setImageViewResource(R.id.image_view_play_toggle, player.isPlaying()
                        ? R.drawable.ic_remote_view_pause : R.drawable.ic_remote_view_play);
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }
        }
    }


    //From Notification Play pause
    public void setPlayPause() {
        if (FolioActivity.Companion.isLoading()) {
            isNotifyPlay = true;
            showNotification();
            playSong();
            return;
        }
        isNotifyPlay = false;

        if (player.isPlaying())
            pausePlayer();
        else
            playPlayer();

        if (mCallback != null)
            mCallback.onSongPlayPause();
    }

    public void stopForegroundNotification() {
        mNotificationManager.cancel(NOTIFY_ID);
        stopForeground(true);
        Log.e("Music", "Destroy2");
    }

    public void stopPlayer() {
        player.stop();
    }

    // PendingIntent
    private PendingIntent getPendingIntent(String action) {
        return PendingIntent.getBroadcast(this, 0, new Intent(this, NotificationBroadcast.class).setAction(action), 0);
    }

    //Binder Class
    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

}
