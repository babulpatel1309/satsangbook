package com.folioreader.model.sqlite;

import android.content.ContentValues;
import android.database.Cursor;

public class ReadLocatorTable {
    public static final String TABLE_NAME = "read_locator_table";
    public static final String ID = "_id";
    public static final String COL_BOOK_ID = "bookId";
    private static final String COL_CONTENT = "content";

    public static final String SQL_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " + ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT" + ","
            + COL_BOOK_ID + " TEXT" + ","
            + COL_CONTENT + " TEXT" + ")";

    public static ContentValues getHighlightContentValues(String book, String readLocator) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_BOOK_ID, book);
        contentValues.put(COL_CONTENT, readLocator);
        return contentValues;
    }

    public static void saveReadLocatorIfNotExists(String b_name, String readLocator) {
        String query = "SELECT " + ID + " FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" + b_name + "'";
        int id = DbAdapter.getIdForQuery(query);
        if (id == -1) {
            DbAdapter.saveReadLocator(getHighlightContentValues(b_name, readLocator));
        } else {
            DbAdapter.updateReadLocator(getHighlightContentValues(b_name, readLocator), String.valueOf(id));
        }
    }

    public static String getReadLocator(String b_name) {
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" + b_name + "'";
        Cursor c = DbAdapter.getReadLocatorForQuery(query);
        String str = "";
        while (c.moveToNext()) {
            str = c.getString(c.getColumnIndex(COL_CONTENT));
        }
        c.close();
        return str;
    }

}
