package com.folioreader.model;

import android.os.Parcelable;

public interface Bookmark extends Parcelable {

    String getBookId();

    String getBookFile();

    String getChapterHref();

    String getValue();

    int getIsKavya();

    String toJson();
}
