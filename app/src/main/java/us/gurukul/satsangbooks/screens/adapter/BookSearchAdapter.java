package us.gurukul.satsangbooks.screens.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.folioreader.model.Book;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import us.gurukul.satsangbooks.screens.Common;
import us.gurukul.satsangbooks.screens.R;

public class BookSearchAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private ArrayList<Book> bookList;
    private ArrayList<Book> filteredList = new ArrayList<>();
    private BookFilter bookFilter;

    public BookSearchAdapter(Context activity, ArrayList<Book> bookList) {
        this.context = activity;
        this.bookList = bookList;

        getFilter();
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Book book = (Book) getItem(position);
        Log.e("Search", book.getTitle());
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.book_list_row_layout, parent, false);
            holder = new ViewHolder();

            holder.iconText = convertView.findViewById(R.id.book_image);
            holder.name = convertView.findViewById(R.id.book_title);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        Picasso.get().load(Common.imgPath + "320x420/" + book.getFront_img())
                .placeholder(R.drawable.white_bg)
                .into(holder.iconText);
        holder.name.setText(book.getTitle());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (bookFilter == null) {
            bookFilter = new BookFilter();
        }
        return bookFilter;
    }

    public int getItemCount() {
        return filteredList.size();
    }

    public void clearData() {
        filteredList.clear();
        notifyDataSetChanged();
    }

    //
    static class ViewHolder {
        ImageView iconText;
        TextView name;
    }

    //
    private class BookFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<Book> tempList = new ArrayList<>();
                // search content in friend list
                for (Book book : bookList) {
                    if (book.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(book);
                    }
                }
                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = bookList.size();
                filterResults.values = bookList;
            }

            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (ArrayList<Book>) results.values;
            //Log.e("filteredList", filteredList.get(0).getTitle());
            try {
                Collections.sort(filteredList, new Comparator<Book>() {
                    @Override
                    public int compare(Book t1, Book t2) {
                        return t1.getTitle().compareToIgnoreCase(t2.getTitle());
                    }
                });
            } catch (Exception ignored) {
            }
            notifyDataSetChanged();
        }
    }
}
