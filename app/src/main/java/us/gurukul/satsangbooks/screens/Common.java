package us.gurukul.satsangbooks.screens;

import android.os.Environment;

import java.io.File;

public class Common {
    public static final String APP_STORE_URL = "https://play.google.com/store/apps/details?id=";
    public static final String BASE_URL = "http://book2.gurukul.us/";
    public static final String BASE_URL_API = "https://www.rajkotgurukul.com/leadform_test/frontend/api/";
    public static final File fileDir = new File(Environment.getExternalStorageDirectory().toString() + "/Satsang Books/.Books");
    public static final File fav_fileDir = new File(Environment.getExternalStorageDirectory().toString() + "/Satsang Books/.Fav Books");
    public static final File share_fileDir = new File(Environment.getExternalStorageDirectory().toString() + "/Satsang Books/.shareBook");

    public static String imgPath = "http://book2.gurukul.us/resize/book_front_image/";
    public static String filePath = "http://book2.gurukul.us/uploads/book_file/";
    public static String loginURL = "getlogin?phone=";
    public static String emailURL = "getlogin?email=";
    public static String PREF_USER_EMAIL = "email";
    public static String PREF_USER_NAME= "name";



}
