package com.folioreader.model.sqlite;

import android.content.ContentValues;

public class SeenChapterTable {
    static final String TABLE_NAME = "seen_chapter_table";
    public static final String ID = "_id";
    private static final String COL_BOOK_ID = "bookId";
    public static final String COL_BOOK_FILE = "bookFile";
    private static final String COL_CONTENT = "content";

    public static final String SQL_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " + ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT" + ","
            + COL_BOOK_ID + " TEXT" + ","
            + COL_BOOK_FILE + " TEXT" + ","
            + COL_CONTENT + " TEXT" + ")";

    public static ContentValues getHighlightContentValues(String book, String title,String bookFile) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_BOOK_ID, book);
        contentValues.put(COL_BOOK_FILE, bookFile);
        contentValues.put(COL_CONTENT, title);
        return contentValues;
    }

    public static void saveSeenChapterIfNotExists(String b_name, String title,String bookFile) {
        String query = "SELECT " + ID + " FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" + b_name + "'" + " and " + COL_CONTENT + " = '" + title + "'";
        int id = DbAdapter.getIdForQuery(query);
        if (id == -1) {
            DbAdapter.saveSeenChapter(getHighlightContentValues(b_name, title,bookFile));
        } else {
            DbAdapter.updateSeenChapter(getHighlightContentValues(b_name, title,bookFile), String.valueOf(id));
        }
    }

    public static int getSeenChapterIfNotExists(String b_name, String title) {
        String query = "SELECT " + ID + " FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" + b_name + "'" + " and " + COL_CONTENT + " = '" + title + "'";
        return DbAdapter.getIdForQuery(query);//if not exist return -1, otherwise id number
    }

}
