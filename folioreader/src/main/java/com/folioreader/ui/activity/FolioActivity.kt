package com.folioreader.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.AlertDialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.folioreader.Config
import com.folioreader.Constants
import com.folioreader.Constants.*
import com.folioreader.FolioReader
import com.folioreader.R
import com.folioreader.model.*
import com.folioreader.model.locators.ReadLocator
import com.folioreader.model.locators.SearchLocator
import com.folioreader.model.sqlite.BookMarkTable
import com.folioreader.model.sqlite.HighLightTable
import com.folioreader.model.sqlite.SeenChapterTable
import com.folioreader.service.AudioDownloadService
import com.folioreader.service.MusicService
import com.folioreader.service.SongListener
import com.folioreader.ui.adapter.FolioPageFragmentAdapter
import com.folioreader.ui.adapter.SearchAdapter
import com.folioreader.ui.fragment.FolioPageFragment
import com.folioreader.ui.view.ConfigBottomSheetDialogFragment
import com.folioreader.ui.view.DirectionalViewpager
import com.folioreader.ui.view.FolioAppBarLayout
import com.folioreader.ui.view.MediaControllerCallback
import com.folioreader.util.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.folio_activity.*
import org.readium.r2.shared.Link
import org.readium.r2.shared.Publication
import org.readium.r2.streamer.parser.CbzParser
import org.readium.r2.streamer.parser.EpubParser
import org.readium.r2.streamer.parser.PubBox
import org.readium.r2.streamer.server.Server
import java.io.File
import java.lang.ref.WeakReference

class FolioActivity : AppCompatActivity(), FolioActivityCallback, MediaControllerCallback,
        View.OnSystemUiVisibilityChangeListener, SongListener, View.OnClickListener, OnHighlightListener {

    override fun onHighlight(highlight: HighlightImpl?, type: HighLight.HighLightAction?) {
        if (highlight != null && type == HighLight.HighLightAction.MODIFY) {
            if (highlight.type.equals("highlight_red") || highlight.type.equals("red")) {
                if (note != "") {
                    highlight.note = note
                    note = ""
                    HighLightTable.updateHighlight(highlight)
                }
            }
            Log.e(LOG_TAG, "-> onHighlight -> $highlight")
        }
    }

    override fun onClick(v: View?) {
        getClickOnId(v!!.id)
    }

    private fun getClickOnId(id: Int) {
        Log.d("FolioActivity", "currentChapterIndex " + currentChapterIndex)
        if (id == R.id.prev || id == R.id.prev_button) {
            if (getCurrentChapterIndex() > 0) {
                goToNextPrev(getCurrentChapterIndex() - 1)
            } else {
                Log.d("FolioActivity", "this is first page.")
            }
        } else if (id == R.id.next || id == R.id.next_button) {
            Log.d("FolioActivity", "size " + spine?.size)
            if (spine != null) {
                if (currentChapterIndex < spine!!.size - 1) {
                    goToNextPrev(getCurrentChapterIndex() + 1)
                } else {
                    Log.d("FolioActivity", "The book has been completed.")
                    Toast.makeText(this, "The book has been completed.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun goToNextPrev(curPos: Int) {
        mFolioPageViewPager!!.currentItem = curPos
        currentFragment!!.scrollToFirst()
        musicSrv!!.stopForegroundNotification()//hide when click on next/prev option of chapter
    }

    override fun onSongPrepared(boolean: Boolean) {//when song loaded from server to set view
        isFirst = boolean
        isLoading = boolean
        if (isLoading) {
            mediaControl.visibility = View.INVISIBLE
            fab_play_pause.visibility = View.INVISIBLE
            if (!distractionFreeMode)
                loadProgress.visibility = View.VISIBLE
        } else {
            fab_play_pause.setImageResource(R.drawable.ic_pause_black_24dp)
            loadProgress.visibility = View.INVISIBLE

            if (!distractionFreeMode) {
                fab_play_pause.visibility = View.VISIBLE
                if (musicSrv!!.isPng)
                    mediaControl.visibility = View.VISIBLE
            }

            if (!musicSrv!!.wasPlaying) {//last song playing stop to set
                musicSrv!!.playPlayer()
                mHandler.postDelayed(mUpdateTimeTask, 1000)
                if (!distractionFreeMode)
                    mediaControl.visibility = View.VISIBLE
            }
        }
    }

    override fun onSongPlayPrev() {
        getClickOnId(R.id.prev)
    }

    override fun onSongPlayNext() {
        getClickOnId(R.id.next)
    }

    override fun onSongPlayPause() {
        if (musicSrv!!.isPng) {
            fab_play_pause.setImageResource(R.drawable.ic_pause_black_24dp)
            mediaControl.visibility = View.VISIBLE
        } else {
            fab_play_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
            mediaControl.visibility = View.INVISIBLE
        }
    }

    override fun onSongFailed() {
        isLoading = false
        loadProgress.visibility = View.INVISIBLE
        fab_play_pause.visibility = View.INVISIBLE
        fab_play_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
        Toast.makeText(this, "failed to play from server", Toast.LENGTH_SHORT).show()
    }

    private var bookFileName: String? = null

    private var mFolioPageViewPager: DirectionalViewpager? = null
    private var actionBar: ActionBar? = null
    private var appBarLayout: FolioAppBarLayout? = null
    private var toolbar: Toolbar? = null
    private var distractionFreeMode: Boolean = false
    private var currentChapterIndex: Int = 0
    private var mFolioPageFragmentAdapter: FolioPageFragmentAdapter? = null
    private var entryReadLocator: ReadLocator? = null
    private var lastReadLocator: ReadLocator? = null
    private var outState: Bundle? = null
    private var savedInstanceState: Bundle? = null

    private var r2StreamerServer: Server? = null
    private var pubBox: PubBox? = null
    private var spine: List<Link>? = null

    private var mBookId: String? = null
    private var mEpubFilePath: String? = null
    private var mEpubSourceType: EpubSourceType? = null
    private var mEpubRawId = 0
    private var direction: Config.Direction = Config.Direction.VERTICAL
    private var portNumber: Int = Constants.DEFAULT_PORT_NUMBER
    private var streamerUri: Uri? = null

    private var searchUri: Uri? = null
    private var searchAdapterDataBundle: Bundle? = null
    private var searchQuery: CharSequence? = null
    private var searchLocator: SearchLocator? = null

    private var displayMetrics: DisplayMetrics? = null
    private var density: Float = 0.toFloat()
    private var topActivity: Boolean? = null
    private var taskImportance: Int = 0
    private var menu: Menu? = null
    private var thread1: Thread? = null
    private var mHandlerThread: Handler? = null
    private var isScrolled: Boolean = false
    private var scroll = 0
    private var scrollTime = 80
    private var audioUrl = "null"
    private var playIntent: Intent? = null
    private var mIntentFilter: IntentFilter? = null
    private var isToast: Boolean = true
    private val UPDATE_COUNT = 101
    var config: Config? = null

    companion object {

        @JvmField
        val LOG_TAG: String = FolioActivity::class.java.simpleName
        const val INTENT_EPUB_SOURCE_PATH = "com.folioreader.epub_asset_path"
        const val INTENT_EPUB_SOURCE_TYPE = "epub_source_type"
        const val EXTRA_READ_LOCATOR = "com.folioreader.extra.READ_LOCATOR"
        private const val BUNDLE_READ_LOCATOR_CONFIG_CHANGE = "BUNDLE_READ_LOCATOR_CONFIG_CHANGE"
        private const val BUNDLE_DISTRACTION_FREE_MODE = "BUNDLE_DISTRACTION_FREE_MODE"
        const val EXTRA_SEARCH_ITEM = "EXTRA_SEARCH_ITEM"
        const val ACTION_SEARCH_CLEAR = "ACTION_SEARCH_CLEAR"
        private const val HIGHLIGHT_ITEM = "highlight_item"
        var isLoading = false
        var isFirst = false
        var musicSrv: MusicService? = null
        var isChapterChanged = false
    }

    //connect to the service
    private val musicConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            musicSrv = (service as MusicService.MusicBinder).service
            musicSrv!!.setCallbacks(this@FolioActivity)
            bindService(Intent(this@FolioActivity, MusicService::class.java), this, Context.BIND_AUTO_CREATE)
        }

        override fun onServiceDisconnected(name: ComponentName) {
        }
    }

    @SuppressLint("HandlerLeak")
    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            when (msg.what) {
                0 -> if (!distractionFreeMode) {
                    if (musicSrv!!.isPng) {
                        fab_play_pause.visibility = View.INVISIBLE
                        loadProgress.visibility = View.VISIBLE
                        isLoading = true
                        //Log.d("MUSIC PLAYER", "playSong-> " + true)
                    }
                }
                1 -> if (!distractionFreeMode) {
                    fab_play_pause.visibility = View.VISIBLE
                    loadProgress.visibility = View.INVISIBLE
                    isLoading = false
                    //Log.d("MUSIC PLAYER", "playSong-> " + false)
                }
            }
        }
    }

    private val mUpdateTimeTask = object : Runnable {
        override fun run() {
            if (musicSrv != null) {
                if (musicSrv!!.isPng) {
                    updateProgressBar()
                    if (musicSrv!!.buffered < seekBar.progress)
                        mHandler.sendEmptyMessage(0)
                    else
                        mHandler.sendEmptyMessage(1)
                }
            }
            mHandler.postDelayed(this, 500)
        }
    }

    private fun updateProgressBar() {
        seekBar.progress = getProgressPercentage(musicSrv!!.posn.toLong(), musicSrv!!.dur.toLong())
        //Log.d("progress", "" + seekBar.progress);
        seekBar.secondaryProgress = musicSrv!!.buffered
    }

    private fun getProgressPercentage(currentDuration: Long, totalDuration: Long): Int {
        //val percentage: Double?
        val currentSeconds = currentDuration / 1000
        val totalSeconds = totalDuration / 1000
        // calculating percentage and return percentage
        val percentage = currentSeconds.toFloat() / totalSeconds * 100
        return percentage.toInt()
    }

    private fun progressToTimer(progress: Int, totalDuration: Int): Int {
        val currentDuration = (progress.toFloat() / 100 * (totalDuration / 1000)).toInt()
        return currentDuration * 1000
    }

    private val closeBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.v(LOG_TAG, "-> closeBroadcastReceiver -> onReceive -> " + intent.action!!)

            val action = intent.action
            if (action != null && action == FolioReader.ACTION_CLOSE_FOLIOREADER) {

                try {
                    val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
                    val tasks = activityManager.runningAppProcesses
                    taskImportance = tasks[0].importance
                } catch (e: Exception) {
                    Log.e(LOG_TAG, "-> ", e)
                }

                val closeIntent = Intent(applicationContext, FolioActivity::class.java)
                closeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                closeIntent.action = FolioReader.ACTION_CLOSE_FOLIOREADER
                this@FolioActivity.startActivity(closeIntent)
            }
        }
    }

    val statusBarHeight: Int
        get() {
            var result = 0
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0)
                result = resources.getDimensionPixelSize(resourceId)
            return result
        }

    private val searchReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.v(LOG_TAG, "-> searchReceiver -> onReceive -> " + intent.action!!)

            val action = intent.action ?: return
            when (action) {
                ACTION_SEARCH_CLEAR -> clearSearchLocator()
            }
        }
    }

    private val currentFragment: FolioPageFragment?
        get() = if (mFolioPageFragmentAdapter != null && mFolioPageViewPager != null) {
            mFolioPageFragmentAdapter!!
                    .getItem(mFolioPageViewPager!!.currentItem) as FolioPageFragment
        } else {
            null
        }

    enum class EpubSourceType {
        RAW,
        ASSETS,
        SD_CARD
    }

    private enum class RequestCode(internal val value: Int) {
        CONTENT_HIGHLIGHT(77),
        SEARCH(101)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        Log.v(LOG_TAG, "-> onNewIntent")

        val action = getIntent().action
        if (action != null && action == FolioReader.ACTION_CLOSE_FOLIOREADER) {

            if (topActivity == null || topActivity == false) {
                // FolioActivity was already left, so no need to broadcast ReadLocator again.
                // Finish activity without going through onPause() and onStop()
                finish()

                // To determine if app in background or foreground
                var appInBackground = false
                if (Build.VERSION.SDK_INT < 26) {
                    if (ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND == taskImportance)
                        appInBackground = true
                } else {
                    if (ActivityManager.RunningAppProcessInfo.IMPORTANCE_CACHED == taskImportance)
                        appInBackground = true
                }
                if (appInBackground)
                    moveTaskToBack(true)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        try {
            if (playIntent == null) {
                playIntent = Intent(this, MusicService::class.java)
                bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE)
                startService(playIntent)
            }
        } catch (e: Exception) {
            Log.e("MainActivity=Exception ", e.message)
        }
    }

    override fun onRestart() {
        super.onRestart()
        try {
            if (playIntent == null) {
                playIntent = Intent(this, MusicService::class.java)
                bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE)
                startService(playIntent)
            }
        } catch (e: Exception) {
            Log.e("MainActivity=Exception ", e.message)
        }
    }

    override fun onResume() {
        super.onResume()
        Log.v(LOG_TAG, "-> onResume")
        topActivity = true

        val action = intent.action
        if (action != null && action == FolioReader.ACTION_CLOSE_FOLIOREADER) {
            // FolioActivity is topActivity, so need to broadcast ReadLocator.
            finish()
        }

        registerReceiver(receiver, mIntentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(receiver)
        mHandler.removeCallbacks(mUpdateTimeTask)
    }

    override fun onStop() {
        super.onStop()
        Log.v(LOG_TAG, "-> onStop")
        topActivity = false
    }

    override fun onBackPressed() {
        appExitInfo()
    }

    private fun appExitInfo() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Do you want to exit from Book?")

        builder.setPositiveButton("Yes") { dialog, which ->
            finish()
        }
        builder.setNegativeButton("No") { dialog, which ->
            dialog.dismiss()
        }
        val sortDialog = builder.create()
        sortDialog.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Need to add when vector drawables support library is used.
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        System.gc()
        System.gc()

        val display = windowManager.defaultDisplay
        displayMetrics = resources.displayMetrics
        display.getRealMetrics(displayMetrics)
        density = displayMetrics!!.density
        LocalBroadcastManager.getInstance(this).registerReceiver(
                closeBroadcastReceiver,
                IntentFilter(FolioReader.ACTION_CLOSE_FOLIOREADER)
        )

        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.folio_activity)

        FolioReader.get().setOnHighlightListener(this)

        setConfig(savedInstanceState)

        initDistractionFreeMode(savedInstanceState)

        this.savedInstanceState = savedInstanceState

        if (savedInstanceState != null) {
            searchAdapterDataBundle = savedInstanceState.getBundle(SearchAdapter.DATA_BUNDLE)
            searchQuery = savedInstanceState.getCharSequence(SearchActivity.BUNDLE_SAVE_SEARCH_QUERY)
        }

        mBookId = intent.getStringExtra(FolioReader.EXTRA_BOOK_ID)
        mEpubSourceType = intent.extras!!.getSerializable(FolioActivity.INTENT_EPUB_SOURCE_TYPE) as EpubSourceType
        if (mEpubSourceType == EpubSourceType.RAW) {
            mEpubRawId = intent.extras!!.getInt(FolioActivity.INTENT_EPUB_SOURCE_PATH)
        } else {
            mEpubFilePath = intent.extras!!.getString(FolioActivity.INTENT_EPUB_SOURCE_PATH)
        }

        initActionBar()
        initController()

        if (ContextCompat.checkSelfPermission(this@FolioActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@FolioActivity, Constants.getWriteExternalStoragePerms(), Constants.WRITE_EXTERNAL_STORAGE_REQUEST)
        } else {
            setupBook()
        }
    }

    private fun initActionBar() {
        appBarLayout = findViewById(R.id.appBarLayout)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        actionBar = supportActionBar

        val config = AppUtil.getSavedConfig(applicationContext)!!

        val drawable = ContextCompat.getDrawable(this, R.drawable.ic_drawer)
        //UiUtil.setColorIntToDrawable(config.themeColor, drawable!!) // theme green color
        //UiUtil.setColorIntToDrawable(ContextCompat.getColor(this, R.color.white), drawable!!)
        toolbar!!.navigationIcon = drawable

        if (config.isNightMode) {
            setNightMode()
        } else {
            setDayMode()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val color: Int
            if (config.isNightMode) {
                color = ContextCompat.getColor(this, R.color.black)
            } else {
                val attrs = intArrayOf(android.R.attr.navigationBarColor)
                val typedArray = theme.obtainStyledAttributes(attrs)
                color = typedArray.getColor(0, ContextCompat.getColor(this, R.color.white))
            }
            window.navigationBarColor = color
        }

        /*if (Build.VERSION.SDK_INT < 16) {
            // Fix for appBarLayout.fitSystemWindows() not being called on API < 16
            appBarLayout!!.setTopMargin(statusBarHeight)
        }*/

    }

    override fun setDayMode() {
        Log.v(LOG_TAG, "-> setDayMode")
        actionBar!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary)))
        toolbar!!.setTitleTextColor(ContextCompat.getColor(this, R.color.white))

        UiUtil.setColorIntToDrawable(ContextCompat.getColor(this, R.color.mark_color), prev.background)
        UiUtil.setColorIntToDrawable(ContextCompat.getColor(this, R.color.mark_color), next.background)

        fab_play_pause.background = ContextCompat.getDrawable(this, R.drawable.orange_color_circle)
        download_audio.background = ContextCompat.getDrawable(this, R.drawable.orange_color_circle)
        loadProgress.background = ContextCompat.getDrawable(this, R.drawable.orange_color_circle)

        bottomLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_color))
        switchButton.setTextColor(ContextCompat.getColor(this, R.color.scroll_color))
        scrollText.setTextColor(ContextCompat.getColor(this, R.color.scroll_color))
    }

    override fun setNightMode() {
        Log.v(LOG_TAG, "-> setNightMode")
        actionBar!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(this, R.color.black)))
        toolbar!!.setTitleTextColor(ContextCompat.getColor(this, R.color.white))

        UiUtil.setColorIntToDrawable(ContextCompat.getColor(this, R.color.black), prev.background)
        UiUtil.setColorIntToDrawable(ContextCompat.getColor(this, R.color.black), next.background)

        fab_play_pause.background = ContextCompat.getDrawable(this, R.drawable.black_circle_shape)
        download_audio.background = ContextCompat.getDrawable(this, R.drawable.black_circle_shape)
        loadProgress.background = ContextCompat.getDrawable(this, R.drawable.black_circle_shape)

        bottomLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.black))
        switchButton.setTextColor(ContextCompat.getColor(this, R.color.app_gray))
        scrollText.setTextColor(ContextCompat.getColor(this, R.color.app_gray))
    }

    override fun searchSelectionText(text: String) {
        callToSearchView(text)
    }

    private fun getSctollTime(progress: Int): Int {
        var value = 80
        when (progress) {
            in 10..19 -> value = 90
            in 20..29 -> value = 80
            in 30..39 -> value = 70
            in 40..49 -> value = 60
            in 50..59 -> value = 50
            in 60..69 -> value = 40
            in 70..79 -> value = 30
            in 80..89 -> value = 20
            in 90..100 -> value = 10
        }

        return value
    }

    private fun initController() {
        Log.v(LOG_TAG, "-> initController")

        mIntentFilter = IntentFilter()
        mIntentFilter!!.addAction(AudioDownloadService.RESULT)
        mIntentFilter!!.addAction(AudioDownloadService.FAILED)

        prev.setOnClickListener(this)
        next.setOnClickListener(this)
        prev_button.setOnClickListener(this)
        next_button.setOnClickListener(this)

        fab_play_pause.setOnClickListener {
            Log.d("FolioActivity", "Fab click")
            if (musicSrv!!.isPng) {
                musicSrv!!.pausePlayer()
                fab_play_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
                mediaControl.visibility = View.INVISIBLE
            } else {
                if (isFirst) {
                    if (audioUrl.startsWith("http://") || audioUrl.startsWith("https://")) {
                        if (NetworkHelper.isOnline(this)) {//online
                            Log.d("FolioActivity", "isOnline-> online play")
                            musicSrv!!.playSong()
                            loadProgress.visibility = View.VISIBLE
                            fab_play_pause.visibility = View.INVISIBLE
                        } else {
                            if (isToast) {
                                Toast.makeText(this, "No Internet Connection.", Toast.LENGTH_SHORT).show()
                                isToast = false
                                Handler().postDelayed({ isToast = true }, 2000)
                            }
                        }
                    } else {//off line
                        val file = File(audioUrl)
                        val savedFile = File(fileAudioDir, file.name)
                        if (savedFile.exists()) {
                            Log.d("FolioActivity", "savedFile-> offline play")
                            musicSrv!!.playSong()
                            loadProgress.visibility = View.VISIBLE
                            fab_play_pause.visibility = View.INVISIBLE
                        } else
                            Log.d("FolioActivity", "savedFile missing-> offline play")
                    }
                } else {
                    musicSrv!!.playPlayer()
                    mHandler.postDelayed(mUpdateTimeTask, 1000)
                    fab_play_pause.setImageResource(R.drawable.ic_pause_black_24dp)

                    loadProgress.visibility = View.INVISIBLE
                    fab_play_pause.visibility = View.VISIBLE
                    mediaControl.visibility = View.VISIBLE
                }
            }
        }

        @SuppressLint("HandlerLeak")
        mHandlerThread = object : Handler() {
            override fun handleMessage(msg: Message) {
                super.handleMessage(msg)
                if (msg.what == UPDATE_COUNT) {
                    val folioPageFragment = currentFragment
                    scroll = folioPageFragment!!.mWebview!!.scrollY
                    //Log.e("scrollY", ":$scroll")
                    scroll += 1
                    folioPageFragment.mWebview!!.scrollTo(0, scroll)
                }
            }
        }

        thread1 = Thread(Runnable {
            while (true) {
                if (isScrolled) {
                    try {
                        Thread.sleep(scrollTime.toLong())
                    } catch (ex: InterruptedException) {
                        ex.printStackTrace()
                    }
                    mHandlerThread!!.sendEmptyMessage(UPDATE_COUNT)
                }
            }
        })

        switchButton.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.e("Switch", "Value $isChecked")
            if (isChecked) {
                if (thread1!!.isAlive)
                    mHandlerThread!!.sendEmptyMessage(UPDATE_COUNT)
                else
                    thread1!!.start()
            }
            isScrolled = isChecked
            val folioPageFragment = currentFragment
            if (folioPageFragment != null)
                folioPageFragment.mWebview?.setScrollingBar(isScrolled)
        }

        scrollSeekbar.progress = SharedPreferenceUtil.getSharedPreferencesInt(this, "progress", 20)
        scrollSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                Log.e("Progress", "Value $progress")
                if (progress >= 10)
                    scrollText.text = String.format("%sx", progress / 10)
                else {
                    scrollSeekbar.progress = 10
                    scrollText.text = String.format("%sx", 1)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                scrollTime = getSctollTime(scrollSeekbar.progress)
            }
        })

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                Log.e("Progress", "Value $progress")
                mHandler.removeCallbacks(mUpdateTimeTask)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mHandler.removeCallbacks(mUpdateTimeTask)
                if (musicSrv != null) {
                    musicSrv!!.seek(progressToTimer(seekBar.progress, musicSrv!!.dur))
                    mHandler.postDelayed(mUpdateTimeTask, 1000)
                    mHandler.hasMessages(0)
                }
            }
        })

        if (!fileAudioDir.exists())
            fileAudioDir.mkdir()

        //Audio
        download_audio.setOnClickListener {
            if (NetworkHelper.isOnline(this)) {
                if (!audioUrl.equals("")) {
                    val fragment = currentFragment
                    val title1 = getChapterTitle(fragment?.spineItem?.href!!)
                    val audio = Audio(1, title.toString(), title1, audioUrl)
                    try {
                        download_audio.visibility = View.INVISIBLE
                        val myServiceIntent = Intent(this@FolioActivity, AudioDownloadService::class.java)
                        myServiceIntent.putExtra("audioItem", audio)
                        startService(myServiceIntent)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            } else {
                if (isToast) {
                    Toast.makeText(this, "No Internet Connection.", Toast.LENGTH_SHORT).show()
                    isToast = false
                    Handler().postDelayed({ isToast = true }, 2000)
                }
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        if (itemId == R.id.itemHome) {
            Constants.isHome = true
            finish()
        } else if (itemId == android.R.id.home) {
            Log.v(LOG_TAG, "-> onOptionsItemSelected -> drawer")
            startContentHighlightActivity()
            return true

        } else if (itemId == R.id.itemSearch) {
            Log.v(LOG_TAG, "-> onOptionsItemSelected -> " + item.title)
            if (searchUri == null)
                return true
            callToSearchView("")
            return true

        } else if (itemId == R.id.itemConfig) {
            Log.v(LOG_TAG, "-> onOptionsItemSelected -> " + item.title)
            showConfigBottomSheetDialogFragment()
            return true

        } else if (itemId == R.id.itemFav) {
            Log.v(LOG_TAG, "-> onOptionsItemSelected -> " + item.title)
            val bookmarkPosition = getBookmarkPosition()
            if (bookmarkPosition != null) {
                BookMarkTable.saveBookmarkIfNotExists(bookmarkPosition)
                val id = BookMarkTable.getBookmarkIfNotExists(bookmarkPosition)
                if (id != -1)
                    Toast.makeText(this, "Added to Bookmark successfully", Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(this, "Removed from Bookmark successfully", Toast.LENGTH_SHORT).show()

                setBookmark(id)
            }

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun callToSearchView(word: String) {
        val intent = Intent(this, SearchActivity::class.java)
        intent.putExtra(SearchActivity.BUNDLE_SPINE_SIZE, spine?.size ?: 0)
        intent.putExtra(SearchActivity.BUNDLE_SEARCH_URI, searchUri)
        intent.putExtra(SearchAdapter.DATA_BUNDLE, searchAdapterDataBundle)
        intent.putExtra(SearchActivity.BUNDLE_SAVE_SEARCH_QUERY, searchQuery)
        intent.putExtra(SearchActivity.BUNDLE_WORD_TEXT, word)
        intent.putExtra(Constants.PUBLICATION, pubBox!!.publication)
        startActivityForResult(intent, RequestCode.SEARCH.value)
    }

    private fun startContentHighlightActivity() {
        val intent = Intent(this@FolioActivity, ContentHighlightActivity::class.java)
        intent.putExtra(Constants.PUBLICATION, pubBox?.publication)

        try {
            intent.putExtra(CHAPTER_SELECTED, spine!![currentChapterIndex].href)
        } catch (e: NullPointerException) {
            Log.w(LOG_TAG, "-> ", e)
            intent.putExtra(CHAPTER_SELECTED, "")
        } catch (e: IndexOutOfBoundsException) {
            Log.w(LOG_TAG, "-> ", e)
            intent.putExtra(CHAPTER_SELECTED, "")
        }

        intent.putExtra(FolioReader.EXTRA_BOOK_ID, mBookId)
        intent.putExtra(Constants.BOOK_TITLE, bookFileName)

        startActivityForResult(intent, RequestCode.CONTENT_HIGHLIGHT.value)
        //overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    private fun showConfigBottomSheetDialogFragment() {
        val bottomDialog = ConfigBottomSheetDialogFragment()
        bottomDialog.setLoading(currentFragment?.loadingView)
        bottomDialog.show(supportFragmentManager, ConfigBottomSheetDialogFragment.LOG_TAG)
    }

    private fun setupBook() {
        Log.v(LOG_TAG, "-> setupBook")
        try {
            initBook()
            onBookInitSuccess()
        } catch (e: Exception) {
            Log.e(LOG_TAG, "-> Failed to initialize audio", e)
            onBookInitFailure()
        }
    }

    @Throws(Exception::class)
    private fun initBook() {
        Log.v(LOG_TAG, "-> initBook")

        bookFileName = FileUtil.getEpubFilename(this, mEpubSourceType!!, mEpubFilePath, mEpubRawId)
        val path = FileUtil.saveEpubFileAndLoadLazyBook(
                this, mEpubSourceType, mEpubFilePath,
                mEpubRawId, bookFileName
        )
        val extension: Publication.EXTENSION
        var extensionString: String? = null
        try {
            extensionString = FileUtil.getExtensionUppercase(path)
            extension = Publication.EXTENSION.valueOf(extensionString)
        } catch (e: IllegalArgumentException) {
            throw Exception("-> Unknown audio file extension `$extensionString`", e)
        }

        pubBox = when (extension) {
            Publication.EXTENSION.EPUB -> {
                val epubParser = EpubParser()
                epubParser.parse(path!!, "")
            }
            Publication.EXTENSION.CBZ -> {
                val cbzParser = CbzParser()
                cbzParser.parse(path!!, "")
            }
            else -> {
                null
            }
        }

        portNumber = intent.getIntExtra(FolioReader.EXTRA_PORT_NUMBER, Constants.DEFAULT_PORT_NUMBER)
        portNumber = AppUtil.getAvailablePortNumber(portNumber)

        r2StreamerServer = Server(portNumber)
        r2StreamerServer!!.addEpub(
                pubBox!!.publication, pubBox!!.container,
                "/" + bookFileName!!, null
        )

        r2StreamerServer!!.start()

        FolioReader.initRetrofit(streamerUrl)
    }

    private fun onBookInitFailure() {
        //TODO -> Fail gracefully
    }

    private fun onBookInitSuccess() {

        val publication = pubBox!!.publication
        spine = publication.readingOrder
        title = publication.metadata.title

        if (mBookId == null) {
            if (!publication.metadata.identifier.isEmpty()) {
                mBookId = publication.metadata.identifier
            } else {
                if (!publication.metadata.title.isEmpty()) {
                    mBookId = publication.metadata.title.hashCode().toString()
                } else {
                    mBookId = bookFileName!!.hashCode().toString()
                }
            }
        }

        // searchUri currently not in use as it's uri is constructed through Retrofit,
        // code kept just in case if required in future.
        for (link in publication.links) {
            if (link.rel.contains("search")) {
                searchUri = Uri.parse("http://" + link.href!!)
                break
            }
        }
        if (searchUri == null)
            searchUri = Uri.parse(streamerUrl + "search")

        configFolio()
    }

    override fun getStreamerUrl(): String {

        if (streamerUri == null) {
            streamerUri = Uri.parse(String.format(STREAMER_URL_TEMPLATE, LOCALHOST, portNumber, bookFileName))
        }
        return streamerUri.toString()
    }

    override fun onDirectionChange(newDirection: Config.Direction) {
        Log.v(LOG_TAG, "-> onDirectionChange")

        var folioPageFragment: FolioPageFragment? = currentFragment ?: return
        entryReadLocator = folioPageFragment!!.getLastReadLocator()
        val searchLocatorVisible = folioPageFragment.searchLocatorVisible

        isScrolled = false

        direction = newDirection
        if (direction == Config.Direction.HORIZONTAL) {
            bottomLayout.visibility = View.GONE
        } else
            bottomLayout.visibility = View.VISIBLE

        mFolioPageViewPager!!.setDirection(newDirection)
        mFolioPageFragmentAdapter = FolioPageFragmentAdapter(
                supportFragmentManager,
                spine, bookFileName, mBookId
        )
        mFolioPageViewPager!!.adapter = mFolioPageFragmentAdapter
        mFolioPageViewPager!!.currentItem = currentChapterIndex

        folioPageFragment = currentFragment ?: return
        searchLocatorVisible?.let {
            folioPageFragment.highlightSearchLocator(searchLocatorVisible)
        }
    }

    override fun setScreenBrightness(brightnessValue: Int) {
        // Make sure brightness value between 0 to 255
        if (brightnessValue in 0..255) {
            val layoutpars = window.attributes
            layoutpars.screenBrightness = brightnessValue / 255.toFloat()
            window.attributes = layoutpars
        }
    }

    override fun onNoteWordMeaning(wordMeaning: String) {
        val snackBar = Snackbar.make(findViewById(R.id.main), wordMeaning, Snackbar.LENGTH_INDEFINITE)

        val layout = snackBar.view as Snackbar.SnackbarLayout
        // Hide the text
        val textView = layout.findViewById<View>(com.google.android.material.R.id.snackbar_text)
        textView.visibility = View.INVISIBLE

        // Inflate our custom view
        val snackView = LayoutInflater.from(this).inflate(R.layout.my_snackbar, null)
        // Configure the view
        snackView.findViewById<ImageView>(R.id.closeIcon).setOnClickListener(View.OnClickListener { snackBar.dismiss() })

        val config = AppUtil.getSavedConfig(applicationContext)!!
        if (config.isNightMode) {
            snackView.setBackgroundColor(ContextCompat.getColor(applicationContext, R.color.night_text_color))
        }

        val textViewTop = snackView.findViewById<TextView>(R.id.wordMeaning)
        textViewTop.text = wordMeaning

        //If the view is not covering the whole snackbar layout, add this line
        layout.setPadding(0, 0, 0, 0)
        layout.setBackgroundColor(resources.getColor(android.R.color.transparent))

        // Add the view to the Snackbar's layout
        layout.addView(snackView, 0)

        snackBar.show()
    }

    override fun getAudioUrl(url: String) {
        //set is scrolling text
        if (switchButton.isChecked) {
            isScrolled = true
        }

        //get Url
        val file = File(url)
        val savedFile = File(fileAudioDir, file.name)
        if (savedFile.exists())
            audioUrl = savedFile.absolutePath
        else
            audioUrl = url
        Log.e("Audio Url", "audioUrl-> $audioUrl")

        runOnUiThread {
            // Stuff that updates the UI
            if (audioUrl == "null") {
                fab_play_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
                fab_play_pause.visibility = View.INVISIBLE
                mediaControl.visibility = View.INVISIBLE
                download_audio.visibility = View.INVISIBLE
                musicSrv!!.stopForegroundNotification()
            } else {
                val fragment = currentFragment
                if (fragment != null)
                    musicSrv!!.setPlayData(title.toString(), getChapterTitle(fragment.spineItem.href!!), audioUrl)
                else
                    musicSrv!!.setPlayData(title.toString(), "", audioUrl)

                musicSrv!!.showNotification()//show when music available of Chapter

                if (!distractionFreeMode) {
                    fab_play_pause.visibility = View.VISIBLE
                    if (savedFile.exists())
                        download_audio.visibility = View.INVISIBLE
                    else
                        download_audio.visibility = View.VISIBLE
                }
                if (isChapterChanged && musicSrv!!.wasPlaying) {
                    if (audioUrl.startsWith("http://") || audioUrl.startsWith("https://")) {
                        if (NetworkHelper.isOnline(this)) {
                            musicSrv!!.playSong()
                            if (!distractionFreeMode)
                                loadProgress.visibility = View.VISIBLE
                            fab_play_pause.visibility = View.INVISIBLE
                        } else {
                            fab_play_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
                            Toast.makeText(this, "No Internet Connection.", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        musicSrv!!.playSong()
                        if (!distractionFreeMode)
                            loadProgress.visibility = View.VISIBLE
                        fab_play_pause.visibility = View.INVISIBLE
                    }
                }
            }
        }

    }

    private fun initDistractionFreeMode(savedInstanceState: Bundle?) {
        Log.v(LOG_TAG, "-> initDistractionFreeMode")

        window.decorView.setOnSystemUiVisibilityChangeListener(this)

        // Deliberately Hidden and shown to make activity contents lay out behind SystemUI
        hideSystemUI()
        showSystemUI()

        distractionFreeMode = savedInstanceState != null && savedInstanceState.getBoolean(BUNDLE_DISTRACTION_FREE_MODE)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        Log.v(LOG_TAG, "-> onPostCreate")

        if (distractionFreeMode) {
            Handler().post { hideSystemUI() }
        }
    }

    /**
     * @return returns height of status bar + app bar as requested by param [DisplayUnit]
     */
    override fun getTopDistraction(unit: DisplayUnit): Int {

        var topDistraction = 0
        if (!distractionFreeMode) {
            topDistraction = statusBarHeight
            if (actionBar != null)
                topDistraction += actionBar!!.height
        }

        when (unit) {
            DisplayUnit.PX -> return topDistraction

            DisplayUnit.DP -> {
                topDistraction /= density.toInt()
                return topDistraction
            }

            else -> throw IllegalArgumentException("-> Illegal argument -> unit = $unit")
        }
    }

    /**
     * Calculates the bottom distraction which can cause due to navigation bar.
     * In mobile landscape mode, navigation bar is either to left or right of the screen.
     * In tablet, navigation bar is always at bottom of the screen.
     *
     * @return returns height of navigation bar as requested by param [DisplayUnit]
     */
    override fun getBottomDistraction(unit: DisplayUnit): Int {

        var bottomDistraction = 0
        if (!distractionFreeMode)
            bottomDistraction = appBarLayout!!.navigationBarHeight

        when (unit) {
            DisplayUnit.PX -> return bottomDistraction

            DisplayUnit.DP -> {
                bottomDistraction /= density.toInt()
                return bottomDistraction
            }

            else -> throw IllegalArgumentException("-> Illegal argument -> unit = $unit")
        }
    }

    /**
     * Calculates the Rect for visible viewport of the webview in PX.
     * Visible viewport changes in following cases -
     * 1. In distraction free mode,
     * 2. In mobile landscape mode as navigation bar is placed either on left or right side,
     * 3. In tablets, navigation bar is always placed at bottom of the screen.
     */
    private fun computeViewportRect(): Rect {
        //Log.v(LOG_TAG, "-> computeViewportRect");

        val viewportRect = Rect(appBarLayout!!.insets)
        if (distractionFreeMode)
            viewportRect.left = 0
        viewportRect.top = getTopDistraction(DisplayUnit.PX)
        if (distractionFreeMode) {
            viewportRect.right = displayMetrics!!.widthPixels
        } else {
            viewportRect.right = displayMetrics!!.widthPixels - viewportRect.right
        }
        viewportRect.bottom = displayMetrics!!.heightPixels - getBottomDistraction(DisplayUnit.PX)

        return viewportRect
    }

    override fun getViewportRect(unit: DisplayUnit): Rect {

        val viewportRect = computeViewportRect()
        when (unit) {
            DisplayUnit.PX -> return viewportRect

            DisplayUnit.DP -> {
                viewportRect.left /= density.toInt()
                viewportRect.top /= density.toInt()
                viewportRect.right /= density.toInt()
                viewportRect.bottom /= density.toInt()
                return viewportRect
            }

            DisplayUnit.CSS_PX -> {
                viewportRect.left = Math.ceil((viewportRect.left / density).toDouble()).toInt()
                viewportRect.top = Math.ceil((viewportRect.top / density).toDouble()).toInt()
                viewportRect.right = Math.ceil((viewportRect.right / density).toDouble()).toInt()
                viewportRect.bottom = Math.ceil((viewportRect.bottom / density).toDouble()).toInt()
                return viewportRect
            }

            else -> throw IllegalArgumentException("-> Illegal argument -> unit = $unit")
        }
    }

    override fun getActivity(): WeakReference<FolioActivity> {
        return WeakReference(this)
    }

    override fun onSystemUiVisibilityChange(visibility: Int) {
        Log.v(LOG_TAG, "-> onSystemUiVisibilityChange -> visibility = $visibility")

        distractionFreeMode = visibility != View.SYSTEM_UI_FLAG_VISIBLE
        Log.v(LOG_TAG, "-> distractionFreeMode = $distractionFreeMode")

        if (actionBar != null) {
            if (distractionFreeMode) {
                actionBar!!.hide()
            } else {
                actionBar!!.show()
            }
        }
    }

    override fun toggleSystemUI() {
        if (distractionFreeMode) {
            showSystemUI()
        } else {
            hideSystemUI()
        }

        val decorView = window.decorView
        val uiOptions = decorView.systemUiVisibility
        val isImmersiveModeEnabled = uiOptions or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY == uiOptions
        Log.v(LOG_TAG, "-> toggleSystemUI ->distractionFreeMode -$distractionFreeMode isImmersiveModeEnabled-$isImmersiveModeEnabled")
        if (isImmersiveModeEnabled) {
            distractionFreeMode = true
            actionBar!!.hide()
        } else {
            distractionFreeMode = false
            actionBar!!.show()
        }
    }

    private fun showSystemUI() {
        Log.v(LOG_TAG, "-> showSystemUI")
        prevNextLayout.visibility = View.VISIBLE
        if (direction == Config.Direction.HORIZONTAL)
            bottomLayout.visibility = View.GONE
        else
            bottomLayout.visibility = View.VISIBLE

        if (audioUrl == "null") {
            fab_play_pause.visibility = View.INVISIBLE
            mediaControl.visibility = View.INVISIBLE
            loadProgress.visibility = View.INVISIBLE
        } else {
            if (isLoading)
                loadProgress.visibility = View.VISIBLE
            else {
                fab_play_pause.visibility = View.VISIBLE

                val file = File(audioUrl)
                val savedFile = File(fileAudioDir, file.name)
                if (savedFile.exists())
                    download_audio.visibility - View.INVISIBLE
                else
                    download_audio.visibility = View.VISIBLE
            }
        }
        if (musicSrv != null)
            if (musicSrv!!.isPng)
                mediaControl.visibility = View.VISIBLE

        if (Build.VERSION.SDK_INT >= 16) {
            val decorView = window.decorView
            decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    //or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            if (appBarLayout != null)
                appBarLayout!!.setTopMargin(statusBarHeight)
            onSystemUiVisibilityChange(View.SYSTEM_UI_FLAG_VISIBLE)
        }

        //
        if (!audioUrl.equals("null")) {
            val file = File(audioUrl)
            if (AudioDownloadService.isDownload)
                download_audio.visibility = View.INVISIBLE
            else if (!file.exists())
                download_audio.visibility = View.VISIBLE
        }
    }

    private fun hideSystemUI() {
        Log.v(LOG_TAG, "-> hideSystemUI")

        prevNextLayout.visibility = View.INVISIBLE
        fab_play_pause.visibility = View.INVISIBLE
        download_audio.visibility = View.INVISIBLE
        loadProgress.visibility = View.INVISIBLE
        mediaControl.visibility = View.INVISIBLE
        bottomLayout.visibility = View.GONE

        if (Build.VERSION.SDK_INT >= 16) {
            val decorView = window.decorView
            decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    // Set the content to appear under the system bars so that the
                    // content doesn't resize when the system bars hide and show.
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    //or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    // Hide the nav bar and status bar
                    //or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)
        } else {
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
            // Specified 1 just to mock anything other than View.SYSTEM_UI_FLAG_VISIBLE
            onSystemUiVisibilityChange(1)
        }
    }

    override fun getEntryReadLocator(): ReadLocator? {
        if (entryReadLocator != null) {
            val tempReadLocator = entryReadLocator
            entryReadLocator = null
            return tempReadLocator
        }
        return null
    }

    override fun setBookMarkChapter() {
        //When change Chapter using scroll or swipe
        val bookmarkPosition = getBookmarkPosition()
        if (bookmarkPosition != null) {
            val id = BookMarkTable.getBookmarkIfNotExists(bookmarkPosition)
            setBookmark(id)
        }

        //save seen chapter to next/prev and scroll next/prev page
        saveSeenChapter()
    }

    private fun saveSeenChapter() {
        val folioPageFragment = currentFragment
        if (folioPageFragment != null) {
            folioPageFragment.mWebview!!.loadUrl("javascript:getAudioUrl()")
            SeenChapterTable.saveSeenChapterIfNotExists(mBookId, folioPageFragment.spineItem.href, File(mEpubFilePath).name)
        }
    }

    private fun getBookmarkPosition(): BookmarkImpl? {
        val folioPageFragment = currentFragment
        if (folioPageFragment != null) {
            //get Epub file path
            return BookmarkImpl(title as String, File(mEpubFilePath).name, folioPageFragment.spineItem.href, getChapterTitle(folioPageFragment.spineItem.href!!), config!!.is_kavya)
            //return BookmarkImpl(mBookId, File(mEpubFilePath).name, folioPageFragment.spineItem.href, getChapterTitle(folioPageFragment.spineItem.href!!))
        }
        return null
    }

    private fun setBookmark(id: Int) {
        if (id != -1) {
            UiUtil.setColorIntToDrawable(ContextCompat.getColor(this, R.color.mark_color), menu!!.findItem(R.id.itemFav).icon)
        } else {
            UiUtil.setColorIntToDrawable(ContextCompat.getColor(this, android.R.color.white), menu!!.findItem(R.id.itemFav).icon)
        }
    }

    private fun getChapterTitle(href: String): String? {
        if (pubBox!!.publication.tableOfContents.isNotEmpty()) {
            for (tocLink in pubBox!!.publication.tableOfContents) {
                if (tocLink.href!!.contains(href)) {
                    Log.e("Chapter Title", tocLink.title)
                    return tocLink.title
                }

                //Log.e("tocLink", "" + tocLink.getChildren().size());
                for (tocLink1 in tocLink.children) {
                    if (tocLink1.href!!.contains(href)) {
                        Log.e("Sub Chapter Title", tocLink1.title)
                        return tocLink1.title
                    }
                }
            }
        }
        return "Chapter undefine"
    }

    /**
     * Go to chapter specified by href
     *
     * @param href http link or relative link to the page or to the anchor
     * @return true if href is of EPUB or false if other link
     */
    override fun goToChapter(href: String): Boolean {

        for (link in spine!!) {
            if (href.contains(link.href!!)) {
                currentChapterIndex = spine!!.indexOf(link)
                mFolioPageViewPager!!.currentItem = currentChapterIndex
                val folioPageFragment = currentFragment
                folioPageFragment!!.scrollToFirst()
                folioPageFragment.scrollToAnchorId(href)
                return true
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == RequestCode.SEARCH.value) {
            Log.v(LOG_TAG, "-> onActivityResult -> " + RequestCode.SEARCH)

            if (resultCode == Activity.RESULT_CANCELED)
                return

            searchAdapterDataBundle = data!!.getBundleExtra(SearchAdapter.DATA_BUNDLE)
            searchQuery = data.getCharSequenceExtra(SearchActivity.BUNDLE_SAVE_SEARCH_QUERY)

            if (resultCode == SearchActivity.ResultCode.ITEM_SELECTED.value) {

                searchLocator = data.getParcelableExtra(EXTRA_SEARCH_ITEM)
                // In case if SearchActivity is recreated due to screen rotation then FolioActivity
                // will also be recreated, so mFolioPageViewPager might be null.
                if (mFolioPageViewPager == null) return
                currentChapterIndex = getChapterIndex(Constants.HREF, searchLocator!!.href)
                mFolioPageViewPager!!.currentItem = currentChapterIndex
                val folioPageFragment = currentFragment ?: return
                folioPageFragment.highlightSearchLocator(searchLocator!!)
                searchLocator = null
            }

        } else if (requestCode == RequestCode.CONTENT_HIGHLIGHT.value && resultCode == Activity.RESULT_OK && data!!.hasExtra(TYPE)) {

            val type = data.getStringExtra(TYPE)

            if (type == CHAPTER_SELECTED) {
                goToChapter(data.getStringExtra(SELECTED_CHAPTER_POSITION))

            } else if (type == HIGHLIGHT_SELECTED) {
                val highlightImpl = data.getParcelableExtra<HighlightImpl>(HIGHLIGHT_ITEM)
                currentChapterIndex = highlightImpl.pageNumber
                mFolioPageViewPager!!.currentItem = currentChapterIndex
                val folioPageFragment = currentFragment ?: return
                folioPageFragment.scrollToHighlightId(highlightImpl.rangy)
            } else {
                //For Bookmark
                val readPosition = data.getParcelableExtra<BookmarkImpl>("bookmark")
                goToChapter(readPosition.chapterHref)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (outState != null)
            outState!!.putSerializable(BUNDLE_READ_LOCATOR_CONFIG_CHANGE, lastReadLocator)

        val localBroadcastManager = LocalBroadcastManager.getInstance(this)
        localBroadcastManager.unregisterReceiver(searchReceiver)
        localBroadcastManager.unregisterReceiver(closeBroadcastReceiver)

        if (r2StreamerServer != null)
            r2StreamerServer!!.stop()

        if (isFinishing) {
            localBroadcastManager.sendBroadcast(Intent(FolioReader.ACTION_FOLIOREADER_CLOSED))
            FolioReader.get().retrofit = null
            FolioReader.get().r2StreamerApi = null
        }

        musicSrv!!.stopForegroundNotification()
        if (musicSrv!!.isPng)
            musicSrv!!.stopPlayer()
        unbindService(musicConnection)
    }

    override fun getCurrentChapterIndex(): Int {
        return currentChapterIndex
    }

    private fun configFolio() {

        mFolioPageViewPager = findViewById(R.id.folioPageViewPager)
        // Replacing with addOnPageChangeListener(), onPageSelected() is not invoked
        mFolioPageViewPager!!.setOnPageChangeListener(object : DirectionalViewpager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                Log.v(LOG_TAG, "-> onPageSelected -> DirectionalViewpager -> position = $position")

                isScrolled = false
                scroll = 0

                isChapterChanged = true
                isLoading = false
                isFirst = true
                fab_play_pause.visibility = View.INVISIBLE
                loadProgress.visibility = View.INVISIBLE
                mediaControl.visibility = View.INVISIBLE
                download_audio.visibility = View.INVISIBLE
                audioUrl = "null"

                if (musicSrv != null)
                    if (musicSrv!!.isPng) {//if music playing to change chapter, music stop with is playing mode
                        musicSrv!!.wasPlaying = true
                        musicSrv!!.pausePlayerPlayingNext()
                    } else
                        musicSrv!!.wasPlaying = false

                currentChapterIndex = position

                if (position == 0) {
                    setBookMarkChapter()
                }
                if (mFolioPageFragmentAdapter != null) {
                    if (mFolioPageFragmentAdapter!!.count - 1 == position) {
                        setBookMarkChapter()
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

                if (state == DirectionalViewpager.SCROLL_STATE_IDLE) {
                    val position = mFolioPageViewPager!!.currentItem
                    Log.v(LOG_TAG, "-> onPageScrollStateChanged -> DirectionalViewpager -> " +
                            "position = " + position)

                    if (position == spine!!.size - 1)
                        Toast.makeText(applicationContext, "The book has been completed.", Toast.LENGTH_SHORT).show()

                    var folioPageFragment = mFolioPageFragmentAdapter!!.getItem(position - 1) as FolioPageFragment?
                    if (folioPageFragment != null) {
                        folioPageFragment.scrollToLast()
                        if (folioPageFragment.mWebview != null)
                            folioPageFragment.mWebview!!.dismissPopupWindow()
                    }

                    folioPageFragment = mFolioPageFragmentAdapter!!.getItem(position + 1) as FolioPageFragment?
                    if (folioPageFragment != null) {
                        folioPageFragment.scrollToFirst()
                        if (folioPageFragment.mWebview != null)
                            folioPageFragment.mWebview!!.dismissPopupWindow()
                    }
                }
            }
        })

        mFolioPageViewPager!!.setDirection(direction)
        mFolioPageFragmentAdapter = FolioPageFragmentAdapter(
                supportFragmentManager,
                spine, bookFileName, mBookId
        )
        mFolioPageViewPager!!.adapter = mFolioPageFragmentAdapter

        // In case if SearchActivity is recreated due to screen rotation then FolioActivity
        // will also be recreated, so searchLocator is checked here.
        if (searchLocator != null) {

            currentChapterIndex = getChapterIndex(Constants.HREF, searchLocator!!.href)
            mFolioPageViewPager!!.currentItem = currentChapterIndex
            val folioPageFragment = currentFragment ?: return
            folioPageFragment.highlightSearchLocator(searchLocator!!)
            searchLocator = null

        } else {

            val readLocator: ReadLocator?
            if (savedInstanceState == null) {
                readLocator = intent.getParcelableExtra(FolioActivity.EXTRA_READ_LOCATOR)
                entryReadLocator = readLocator
            } else {
                readLocator = savedInstanceState!!.getParcelable(BUNDLE_READ_LOCATOR_CONFIG_CHANGE)
                lastReadLocator = readLocator
            }
            currentChapterIndex = getChapterIndex(readLocator)
            mFolioPageViewPager!!.currentItem = currentChapterIndex
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(
                searchReceiver,
                IntentFilter(ACTION_SEARCH_CLEAR)
        )
    }

    private fun getChapterIndex(readLocator: ReadLocator?): Int {

        if (readLocator == null) {
            return 0
        } else if (!TextUtils.isEmpty(readLocator.href)) {
            return getChapterIndex(Constants.HREF, readLocator.href)
        }

        return 0
    }

    private fun getChapterIndex(caseString: String, value: String): Int {
        for (i in spine!!.indices) {
            when (caseString) {
                Constants.HREF -> if (spine!![i].href == value)
                    return i
            }
        }
        return 0
    }

    /**
     * If called, this method will occur after onStop() for applications targeting platforms
     * starting with Build.VERSION_CODES.P. For applications targeting earlier platform versions
     * this method will occur before onStop() and there are no guarantees about whether it will
     * occur before or after onPause()
     *
     * @see Activity.onSaveInstanceState
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.v(LOG_TAG, "-> onSaveInstanceState")
        this.outState = outState

        outState.putBoolean(BUNDLE_DISTRACTION_FREE_MODE, distractionFreeMode)
        outState.putBundle(SearchAdapter.DATA_BUNDLE, searchAdapterDataBundle)
        outState.putCharSequence(SearchActivity.BUNDLE_SAVE_SEARCH_QUERY, searchQuery)
    }

    override fun storeLastReadLocator(lastReadLocator: ReadLocator) {
        Log.v(LOG_TAG, "-> storeLastReadLocator")
        this.lastReadLocator = lastReadLocator
    }

    private fun setConfig(savedInstanceState: Bundle?) {
        val intentConfig = intent.getParcelableExtra<Config>(Config.INTENT_CONFIG)
        val overrideConfig = intent.getBooleanExtra(Config.EXTRA_OVERRIDE_CONFIG, false)
        val savedConfig = AppUtil.getSavedConfig(this)

        if (savedInstanceState != null) {
            config = savedConfig

        } else if (savedConfig == null) {
            if (intentConfig == null) {
                config = Config()
            } else {
                config = intentConfig
            }

        } else {
            if (intentConfig != null && overrideConfig) {
                config = intentConfig
            } else {
                config = savedConfig
            }
        }

        // Code would never enter this if, just added for any unexpected error
        // and to avoid lint warning
        if (config == null)
            config = Config()
        if (config!!.brightness == 25)
            config!!.brightness = brightness

        AppUtil.saveConfig(this, config!!)

        val brightnessValue = config!!.brightness
        setScreenBrightness(brightnessValue)

        direction = config!!.direction
        //setScreenBrightness(config.brightness)
        if (direction == Config.Direction.HORIZONTAL)
            bottomLayout.visibility = View.GONE
        else
            bottomLayout.visibility = View.VISIBLE
    }

    private val Context.brightness: Int
        get() {
            return Settings.System.getInt(this.contentResolver,
                    Settings.System.SCREEN_BRIGHTNESS, 0)
        }

    override fun play() {
        //EventBus.getDefault().post(MediaOverlayPlayPauseEvent(spine!![currentChapterIndex].href, true, false))
    }

    override fun pause() {
        //EventBus.getDefault().post(MediaOverlayPlayPauseEvent(spine!![currentChapterIndex].href, false, false))
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.WRITE_EXTERNAL_STORAGE_REQUEST -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setupBook()
            } else {
                Toast.makeText(this, getString(R.string.cannot_access_epub_message), Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    override fun getDirection(): Config.Direction {
        return direction
    }

    private fun clearSearchLocator() {
        Log.v(LOG_TAG, "-> clearSearchLocator")

        val fragments = mFolioPageFragmentAdapter!!.fragments
        for (i in fragments.indices) {
            val folioPageFragment = fragments[i] as FolioPageFragment?
            folioPageFragment?.clearSearchLocator()
        }

        val savedStateList = mFolioPageFragmentAdapter!!.savedStateList
        if (savedStateList != null) {
            for (i in savedStateList.indices) {
                val savedState = savedStateList[i]
                val bundle = FolioPageFragmentAdapter.getBundleFromSavedState(savedState)
                bundle?.putParcelable(FolioPageFragment.BUNDLE_SEARCH_LOCATOR, null)
            }
        }
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val bundle = intent.extras
            if (bundle != null) {
                if (intent.action == AudioDownloadService.RESULT) {
                    audioUrl = File(fileAudioDir.path, bundle.getString(AudioDownloadService.FILE)).absolutePath
                    musicSrv?.setSongUrl(audioUrl)
                    //here
                    Log.d("Download", "Completed")
                    Toast.makeText(this@FolioActivity, bundle.getString(AudioDownloadService.MSG), Toast.LENGTH_SHORT).show()
                } else if (intent.action == AudioDownloadService.FAILED) {
                    if (!distractionFreeMode)
                        download_audio.visibility = View.VISIBLE
                    //here
                    Log.d("Download", "Failed")
                    Toast.makeText(this@FolioActivity, bundle.getString(AudioDownloadService.MSG), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}