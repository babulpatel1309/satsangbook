package us.gurukul.satsangbooks.screens.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.folioreader.model.Book;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Map;
import java.util.Random;

import us.gurukul.satsangbooks.screens.Common;
import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.activity.DetailActivity;

public class MyFirebaseNotificationService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //super.onMessageReceived(remoteMessage);
        if (remoteMessage != null) {
            if (remoteMessage.getData().size() > 0)
                showNotification(remoteMessage.getData());
        }
    }

    private void showNotification(Map<String, String> data) {
        Log.e("showNotification", "data->" + data);
        String title = data.get("title");
        String message = data.get("message");

        Intent intent = new Intent(this, DetailActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        String jsonData = data.get("details");
        Log.e("showNotification", "jsonData->" + jsonData);
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject bookObject = jsonObject.getJSONObject("book");
            Log.e("showNotification", "bookObject->" + bookObject);
            JSONArray tagsArray = bookObject.getJSONArray("tags");
            StringBuilder tag = new StringBuilder();
            for (int j = 0; j < tagsArray.length(); j++) {
                tag.append(tagsArray.getString(j)).append(",");
            }
            Book book = new Book(
                    bookObject.getString("id"),
                    bookObject.getString("added_by"),
                    bookObject.getString("title"),
                    getJsonString(bookObject, "author"),
                    bookObject.getString("front_img"),
                    bookObject.getString("books_file"),
                    getJsonString(bookObject, "description"),
                    getJsonInt(bookObject, "is_book_audio"),
                    getJsonInt(bookObject, "is_kavya"),
                    bookObject.getString("extra_fields"),
                    bookObject.getString("status"),
                    bookObject.getString("book_updated_at"),
                    bookObject.getString("created_at"),
                    bookObject.getString("updated_at"),
                    tag.toString(),
                    //bookObject.getString("tags"),
                    0, 0, 0,
                    bookObject.getInt("border")
            );

            Common.imgPath = jsonObject.getString("bookcover_image_resize");
            Log.e("showNotification", "imgPath->" + Common.imgPath);
            Common.filePath = jsonObject.getString("book_file_path");
            Log.e("showNotification", "filePath->" + Common.filePath);

            intent.putExtra("bookItemId", book.getId());
            intent.putExtra("bookItem", book);
            intent.putExtra("isBookmark", false);
            intent.putExtra("isNotify", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Random r = new Random();
        int notifyID = r.nextInt(30000 - 1) + 1;
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), notifyID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent.getService(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        String channelId = "CHANNEL_ID";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    data.get("title"),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        Notification notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(m)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                //.setOngoing(true)
                .setContentIntent(pendingIntent).build();

        notificationManager.notify(m/* ID of notification */, notificationBuilder);

    }

    public String getJsonString(JSONObject jso, String field) {
        if (jso.isNull(field))
            return "";
        else
            try {
                return jso.getString(field);
            } catch (Exception ex) {
                Log.e("model", "Error parsing value");
                return null;
            }
    }

    public int getJsonInt(JSONObject jso, String field) {
        if (jso.isNull(field))
            return 0;
        else
            try {
                return jso.getInt(field);
            } catch (Exception ex) {
                Log.e("model", "Error parsing value");
                return 0;
            }
    }
}
