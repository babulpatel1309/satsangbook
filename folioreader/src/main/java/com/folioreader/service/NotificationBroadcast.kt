package com.folioreader.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.folioreader.ui.activity.FolioActivity

class NotificationBroadcast : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.e("onReceive", intent.action)

        when (intent.action) {
            MusicService.NOTIFY_PLAY_TOGGLE -> FolioActivity.musicSrv!!.setPlayPause()
            MusicService.NOTIFY_NEXT -> FolioActivity.musicSrv!!.playNext()
            MusicService.NOTIFY_PREVIOUS -> FolioActivity.musicSrv!!.playPrev()
            MusicService.NOTIFY_DELETE -> FolioActivity.musicSrv!!.stopForegroundNotification()
        }
    }
}
