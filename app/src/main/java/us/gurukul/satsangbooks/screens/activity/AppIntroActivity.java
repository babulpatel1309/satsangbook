package us.gurukul.satsangbooks.screens.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;

import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.fragments.AppIntroFragment;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        String welcome = getResources().getString(R.string.welcome);
        String home_page = getResources().getString(R.string.home_page);
        String book_filter = getResources().getString(R.string.book_filter);
        String book_download = getResources().getString(R.string.book_download);
        String book_detail = getResources().getString(R.string.book_detail);
        String search_bookmark = getResources().getString(R.string.search_bookmark);
        String note_copy_share = getResources().getString(R.string.note_copy_share);
        String index_highlight = getResources().getString(R.string.index_highlight);
        String book_setting = getResources().getString(R.string.book_setting);
        String audio_scroll = getResources().getString(R.string.audio_scroll);
        //Log.e("AppIntroActivity", "str-> " + str);
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, welcome, R.drawable.welcome));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, home_page, R.drawable.home_page));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, book_filter, R.drawable.book_filter));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, book_download, R.drawable.book_download));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, book_detail, R.drawable.book_detail));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, search_bookmark, R.drawable.search_bookmark));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, note_copy_share, R.drawable.note_copy_share));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, index_highlight, R.drawable.index_highlights));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, book_setting, R.drawable.book_setting));
        addSlide(AppIntroFragment.newInstance(R.layout.intro_custom_layout, audio_scroll, R.drawable.audio_scroll));

        setSeparatorColor(Color.GRAY);
        setBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent = getIntent();
        if (intent.hasExtra("bookTags")) {
            showMain();
        } else
            finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = getIntent();
        if (intent.hasExtra("bookTags")) {
            showMain();
        } else
            finish();
    }

    private void showMain() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("bookTags", getIntent().getStringArrayListExtra("bookTags"));
        intent.putExtra("pages", getIntent().getParcelableArrayListExtra("pages"));
        intent.putExtra("app_version", getIntent().getStringExtra("app_version"));
        intent.putExtra("adminEmail", getIntent().getStringExtra("adminEmail"));
        intent.putExtra("email", getIntent().getStringExtra("email"));
        intent.putExtra("name", getIntent().getStringExtra("name"));
        startActivity(intent);
        finish();
    }
}