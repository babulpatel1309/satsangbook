package us.gurukul.satsangbooks.screens.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.model.BookmarkImpl;

import java.util.ArrayList;

import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.listener.RecyclerViewClickListener;


public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<BookmarkImpl> bookmarkList;
    private RecyclerViewClickListener listener;

    public void setListener(RecyclerViewClickListener listener) {
        this.listener = listener;
    }

    public BookmarkAdapter(Context mContext, ArrayList<BookmarkImpl> bookList) {
        this.mContext = mContext;
        this.bookmarkList = bookList;
    }

    public void remove(int position) {
        bookmarkList.remove(position);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_book_mark, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final BookmarkImpl book = getItem(position);
        Log.e("Search", book.getValue());

        holder.name.setText(book.getBookId());
        holder.chapter.setText(book.getValue());

        holder.delete_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemDelete(position);
                }
            }
        });

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemRemoveFav(position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return bookmarkList.size();
    }

    public BookmarkImpl getItem(int position) {
        return bookmarkList.get(position);
    }

    // Class For Holder
    class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout container;
        TextView name;
        TextView chapter;
        ImageView delete_item;

        MyViewHolder(View view) {
            super(view);
            container = view.findViewById(R.id.lyt_container);
            name = view.findViewById(R.id.book_title);
            chapter = view.findViewById(R.id.chapter_title);
            delete_item = view.findViewById(R.id.delete_item);
        }
    }

}
