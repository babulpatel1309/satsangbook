package us.gurukul.satsangbooks.screens.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.model.Book;
import com.folioreader.model.sqlite.BookTable;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;

import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.adapter.BooksAdapter;
import us.gurukul.satsangbooks.screens.listener.FragmentListener;
import us.gurukul.satsangbooks.screens.listener.RecyclerViewClickListener;

import static us.gurukul.satsangbooks.screens.activity.MainActivity.adminEmail;
import static us.gurukul.satsangbooks.screens.activity.MainActivity.email;
import static us.gurukul.satsangbooks.screens.activity.MainActivity.listing;

public class MyBooksFragment extends Fragment {

    private static final String TAG = "MyBooksFragment";
    private Context context;
    private RecyclerView rv_my_books;
    private FragmentListener mListener;
    private BooksAdapter booksAdapter;
    private boolean[] Selectedtruefalse;
    private ArrayList<String> filterListing = new ArrayList<>();
    private View view;

    public MyBooksFragment() {
        // Required empty public constructor
        Log.e(TAG, "Constructor-> ");
    }

    public static MyBooksFragment newInstance() {
        return new MyBooksFragment();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach-> ");
        try {
            this.context = context;
            if (context instanceof FragmentListener) {
                //init the listener
                mListener = (FragmentListener) context;
            }
        } catch (ClassCastException ex) {
            Log.e(TAG, "Fragment-> Error to set listener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.e(TAG, "onCreate-> ");
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView-> ");
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_books, container, false);
        rv_my_books = view.findViewById(R.id.rv_my_books);
        rv_my_books.setLayoutManager(new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false));

        Selectedtruefalse = new boolean[listing.size()];

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume-> ");
        loadData(filterListing);
    }

    private void loadData(ArrayList<String> filter) {
        final ArrayList<Book> books = new ArrayList<>(BookTable.getFilterBook(email, adminEmail, filter, 1));
        Log.e(TAG, "loadData books-> " + books);
        if (books.isEmpty())
            view.findViewById(R.id.tv_error).setVisibility(View.VISIBLE);
        else
            view.findViewById(R.id.tv_error).setVisibility(View.INVISIBLE);

        booksAdapter = new BooksAdapter(getActivity(), books);
        rv_my_books.setAdapter(booksAdapter);
        booksAdapter.setListener(new RecyclerViewClickListener() {
            @Override
            public void onItemClick(Book book) {
                mListener.setOnClickItem(book);
            }

            @Override
            public void onItemRemoveFav(int pos) {
                Log.e(TAG, "onItemRemoveFav-> ");
            }

            @Override
            public void onItemDelete(int pos) {
                books.remove(pos);

                if (books.isEmpty())
                    view.findViewById(R.id.tv_error).setVisibility(View.VISIBLE);
                else
                    view.findViewById(R.id.tv_error).setVisibility(View.INVISIBLE);
            }

            @Override
            public void onExtraItem(String url) {
                mListener.setExtraField(url);
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            Log.e(TAG, "setUserVisibleHint-> ");
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_all_book, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == R.id.action_filter) {
            showDialogSortMyBook();
            return true;
        }

        return false;
    }

    private void showDialogSortMyBook() {
        final CharSequence[] items = listing.toArray(new CharSequence[0]);
        int i = 0;
        for (String s : listing) {
            Selectedtruefalse[i] = filterListing.contains(s);
            i++;
        }

        Log.e(TAG, "showDialogSortMyBook-> " + Arrays.toString(items));
        final AlertDialog dialog = new AlertDialog.Builder(context, R.style.StyledDialog)
                .setTitle(getResources().getString(R.string.text_title_dialog_sort_by_book))
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton("Reset", null)
                .setNeutralButton("Select All", null)
                .setMultiChoiceItems(items, Selectedtruefalse, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                    }
                })
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // Ok
                        filterListing.clear();
                        for (int i = 0; i < listing.size(); i++) {
                            if (Selectedtruefalse[i]) {
                                filterListing.add(items[i].toString());
                            }
                        }

                        loadData(filterListing);

                        //Dismiss once everything is OK.
                        dialog.dismiss();
                    }
                });
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // Select All
                        for (int i = 0; i < items.length; i++) {
                            dialog.getListView().setItemChecked(i, false);
                            Selectedtruefalse[i] = false;
                        }

                    }
                });
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // Reset
                        for (int i = 0; i < items.length; i++) {
                            dialog.getListView().setItemChecked(i, true);
                            Selectedtruefalse[i] = true;
                        }
                    }
                });
            }
        });

        dialog.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
