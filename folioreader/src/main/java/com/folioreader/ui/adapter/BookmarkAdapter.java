package com.folioreader.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.R;
import com.folioreader.model.BookmarkImpl;
import com.folioreader.ui.base.BookmarkListener;

import java.util.ArrayList;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.MyViewHolder> {

    private ArrayList<BookmarkImpl> bookmarkList;
    private BookmarkListener listener;

    public void setListener(BookmarkListener listener) {
        this.listener = listener;
    }

    public BookmarkAdapter(Context mContext, ArrayList<BookmarkImpl> bookList) {
        this.bookmarkList = bookList;
    }

    public void remove(int position) {
        bookmarkList.remove(position);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_book_mark, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final BookmarkImpl bookmark = getItem(position);
        Log.e("Search", bookmark.getValue());

        holder.name.setText(bookmark.getBookId());
        holder.chapter.setText(bookmark.getValue());

        holder.delete_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemDelete(position);
                }
            }
        });

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(bookmark);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return bookmarkList.size();
    }

    public BookmarkImpl getItem(int position) {
        return bookmarkList.get(position);
    }

    // Class For Holder
    class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout container;
        TextView name;
        TextView chapter;
        ImageView delete_item;

        MyViewHolder(View view) {
            super(view);
            container = view.findViewById(R.id.lyt_container);
            name = view.findViewById(R.id.book_title);
            chapter = view.findViewById(R.id.chapter_title);
            delete_item = view.findViewById(R.id.delete_item);
        }
    }

}
