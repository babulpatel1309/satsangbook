package us.gurukul.satsangbooks.screens.listener;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import us.gurukul.satsangbooks.screens.model.LoginModel;

public interface RetrofitInterface {

    @GET("v1/books/list")
    @Streaming
    Call<Object> getBookData();

    @GET()
    @Streaming
    Call<LoginModel> loginUsingPhoneOrEmail(@Url String loginURL);

    @POST("v1/default/login")
    @FormUrlEncoded
    @Streaming
    Call<ResponseBody> loginData(
            @Field("name") String name,
            @Field("email") String email,
            @Field("token") String token);

    @POST("v1/feedback/add")
    @FormUrlEncoded
    @Streaming
    Call<ResponseBody> feedbackData(
            @Field("email") String email,
            @Field("type") String type,
            @Field("mobile") String mobile,
            @Field("comment") String comment,
            @Field("device_details") String device_details);
}