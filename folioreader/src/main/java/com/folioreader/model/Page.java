package com.folioreader.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Page implements Parcelable {
    private String id;
    private String slug;
    private String title;
    private String html;
    private String created_at;
    private String updated_at;

    public Page(String id, String slug, String title, String html, String created_at, String updated_at) {
        this.id = id;
        this.slug = slug;
        this.title = title;
        this.html = html;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    private Page(Parcel in) {
        id = in.readString();
        slug = in.readString();
        title = in.readString();
        html = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<Page> CREATOR = new Creator<Page>() {
        @Override
        public Page createFromParcel(Parcel in) {
            return new Page(in);
        }

        @Override
        public Page[] newArray(int size) {
            return new Page[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(slug);
        dest.writeString(title);
        dest.writeString(html);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
