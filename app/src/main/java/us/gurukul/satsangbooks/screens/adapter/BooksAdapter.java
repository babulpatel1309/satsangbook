package us.gurukul.satsangbooks.screens.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.model.Book;
import com.folioreader.model.ExtraModel;
import com.folioreader.model.sqlite.BookTable;
import com.folioreader.model.sqlite.DbAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import us.gurukul.satsangbooks.screens.Common;
import us.gurukul.satsangbooks.screens.NetworkHelper;
import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.listener.RecyclerViewClickListener;

import static us.gurukul.satsangbooks.screens.Common.fileDir;


public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.MyViewHolder> {

    private RecyclerViewClickListener listener;
    private Context mContext;
    private ArrayList<Book> bookList;
    private ArrayList<Book> sortedList;

    public void setListener(RecyclerViewClickListener listener) {
        this.listener = listener;
    }

    public BooksAdapter(Context mContext, ArrayList<Book> bookList) {
        this.mContext = mContext;
        this.bookList = bookList;
        sortedList = bookList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_book_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Book book = getItem(position);
        if (book.getIs_book_audio() == 1)
            holder.audioIcon.setVisibility(View.VISIBLE);
        holder.book_title.setText(book.getTitle());
        // loading album cover using Glide library
        final String imgUrl = Common.imgPath + "320x420/" + book.getFront_img();
        //Log.e("TAG", "imgUrl-> " + imgUrl);

        Picasso.get()
                .load(imgUrl)
                .placeholder(R.drawable.white_bg) // can also be a drawable
                .into(holder.book_image, new Callback() {
                    @Override
                    public void onSuccess() {
                        // once the image is loaded, load the next image
                        //Log.e("TAG", "onSuccess");
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("BooksAdapter", "onError->" + e.getMessage());
                        Picasso.get().invalidate(imgUrl);
                        Picasso.get()
                                .load(imgUrl)
                                .networkPolicy(NetworkHelper.isOnline(mContext) ? NetworkPolicy.NO_CACHE : NetworkPolicy.OFFLINE)
                                .placeholder(R.drawable.white_bg) // can also be a drawable
                                .into(holder.book_image, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        // once the image is loaded, load the next image
                                        //Log.e("TAG", "onSuccess");
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Log.e("BooksAdapter", "onError->" + e.getMessage());
                                        Picasso.get().invalidate(imgUrl);
                                        Picasso.get()
                                                .load(imgUrl)
                                                .placeholder(R.drawable.white_bg) // but don't clear the imageview or set a placeholder; just leave the previous image in until the new one is ready
                                                .into(holder.book_image);
                                    }
                                });
                    }
                });

        if (book.getIs_downloaded() == 1) {
            holder.icon_download.setImageResource(R.drawable.ic_more_vert_white_24dp);
        } else {
            holder.icon_download.setImageResource(R.drawable.ic_cloud_download_white_24dp);
        }
        if ((position + 1) % 2 == 0)
            holder.rel_bgItem.setScaleX(-1f);

        holder.book_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "Pos " + holder.getAdapterPosition());
                if (listener != null) {
                    listener.onItemClick(book);
                }
            }
        });

        holder.icon_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (book.getIs_downloaded() == 1) {
                    PopupMenu popup = new PopupMenu(mContext, holder.icon_download);
                    popup.getMenuInflater().inflate(R.menu.item_popup_menu, popup.getMenu());
                    MenuItem menuItem = popup.getMenu().findItem(R.id.fav_item);
                    final ArrayList<ExtraModel> extraArray = new ArrayList<>();
                    try {
                        JSONArray jsonArray = new JSONArray(book.getExtraArray());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject extraObject = jsonArray.getJSONObject(i);
                            if (!extraObject.getString("title").equals("") && !extraObject.getString("value").equals("")) {
                                extraArray.add(new ExtraModel(extraObject.getString("title"), extraObject.getString("value")));
                                popup.getMenu().add(R.id.extra_item, Integer.parseInt(book.getId()), i, extraArray.get(i).getTitle());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (book.getIs_fav() == 1) {
                        menuItem.setTitle(mContext.getString(R.string.remove_fav));
                    } else
                        menuItem.setTitle(mContext.getString(R.string.add_fav));

                    popup.show();

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            if (menuItem.getItemId() == R.id.fav_item) {
                                if (menuItem.getTitle() == mContext.getString(R.string.add_fav)) {
                                    Log.e("TAG", "Add Fav");
                                    Toast.makeText(mContext, mContext.getString(R.string.add_fav), Toast.LENGTH_SHORT).show();
                                    book.setIs_fav(1);
                                } else {
                                    Log.e("TAG", "remove Fav");
                                    Toast.makeText(mContext, mContext.getString(R.string.remove_fav), Toast.LENGTH_SHORT).show();
                                    book.setIs_fav(0);
                                    listener.onItemRemoveFav(position);//use for fav Activity
                                }
                                BookTable.updatedBook(book); //update Fav
                            } else if (menuItem.getItemId() == R.id.share_item) {
                                shareBook(book);
                            } else if (menuItem.getItemId() == R.id.delete_item) {
                                bookDeletedAlert(book, position);
                            } else {
                                Log.e("TAG", extraArray.get(menuItem.getOrder()).getValue());
                                listener.onExtraItem(extraArray.get(menuItem.getOrder()).getValue());
                            }
                            return false;
                        }
                    });
                } else {
                    Log.e("TAG", "click on download");
                    if (listener != null) {
                        listener.onItemClick(book);
                    }
                }
            }
        });

    }

    private void bookDeletedAlert(final Book book, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Do you want to delete this Book?");
        builder.setMessage("if yes, your favourite, bookmark and highlight data will be deleted.");

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeFile(book.getBooks_file());

                DbAdapter.removeAllBookmarkChapter(book.getBooks_file());
                DbAdapter.removeAllSeenChapter(book.getBooks_file());
                DbAdapter.removeAllReadLocator(book.getBooks_file());
                //For highlight
                String fileName = book.getBooks_file();
                int pos = fileName.lastIndexOf(".");
                if (pos > 0 && pos < (fileName.length() - 1)) { // If '.' is not the first or last character.
                    fileName = fileName.substring(0, pos);
                }
                DbAdapter.removeAllHighLight(fileName);

                book.setIs_downloaded(0);
                book.setIs_fav(0);
                BookTable.updatedBook(book); //update book

                listener.onItemDelete(position);//use for fragment and fav Activity

                notifyDataSetChanged();
            }
        });

        Dialog sortDialog = builder.create();
        sortDialog.show();
    }

    public List<Book> getList() {
        return sortedList;
    }

    private void shareBook(Book book) {
        Intent localIntent = new Intent(Intent.ACTION_SEND);
        localIntent.setType("text/plain");
        String app_name = mContext.getString(R.string.app_name);
        localIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, app_name);
        localIntent.putExtra(Intent.EXTRA_TEXT,
                book.getTitle() +
                        " Book\nDownload this awesome app is '"
                        + app_name
                        + "' from \nhttps://play.google.com/store/apps/details?id="
                        + mContext.getPackageName()
                        + " Refer book and shared with you...\n");
        mContext.startActivity(Intent.createChooser(localIntent, "Share Via"));
    }

    private void removeFile(String file_name) {
        File src = new File(fileDir, file_name);
        if (src.exists()) {
            if (src.delete())
                Log.e("TAG", "deleted file");
            else
                Log.e("TAG", "can not delete file");
        }
    }

    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    public Book getItem(int position) {
        return sortedList.get(position);
    }

    // Class For Holder
    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView rel_bgItem;
        ImageView book_image;
        TextView book_title;
        ImageView icon_download;
        ImageView audioIcon;

        MyViewHolder(View view) {
            super(view);
            rel_bgItem = view.findViewById(R.id.img_bg_item);
            book_title = view.findViewById(R.id.book_title);
            book_image = view.findViewById(R.id.book_image);
            icon_download = view.findViewById(R.id.icon_download);
            audioIcon = view.findViewById(R.id.audioIcon);
        }
    }
}
