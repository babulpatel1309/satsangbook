package com.folioreader.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.Config;
import com.folioreader.FolioReader;
import com.folioreader.R;
import com.folioreader.model.BookmarkImpl;
import com.folioreader.model.sqlite.BookMarkTable;
import com.folioreader.ui.adapter.BookmarkAdapter;
import com.folioreader.ui.base.BookmarkListener;
import com.folioreader.util.AppUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static com.folioreader.Constants.BOOKMARK_SELECTED;
import static com.folioreader.Constants.TYPE;

public class BookmarkFragment extends Fragment {
    private View mRootView;

    public static BookmarkFragment newInstance(String bookId) {
        BookmarkFragment highlightFragment = new BookmarkFragment();
        Bundle args = new Bundle();
        args.putString(FolioReader.EXTRA_BOOK_ID, bookId);
        highlightFragment.setArguments(args);
        return highlightFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_bookmark_list, container, false);
        return mRootView;
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView bookmark_warning = mRootView.findViewById(R.id.bookmark_warning);
        RecyclerView rv_bookmark = mRootView.findViewById(R.id.rv_bookmark);
        rv_bookmark.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));

        String mBookId = getArguments().getString(FolioReader.EXTRA_BOOK_ID);
        ArrayList<BookmarkImpl> bookmarks = BookMarkTable.getAllBookmark(mBookId);
        if (bookmarks.isEmpty()) {
            bookmark_warning.setVisibility(View.VISIBLE);
        } else
            bookmark_warning.setVisibility(View.INVISIBLE);

        final BookmarkAdapter bookmarkAdapter = new BookmarkAdapter(getActivity(), bookmarks);
        rv_bookmark.setAdapter(bookmarkAdapter);

        bookmarkAdapter.setListener(new BookmarkListener() {
            @Override
            public void onItemClick(BookmarkImpl bookmark) {
                Intent intent = new Intent();
                intent.putExtra("bookmark", bookmark);
                intent.putExtra(TYPE, BOOKMARK_SELECTED);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }

            @Override
            public void onItemDelete(int pos) {
                BookMarkTable.saveBookmarkIfNotExists(bookmarkAdapter.getItem(pos));//here remove bookmark
                bookmarkAdapter.remove(pos);
                if (bookmarkAdapter.getItemCount() == 0)
                    mRootView.findViewById(R.id.bookmark_warning).setVisibility(View.VISIBLE);
            }
        });

        Config config = AppUtil.getSavedConfig(getActivity());
        if (config != null && config.isNightMode()) {
            if (getActivity() != null) {
                rv_bookmark.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.night));
                bookmark_warning.setTextColor(ContextCompat.getColor(getActivity(), R.color.app_gray));
            }
        }

    }

}


