package us.gurukul.satsangbooks.screens.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.folioreader.model.Book;
import com.folioreader.model.Page;
import com.folioreader.model.sqlite.BookTable;
import com.folioreader.model.sqlite.DbAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import us.gurukul.satsangbooks.screens.Common;
import us.gurukul.satsangbooks.screens.NetworkHelper;
import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.listener.RetrofitInterface;

import static us.gurukul.satsangbooks.screens.Common.BASE_URL;

public class SplashActivity extends AppCompatActivity {

    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        pref = getSharedPreferences("SatsangBook", Context.MODE_PRIVATE);
        if (NetworkHelper.isOnline(this))
            initDownload();
        else {
            String str = pref.getString("app_data", "");
            if (str != null && str.equals("")) {
                findViewById(R.id.loadProgress).setVisibility(View.GONE);
                findViewById(R.id.tv_error).setVisibility(View.VISIBLE);
                final Button start = findViewById(R.id.btn_start);
                start.setVisibility(View.VISIBLE);
                start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetworkHelper.isOnline(SplashActivity.this)) {
                            findViewById(R.id.loadProgress).setVisibility(View.VISIBLE);
                            findViewById(R.id.tv_error).setVisibility(View.GONE);
                            start.setVisibility(View.GONE);
                            initDownload();
                        } else
                            Toast.makeText(SplashActivity.this, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        callMainActivity(pref.getString("app_data", ""));
                    }
                }, 1000);
            }

        }

    }

    private void initDownload() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<Object> request = retrofitInterface.getBookData();

        request.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.body() != null) {
                    Log.e("Response body", response.body().toString());
                    String gson = new Gson().toJson(response.body());
                    Log.e("Response Gson", gson);
                    pref.edit().putString("app_data", gson).apply();
                    callMainActivity(gson);
                } else {
                    Log.e("Response", "null");
                    Toast.makeText(SplashActivity.this, "Network Issue", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e("onFailure", "apiData-> " + t.getMessage());
                Toast.makeText(SplashActivity.this, "Network Issue", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void callMainActivity(final String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);//org.json.JSONException:
            if (jsonObject.getBoolean("success")) {
                final String st = jsonObject.getString("data");
                JSONArray bookArray = new JSONObject(st).getJSONArray("books");
                if (bookArray.length() > 0) {
                    for (int i = 0; i < bookArray.length(); i++) {
                        JSONObject bookObject = bookArray.getJSONObject(i);
                        Log.e("JSONObject", "" + bookObject.toString());
                        JSONArray tagsArray = bookObject.getJSONArray("tags");
                        StringBuilder tag = new StringBuilder();
                        for (int j = 0; j < tagsArray.length(); j++) {
                            tag.append(tagsArray.getString(j)).append(",");
                        }
                        Book book = new Book(
                                bookObject.getString("id"),
                                bookObject.getString("added_by"),
                                bookObject.getString("title"),
                                getJsonString(bookObject, "author"),
                                bookObject.getString("front_img"),
                                bookObject.getString("books_file"),
                                getJsonString(bookObject, "description"),
                                getJsonInt(bookObject, "is_book_audio"),
                                getJsonInt(bookObject, "is_kavya"),
                                bookObject.getString("extra_fields"),
                                bookObject.getString("status"),
                                bookObject.getString("book_updated_at"),
                                bookObject.getString("created_at"),
                                bookObject.getString("updated_at"),
                                tag.toString(),
                                0, 0, 0,
                                bookObject.getInt("border"));

                        BookTable.saveBookIfNotExists(book);
                    }
                }

                //bookTags
                JSONArray bookTagsArray = new JSONObject(st).getJSONArray("bookTags");
                final ArrayList<String> bookTags = new ArrayList<>();
                for (int i = 0; i < bookTagsArray.length(); i++) {
                    JSONObject tagObject = bookTagsArray.getJSONObject(i);
                    bookTags.add(tagObject.getString("title"));
                }
                Log.e("bookTags", "" + bookTags);

                Common.imgPath = new JSONObject(st).getString("bookcover_image_resize");
                Common.filePath = new JSONObject(st).getString("book_file_path");
                final String appVersion = new JSONObject(st).getString("app-version");
                final String adminEmail = new JSONObject(st).getString("admin-email");
                Log.e("adminEmail", "" + adminEmail);

                //pages
                JSONArray pageArray = new JSONObject(st).getJSONArray("pages");
                final ArrayList<Page> pages = new ArrayList<>();
                if (pageArray.length() > 0) {
                    for (int i = 0; i < pageArray.length(); i++) {
                        JSONObject pageObject = pageArray.getJSONObject(i);
                        pages.add(new Page(
                                pageObject.getString("id"),
                                pageObject.getString("slug"),
                                pageObject.getString("title"),
                                pageObject.getString("html"),
                                pageObject.getString("created_at"),
                                pageObject.getString("updated_at")
                        ));
                    }
                }

                new Handler()
                        .postDelayed(new Runnable() {
                                         @Override
                                         public void run() {
                                             Intent intent;
                                             if (pref.getBoolean("isAppIntro",true)) {
                                                 intent = new Intent(SplashActivity.this, AppIntroActivity.class);
                                                 pref.edit().putBoolean("isAppIntro",false).apply();
                                             }else {
                                                 intent = new Intent(SplashActivity.this, MainActivity.class);
                                             }
                                             intent.putExtra("bookTags", bookTags);
                                             intent.putExtra("pages", pages);
                                             intent.putExtra("app_version", appVersion);
                                             intent.putExtra("adminEmail", adminEmail);
                                             intent.putExtra("email", getIntent().getStringExtra("email"));
                                             intent.putExtra("name", getIntent().getStringExtra("name"));
                                             startActivity(intent);
                                             finish();
                                         }
                                     }, 1000

                        );
            } else {
                Log.e("onFailure", "true");
                Toast.makeText(SplashActivity.this, "Data not found, please try again...", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    public String getJsonString(JSONObject jso, String field) {
        if (jso.isNull(field))
            return "";
        else
            try {
                return jso.getString(field);
            } catch (Exception ex) {
                Log.e("model", "Error parsing value");
                return null;
            }
    }

    public int getJsonInt(JSONObject jso, String field) {
        if (jso.isNull(field))
            return 0;
        else
            try {
                return jso.getInt(field);
            } catch (Exception ex) {
                Log.e("model", "Error parsing value");
                return 0;
            }
    }
}
