package us.gurukul.satsangbooks.screens;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.folioreader.model.sqlite.DbAdapter;
import com.folioreader.util.Prefs;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class SampleApp extends Application {

    public Prefs pref;
    private static SampleApp appInstace;
    public static String token = "";
    private static final String TAG = "SampleAppClass";

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        appInstace = this;
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

        //FirebaseApp is initialized
        FirebaseApp.initializeApp(this);

        DbAdapter.initialize(this);

        pref = new Prefs(getSharedPreferences("SatsangBook", Context.MODE_PRIVATE));

        //Get token
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
                Log.d(TAG, "token->" + token);
            }
        });

    }

    public static synchronized SampleApp getInstance() {
        return appInstace;
    }


}