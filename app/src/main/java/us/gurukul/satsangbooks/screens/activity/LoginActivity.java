package us.gurukul.satsangbooks.screens.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.folioreader.util.Prefs;
import com.folioreader.util.ProgressDialog;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.hbb20.CountryCodePicker;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import us.gurukul.satsangbooks.screens.Common;
import us.gurukul.satsangbooks.screens.NetworkHelper;
import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.SampleApp;
import us.gurukul.satsangbooks.screens.model.LoginModel;

import static us.gurukul.satsangbooks.screens.Common.PREF_USER_EMAIL;
import static us.gurukul.satsangbooks.screens.Common.PREF_USER_NAME;

public class LoginActivity extends BaseActivity {

    private static final String TAG = "GoogleSignInActivity";
    private GoogleSignInClient googleSignInClient;
    private int SIGN_IN = 30;

    private SignInButton signInButton;
    private CardView btnLogin;
    private EditText etEmailAddress, etPhoneNumber;
    private Context mContext;
    private CountryCodePicker ccp;
    private Prefs preferences;

    @Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        onLoggedIn(account);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_redesign);

        mContext = this;

        preferences = SampleApp.getInstance().pref;

        checkExistingUser();

        initSignIn();
        buttonClicks();
    }

    private void buttonClicks() {
        signInButton = findViewById(R.id.sign_in_button);
        btnLogin = findViewById(R.id.btnLogin);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        etEmailAddress = findViewById(R.id.etEmailAddress);
        ccp = findViewById(R.id.ccp);
        ccp.registerCarrierNumberEditText(etPhoneNumber);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkHelper.isOnline(LoginActivity.this)) {
                    signIn();
                } else {
                    Toast.makeText(LoginActivity.this, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    etEmailAddress.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etEmailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    etPhoneNumber.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {

                    final Dialog dialog = ProgressDialog.show(mContext, "Logging you in...");

                    callWS().loginUsingPhoneOrEmail(loginURL).enqueue(new Callback<LoginModel>() {
                        @Override
                        public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                            dialog.dismiss();
                            if (response.body() != null && response.isSuccessful()) {
                                redirectUserToDashboard(response.body().getData().getEmail(), response.body().getData().getFirstName());
                            } else {
                                Toast.makeText(mContext, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<LoginModel> call, Throwable t) {
                            dialog.dismiss();
                            Toast.makeText(mContext, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void initSignIn() {
        //Sign In with Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    // [START signIn]
    private void signIn() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, SIGN_IN);
    }

    private void onLoggedIn(GoogleSignInAccount googleSignInAccount) {
        if (googleSignInAccount != null) {
            Log.d(TAG, "Already Logged In");
            findViewById(R.id.sign_in_button).setVisibility(View.INVISIBLE);

            String userEmail = googleSignInAccount.getEmail();
            String name = googleSignInAccount.getDisplayName();

            redirectUserToDashboard(userEmail, name);
        } else {
            Log.d(TAG, "Not logged in");
            if (NetworkHelper.isOnline(this)) {
                findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//                signIn();
            } else
                Log.d(TAG, "No intenet connection");
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            //Signed in successfully, show authenticated UI.
            onLoggedIn(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e(TAG, "signInResult:failed code=" + e.getStatusCode());
            onLoggedIn(null);
        }
    }


    private void redirectUserToDashboard(String userEmail, String name) {

        if (userEmail == null) userEmail = "Null";
        if (name == null) name = "Null";

        preferences.setStringDetail(PREF_USER_EMAIL, userEmail);
        preferences.setStringDetail(PREF_USER_NAME, name);

        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("email", userEmail);
        intent.putExtra("name", name);
        startActivity(intent);
        finish();
    }

    private void checkExistingUser() {
        if (!preferences.getStringDetail(PREF_USER_EMAIL, "").trim().isEmpty()) {
            redirectUserToDashboard(preferences.getStringDetail(PREF_USER_EMAIL, ""), preferences.getStringDetail(PREF_USER_NAME, ""));
        }
    }

    String loginURL = "";

    private boolean validate() {

        String emailAddress = etEmailAddress.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();

        if (emailAddress.isEmpty() && phoneNumber.isEmpty()) {
            Toast.makeText(mContext, "Please enter email address or phone number", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!emailAddress.isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()) {
            Toast.makeText(mContext, "Please enter valid email address", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!phoneNumber.isEmpty() && !ccp.isValidFullNumber()) {
            Toast.makeText(mContext, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!emailAddress.isEmpty()) {
            loginURL = Common.BASE_URL_API + Common.emailURL + emailAddress;
        }

        if (!phoneNumber.isEmpty()) {
            loginURL = Common.BASE_URL_API + Common.emailURL + phoneNumber;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SIGN_IN) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            }
        } else {
            Log.d(TAG, "signInResult:failed-> " + data);
        }
    }
}
