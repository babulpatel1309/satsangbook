package com.folioreader.model.sqlite;

import android.content.ContentValues;
import android.database.Cursor;

import com.folioreader.model.Book;

import java.util.ArrayList;

public class BookTable {
    public static final String TABLE_NAME = "book_table";
    public static final String ID = "_id";
    public static final String COL_BOOK_ID = "id";
    private static final String COL_ADDED_BY = "added_by";
    private static final String COL_TITLE = "title";
    private static final String COL_AUTHOR = "author";
    private static final String COL_FRONT_IMG = "front_img";
    private static final String COL_BOOK_FILE = "books_file";
    private static final String COL_DESCRIPTION = "description";
    private static final String COL_IS_BOOK_AUDIO = "is_book_audio";
    private static final String COL_IS_BOOK_KAVYA = "is_book_kavya";
    private static final String COL_EXTRA_FIELDS = "extra_fields";
    public static final String COL_STATUS = "status";
    public static final String COL_BOOK_UPDATED_AT = "book_updated_at";
    private static final String COL_CREATED_AT = "created_at";
    public static final String COL_UPDATED_AT = "updated_at";
    public static final String COL_TAGS = "tags";
    public static final String COL_DOWNLOADED = "downloaded";
    public static final String COL_FAV = "fav";
    public static final String COL_B_ORDER = "b_order";
    private static final String COL_UPDATE_BOOK = "is_update_book";

    public static final String SQL_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( " + ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT" + ","
            + COL_BOOK_ID + " TEXT" + ","
            + COL_ADDED_BY + " TEXT" + ","
            + COL_TITLE + " TEXT" + ","
            + COL_AUTHOR + " TEXT" + ","
            + COL_FRONT_IMG + " TEXT" + ","
            + COL_BOOK_FILE + " TEXT" + ","
            + COL_DESCRIPTION + " TEXT" + ","
            + COL_IS_BOOK_AUDIO + " INTEGER" + ","
            + COL_IS_BOOK_KAVYA + " INTEGER" + ","
            + COL_EXTRA_FIELDS + " TEXT" + ","
            + COL_STATUS + " TEXT" + ","
            + COL_BOOK_UPDATED_AT + " TEXT" + ","
            + COL_CREATED_AT + " TEXT" + ","
            + COL_UPDATED_AT + " TEXT" + ","
            + COL_TAGS + " TEXT" + ","
            + COL_DOWNLOADED + " INTEGER" + ","
            + COL_FAV + " INTEGER" + ","
            + COL_UPDATE_BOOK + " INTEGER" + ","
            + COL_B_ORDER  + " INTEGER" + ")";

    public static ContentValues getBookContentValues(Book book) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_BOOK_ID, book.getId());
        contentValues.put(COL_ADDED_BY, book.getAdded_by());
        contentValues.put(COL_TITLE, book.getTitle());
        contentValues.put(COL_AUTHOR, book.getAuthor());
        contentValues.put(COL_FRONT_IMG, book.getFront_img());
        contentValues.put(COL_BOOK_FILE, book.getBooks_file());
        contentValues.put(COL_DESCRIPTION, book.getDescription());
        contentValues.put(COL_IS_BOOK_AUDIO, book.getIs_book_audio());
        contentValues.put(COL_IS_BOOK_KAVYA, book.getIs_book_kavya());
        contentValues.put(COL_EXTRA_FIELDS, book.getExtraArray());
        contentValues.put(COL_STATUS, book.getStatus());
        contentValues.put(COL_BOOK_UPDATED_AT, book.getBook_updated_at());
        contentValues.put(COL_CREATED_AT, book.getCreated_at());
        contentValues.put(COL_UPDATED_AT, book.getUpdated_at());
        contentValues.put(COL_TAGS, book.getTagsArray());
        contentValues.put(COL_DOWNLOADED, book.getIs_downloaded());
        contentValues.put(COL_FAV, book.getIs_fav());
        contentValues.put(COL_UPDATE_BOOK, book.getIs_update_book());
        contentValues.put(COL_B_ORDER, book.getB_order());
        return contentValues;
    }

    public static void saveBookIfNotExists(Book b_name) {
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" + b_name.getId() + "'";
        //int id = DbAdapter.getIdForBook(query);
        String id = "";
        String update_date = "";
        String book_update_date = "";
        int isDownloaded = 0;
        int isFav = 0;
        Cursor c = DbAdapter.getIdForBookData(query);
        while (c.moveToNext()) {
            id = c.getString(c.getColumnIndex(COL_BOOK_ID));
            update_date = c.getString(c.getColumnIndex(COL_UPDATED_AT));
            book_update_date = c.getString(c.getColumnIndex(COL_BOOK_UPDATED_AT));
            isDownloaded = c.getInt(c.getColumnIndex(COL_DOWNLOADED));
            isFav = c.getInt(c.getColumnIndex(COL_FAV));
        }
        c.close();
        if (id.equals("")) {
            DbAdapter.saveBook(getBookContentValues(b_name));
        } else {
            if (!b_name.getBook_updated_at().equals(book_update_date)) {
                b_name.setIs_update_book(1);
            } else
                b_name.setIs_update_book(0);
            b_name.setIs_downloaded(isDownloaded);
            b_name.setIs_fav(isFav);
            if (!b_name.getUpdated_at().equals(update_date))
                DbAdapter.updateBook(getBookContentValues(b_name), id);
        }
    }

    public static void updatedBook(Book b_name) {
        DbAdapter.updateBook(getBookContentValues(b_name), b_name.getId());
    }

    public static ArrayList<Book> getFilterBook(String email, String adminEmail, ArrayList<String> strings,int val) {
        /*if (strings.isEmpty())
            return new ArrayList<>();*/

        Cursor bookCursor = DbAdapter.getFilterBooks(email, adminEmail, strings,val);
        return getBooks(bookCursor);
        //return new ArrayList<>();
    }

    public static ArrayList<Book> getAllBooks(String email, String adminEmail) {
        Cursor bookCursor = DbAdapter.getAllBooks(email, adminEmail);
        return getBooks(bookCursor);
    }

    public static ArrayList<Book> getMyBooks() {
        Cursor bookCursor = DbAdapter.getMyBooks();
        return getBooks(bookCursor);
    }

    public static ArrayList<Book> getFavBooks() {
        Cursor bookCursor = DbAdapter.getFavBooks();
        return getBooks(bookCursor);
    }

    private static ArrayList<Book> getBooks(Cursor bookCursor) {
        ArrayList<Book> books = new ArrayList<>();
        while (bookCursor.moveToNext()) {

            books.add(new Book(bookCursor.getString(bookCursor.getColumnIndex(COL_BOOK_ID)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_ADDED_BY)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_TITLE)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_AUTHOR)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_FRONT_IMG)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_BOOK_FILE)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_DESCRIPTION)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_IS_BOOK_AUDIO)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_IS_BOOK_KAVYA)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_EXTRA_FIELDS)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_STATUS)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_BOOK_UPDATED_AT)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_CREATED_AT)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_UPDATED_AT)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_TAGS)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_DOWNLOADED)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_FAV)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_UPDATE_BOOK)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_B_ORDER))
            ));
        }

        return books;

    }

    public static Book getBook(int id){
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_BOOK_ID + " = '" +id+ "'";
        Cursor bookCursor = DbAdapter.getIdForBookData(query);
        while (bookCursor.moveToNext()) {
            return new Book(bookCursor.getString(bookCursor.getColumnIndex(COL_BOOK_ID)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_ADDED_BY)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_TITLE)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_AUTHOR)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_FRONT_IMG)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_BOOK_FILE)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_DESCRIPTION)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_IS_BOOK_AUDIO)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_IS_BOOK_KAVYA)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_EXTRA_FIELDS)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_STATUS)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_BOOK_UPDATED_AT)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_CREATED_AT)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_UPDATED_AT)),
                    bookCursor.getString(bookCursor.getColumnIndex(COL_TAGS)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_DOWNLOADED)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_FAV)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_UPDATE_BOOK)),
                    bookCursor.getInt(bookCursor.getColumnIndex(COL_B_ORDER))
            );
        }
        return new Book();

    }

}
