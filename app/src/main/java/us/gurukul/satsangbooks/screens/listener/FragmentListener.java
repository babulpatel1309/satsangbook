package us.gurukul.satsangbooks.screens.listener;

import com.folioreader.model.Book;

public interface FragmentListener {
    void setOnClickItem(Book book);

    void setExtraField(String url);
}
