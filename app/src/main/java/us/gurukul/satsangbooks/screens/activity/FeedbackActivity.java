package us.gurukul.satsangbooks.screens.activity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import us.gurukul.satsangbooks.screens.NetworkHelper;
import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.listener.RetrofitInterface;

import static us.gurukul.satsangbooks.screens.Common.BASE_URL;

public class FeedbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_feedback);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle("Your Feedback");
        }

        final ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.feedback_dropdown_arrays,
                android.R.layout.simple_spinner_dropdown_item);

        final MaterialBetterSpinner materialDesignSpinner = findViewById(R.id.android_material_design_spinner);
        materialDesignSpinner.setAdapter(arrayAdapter);
        materialDesignSpinner.setSelection(0);

        final Button send = findViewById(R.id.sendFeedback);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String type = materialDesignSpinner.getText().toString();
                String comment = ((EditText) findViewById(R.id.comment)).getText().toString();
                String mobile = ((EditText) findViewById(R.id.mobile)).getText().toString();
                if (type.equals("Choose Type")) {
                    Log.e("Selected Item", "null");
                    Toast.makeText(FeedbackActivity.this, "Please, Select feedback type", Toast.LENGTH_SHORT).show();
                } else if (mobile.equals("")) {
                    Toast.makeText(FeedbackActivity.this, "Please, Enter mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobile.length() < 10) {
                    Toast.makeText(FeedbackActivity.this, "Please, Enter correct mobile number ", Toast.LENGTH_SHORT).show();
                } else if (comment.equals("")) {
                    Toast.makeText(FeedbackActivity.this, "Please, Enter comment", Toast.LENGTH_SHORT).show();
                } else {
                    if (NetworkHelper.isOnline(FeedbackActivity.this)) {
                        Log.e("Selected Type", "str-> " + type);
                        Log.e("Comment", "comment-> " + comment);
                        String reqString = Build.MANUFACTURER
                                + " " + Build.MODEL + " " + Build.VERSION.RELEASE
                                + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();

                        String email = getIntent().getStringExtra("email");

                        //findViewById(R.id.loading).setVisibility(View.VISIBLE);
                        send.setText("Sending...");
                        send.setEnabled(false);
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                .build();
                        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
                        Call<ResponseBody> request = retrofitInterface.feedbackData(email, type, mobile, comment, reqString);
                        request.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.body() != null)
                                    Log.e("onResponse", "feedbackData-> " + response.body().toString());
                                //findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                                send.setText(" Send ");
                                send.setEnabled(true);
                                ((EditText) findViewById(R.id.comment)).setText("");
                                ((EditText) findViewById(R.id.mobile)).setText("");
                                Toast.makeText(FeedbackActivity.this, "Your feedback sent.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                //findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                                Log.e("onFailure", "feedbackData-> " + t.getMessage());
                                send.setText(" Send ");
                                send.setEnabled(true);
                                Toast.makeText(FeedbackActivity.this, "feedback failed, Try again.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(FeedbackActivity.this, "No Internet Connection.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
