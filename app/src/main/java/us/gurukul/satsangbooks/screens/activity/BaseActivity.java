package us.gurukul.satsangbooks.screens.activity;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import us.gurukul.satsangbooks.screens.listener.RetrofitInterface;

import static us.gurukul.satsangbooks.screens.Common.BASE_URL;

public abstract class BaseActivity extends AppCompatActivity {

    RetrofitInterface callWS(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        return retrofit.create(RetrofitInterface.class);
    }



}
