package com.folioreader.ui.view

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.SeekBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.folioreader.Color
import com.folioreader.Config
import com.folioreader.R
import com.folioreader.model.event.ReloadDataEvent
import com.folioreader.ui.activity.FolioActivity
import com.folioreader.ui.activity.FolioActivityCallback
import com.folioreader.ui.adapter.ColorAdapter
import com.folioreader.ui.fragment.MediaControllerFragment
import com.folioreader.util.AppUtil
import com.folioreader.util.UiUtil
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.view_config.*
import org.greenrobot.eventbus.EventBus
import java.util.*

class ConfigBottomSheetDialogFragment : BottomSheetDialogFragment() {

    companion object {
        const val FADE_DAY_NIGHT_MODE = 500
        @JvmField
        val LOG_TAG: String = ConfigBottomSheetDialogFragment::class.java.simpleName
    }

    private lateinit var config: Config
    private var isNightMode = false
    private lateinit var activityCallback: FolioActivityCallback
    private var adapterColors: ColorAdapter? = null
    private var loadingView: LoadingView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_config, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (activity is FolioActivity)
            activityCallback = activity as FolioActivity

        view.viewTreeObserver.addOnGlobalLayoutListener {
            val dialog = dialog as BottomSheetDialog
            val bottomSheet =
                    dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
            val behavior = BottomSheetBehavior.from(bottomSheet!!)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
            behavior.peekHeight = 0
        }

        config = AppUtil.getSavedConfig(activity)!!
        initViews()
    }

    override fun onDestroy() {
        super.onDestroy()
        view?.viewTreeObserver?.addOnGlobalLayoutListener(null)
    }

    public fun setLoading(loadingView: LoadingView?) {
        this.loadingView = loadingView
    }

    @SuppressLint("SetTextI18n")
    private fun initViews() {
        inflateView()
        configFonts()
        view_config_font_size_seek_bar.progress = config.fontSize * 10
        configSeekBar()

        isNightMode = config.isNightMode
        if (isNightMode) {
            container.setBackgroundColor(ContextCompat.getColor(context!!, R.color.night))
            fontSizeLabel.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
            fontColorLabel.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
            scrollTypeLabel.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
            rv_colors.visibility = View.GONE
            isSetNightMode.visibility = View.VISIBLE
        } else {
            container.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
        }

        if (isNightMode) {
            view_config_ib_day_mode.isSelected = false
            view_config_ib_night_mode.isSelected = true
            UiUtil.setColorIntToDrawable(config.themeColor, view_config_ib_night_mode.drawable)
            UiUtil.setColorResToDrawable(R.color.app_gray, view_config_ib_day_mode.drawable)
            brightness_label.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
        } else {
            view_config_ib_day_mode.isSelected = true
            view_config_ib_night_mode.isSelected = false
            UiUtil.setColorIntToDrawable(config.themeColor, view_config_ib_day_mode!!.drawable)
            UiUtil.setColorResToDrawable(R.color.app_gray, view_config_ib_night_mode.drawable)
            brightness_label.setTextColor(ContextCompat.getColor(context!!, R.color.black))
        }

        //Set font color
        val colors = ArrayList<Color>()
        colors.add(Color("Black", R.drawable.black_circle_shape))
        colors.add(Color("Green", R.drawable.green_circle_shape))
        colors.add(Color("Pink", R.drawable.pink_circle_shape))
        colors.add(Color("Blue", R.drawable.blue_color_circle))
        colors.add(Color("Brown", R.drawable.brown_circle_shape))
        colors.add(Color("Grey", R.drawable.grey_circle_shape))
        colors.add(Color("Red", R.drawable.red_circle_shape))
        colors.add(Color("Burgandy", R.drawable.burgandy_circle_shape))
        colors.add(Color("Paledark", R.drawable.paledark_circle_shape))

        adapterColors = ColorAdapter(config, colors, ColorAdapter.ColorAdapterCallback { pos ->
            if (loadingView != null && loadingView!!.isShown) {
                Toast.makeText(context!!, "Please wait...", Toast.LENGTH_SHORT).show()
            } else {
                config.fontColor = pos
                AppUtil.saveConfig(context, config)
                EventBus.getDefault().post(ReloadDataEvent())
                dismiss()
            }
        })

        rv_colors.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv_colors.adapter = adapterColors

        //Set Brightness
        brightnessSeekbar.progress = config.brightness//default
        val perc = config.brightness * 100 / 255
        brightness_label.text = "$perc%"
        brightnessSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                val per = progress * 100 / 255
                Log.e("Brightness", "" + per)
                brightness_label.text = "$per%"
                config.brightness = progress
                activityCallback.setScreenBrightness(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                AppUtil.saveConfig(context, config)
            }
        })
    }

    private fun inflateView() {
        if (config.allowedDirection != Config.AllowedDirection.VERTICAL_AND_HORIZONTAL) {
            view5.visibility = View.GONE
            buttonVertical.visibility = View.GONE
            buttonHorizontal.visibility = View.GONE
        }

        view_config_ib_day_mode.setOnClickListener {
            if (loadingView != null && loadingView!!.isShown) {
                Toast.makeText(context!!, "Please wait...", Toast.LENGTH_SHORT).show()
            } else if (!view_config_ib_day_mode.isSelected) {
                isNightMode = true
                toggleBlackTheme()
                view_config_ib_day_mode.isSelected = true
                view_config_ib_night_mode.isSelected = false
                setToolBarColor()
                UiUtil.setColorResToDrawable(R.color.app_gray, view_config_ib_night_mode.drawable)
                UiUtil.setColorIntToDrawable(config.themeColor, view_config_ib_day_mode.drawable)

                fontSizeLabel.setTextColor(ContextCompat.getColor(context!!, R.color.black))
                fontColorLabel.setTextColor(ContextCompat.getColor(context!!, R.color.black))
                brightness_label.setTextColor(ContextCompat.getColor(context!!, R.color.black))
                scrollTypeLabel.setTextColor(ContextCompat.getColor(context!!, R.color.black))
            }
        }

        view_config_ib_night_mode.setOnClickListener {
            if (loadingView != null && loadingView!!.isShown) {
                Toast.makeText(context!!, "Please wait...", Toast.LENGTH_SHORT).show()
            } else if (!view_config_ib_night_mode.isSelected) {
                isNightMode = false
                toggleBlackTheme()
                view_config_ib_day_mode.isSelected = false
                view_config_ib_night_mode.isSelected = true
                setToolBarColor()
                //setAudioPlayerBackground()
                UiUtil.setColorIntToDrawable(config.themeColor, view_config_ib_night_mode.drawable)
                UiUtil.setColorResToDrawable(R.color.app_gray, view_config_ib_day_mode.drawable)

                fontSizeLabel.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
                fontColorLabel.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
                brightness_label.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
                scrollTypeLabel.setTextColor(ContextCompat.getColor(context!!, R.color.app_gray))
            }
        }

        if (activityCallback.direction == Config.Direction.HORIZONTAL) {
            buttonHorizontal.isSelected = true
        } else if (activityCallback.direction == Config.Direction.VERTICAL) {
            buttonVertical.isSelected = true
        }

        buttonVertical.setOnClickListener {
            if (!buttonVertical.isSelected) {
                if (loadingView != null && loadingView!!.isShown) {
                    Toast.makeText(context!!, "Please wait...", Toast.LENGTH_SHORT).show()
                } else {
                    Log.e(FolioActivity.LOG_TAG, "onDirectionChange-> buttonVertical")
                    config.direction = Config.Direction.VERTICAL
                    AppUtil.saveConfig(context, config)
                    activityCallback.onDirectionChange(Config.Direction.VERTICAL)
                    buttonHorizontal.isSelected = false
                    buttonVertical.isSelected = true
                    dismiss()
                }
            }
        }

        buttonHorizontal.setOnClickListener {
            if (!buttonHorizontal.isSelected) {
                if (loadingView != null && loadingView!!.isShown) {
                    Toast.makeText(context!!, "Please wait...", Toast.LENGTH_SHORT).show()
                } else {
                    Log.e(FolioActivity.LOG_TAG, "onDirectionChange-> buttonHorizontal")
                    config.direction = Config.Direction.HORIZONTAL
                    AppUtil.saveConfig(context, config)
                    activityCallback.onDirectionChange(Config.Direction.HORIZONTAL)
                    buttonHorizontal.isSelected = true
                    buttonVertical.isSelected = false
                    dismiss()
                }
            }
        }

    }

    private fun configFonts() {
        val colorStateList = UiUtil.getColorList(config.themeColor, ContextCompat.getColor(context!!, R.color.app_gray))
        buttonVertical.setTextColor(colorStateList)
        buttonHorizontal.setTextColor(colorStateList)
    }

    private fun toggleBlackTheme() {
        val day = ContextCompat.getColor(context!!, R.color.white)
        val night = ContextCompat.getColor(context!!, R.color.night)

        val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), if (isNightMode) night else day, if (isNightMode) day else night)

        colorAnimation.duration = FADE_DAY_NIGHT_MODE.toLong()

        colorAnimation.addUpdateListener { animator ->
            val value = animator.animatedValue as Int
            container.setBackgroundColor(value)
        }

        colorAnimation.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {}

            override fun onAnimationEnd(animator: Animator) {
                isNightMode = !isNightMode
                config.isNightMode = isNightMode
                AppUtil.saveConfig(activity, config)
                EventBus.getDefault().post(ReloadDataEvent())
                dismiss()
            }

            override fun onAnimationCancel(animator: Animator) {}

            override fun onAnimationRepeat(animator: Animator) {}
        })

        colorAnimation.duration = FADE_DAY_NIGHT_MODE.toLong()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val attrs = intArrayOf(android.R.attr.navigationBarColor)
            val typedArray = activity?.theme?.obtainStyledAttributes(attrs)
            val defaultNavigationBarColor = typedArray?.getColor(0,
                    ContextCompat.getColor(context!!, R.color.white)
            )
            val black = ContextCompat.getColor(context!!, R.color.black)

            val navigationColorAnim = ValueAnimator.ofObject(
                    ArgbEvaluator(),
                    if (isNightMode) black else defaultNavigationBarColor,
                    if (isNightMode) defaultNavigationBarColor else black
            )

            navigationColorAnim.addUpdateListener { valueAnimator ->
                val value = valueAnimator.animatedValue as Int
                activity?.window?.navigationBarColor = value
            }

            navigationColorAnim.duration = FADE_DAY_NIGHT_MODE.toLong()
            navigationColorAnim.start()
        }

        colorAnimation.start()
    }

    private fun configSeekBar() {
        /*val thumbDrawable = ContextCompat.getDrawable(activity!!, R.drawable.seekbar_thumb)
        UiUtil.setColorIntToDrawable(config.themeColor, thumbDrawable)
        UiUtil.setColorResToDrawable(R.color.grey_color, view_config_font_size_seek_bar.progressDrawable)
        view_config_font_size_seek_bar.thumb = thumbDrawable*/

        view_config_font_size_seek_bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                if (loadingView != null && loadingView!!.isShown) {
                    Toast.makeText(context!!, "Please wait...", Toast.LENGTH_SHORT).show()
                    return
                }
                config.fontSize = seekBar.progress / 10
                AppUtil.saveConfig(activity, config)
                EventBus.getDefault().post(ReloadDataEvent())
                dismiss()
            }
        })
    }

    private fun setToolBarColor() {
        if (isNightMode) {
            activityCallback.setDayMode()
            rv_colors.visibility = View.VISIBLE
            isSetNightMode.visibility = View.GONE
        } else {
            activityCallback.setNightMode()
            rv_colors.visibility = View.GONE
            isSetNightMode.visibility = View.VISIBLE
        }
    }

    private fun setAudioPlayerBackground() {

        var mediaControllerFragment: Fragment? = fragmentManager?.findFragmentByTag(MediaControllerFragment.LOG_TAG)
                ?: return
        mediaControllerFragment = mediaControllerFragment as MediaControllerFragment
        if (isNightMode) {
            mediaControllerFragment.setDayMode()
        } else {
            mediaControllerFragment.setNightMode()
        }
    }
}
