package com.folioreader.ui.base;

import com.folioreader.model.BookmarkImpl;

public interface BookmarkListener {

    void onItemClick(BookmarkImpl bookmark);

    void onItemDelete(int pos);

}