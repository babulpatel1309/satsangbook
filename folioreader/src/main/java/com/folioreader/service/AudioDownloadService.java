package com.folioreader.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.folioreader.R;
import com.folioreader.model.Audio;

import java.io.File;

import static com.folioreader.Constants.fileAudioDir;

public class AudioDownloadService extends Service {
    private static final int NOTIFY_ID = 100;
    public static final String MSG = "message";
    public static final String FILE = "file";
    public static final String RESULT = "result";
    public static final String FAILED = "failed";
    public static boolean isDownload = false;
    private String LOG_TAG = "";
    public static Audio audio;
    private NotificationManager mNotificationManager;
    private static final String CHANNEL_ID = "us.gurukul.satsangbooks.screens.AUDIO_CHANNEL_ID";

    @Override
    public void onCreate() {
        super.onCreate();
        LOG_TAG = this.getClass().getSimpleName();
        Log.d(LOG_TAG, "In onCreate");
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "In onStartCommand");

        if (intent != null) {
            audio = intent.getParcelableExtra("audioItem");
            final String urlPath = audio.getUrl();
            Log.e("urlPath", urlPath);
            final File file = new File(urlPath);
            final String fileName = file.getName();
            isDownload = true;
            Toast.makeText(this, "Start downloading...", Toast.LENGTH_SHORT).show();
            PRDownloader.download(urlPath, fileAudioDir.getPath(), fileName)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {
                            Log.d("Download Service", "onStartOrResume");
                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {
                            Log.d("Download Service", "onPause");
                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {
                            Log.d("Download Service", "onCancel");

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {
                            //long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                            //publishResults((int) progressPercent, fileName);
                        }
                    })
                    .start(new OnDownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            Log.d("Download Service", "Completed");
                            downloadCompleted(" Download Completed", fileName);
                            stopSelf();
                        }

                        @Override
                        public void onError(Error error) {
                            Log.d("Download Service", "Failed");
                            downloadFailed(" Download Failed", fileName);
                            stopSelf();
                        }
                    });

            showNotification("Downloading...");
        }

        return START_NOT_STICKY;
    }

    public void showNotification(String status) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (mNotificationManager.getNotificationChannel(CHANNEL_ID) == null) {
                NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_LOW);
                notificationChannel.setDescription(getString(R.string.app_name));
                notificationChannel.setLightColor(Color.TRANSPARENT);
                notificationChannel.enableVibration(false);
                notificationChannel.setShowBadge(false);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                //.setContentIntent(pendingIntent)
                //.setColor(0)
                .setContentTitle(audio.getTittle())
                .setContentText(status)
                .setSmallIcon(R.mipmap.ic_launcher)  // the status icon
                //.setCustomContentView(getSmallContentView())
                //.setCustomBigContentView(getBigContentView())
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_LOW);

        startForeground(NOTIFY_ID, notification.build());
    }

    private void downloadCompleted(String msg, String file) {
        isDownload = false;
        if (audio != null) {
            Intent intent = new Intent();
            intent.setAction(RESULT);
            intent.putExtra(MSG, audio.getTittle() + msg);
            intent.putExtra(FILE, file);
            sendBroadcast(intent);
        }
    }

    private void downloadFailed(String msg, String file) {
        isDownload = false;
        if (audio != null) {
            Intent intent = new Intent();
            intent.setAction(FAILED);
            intent.putExtra(MSG, audio.getTittle() + msg);
            sendBroadcast(intent);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "In onBind");
        return null;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d(LOG_TAG, "In onTaskRemoved");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "In onDestroy");
        audio = null;
    }
}
