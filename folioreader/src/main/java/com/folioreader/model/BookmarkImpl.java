package com.folioreader.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.folioreader.util.ObjectMapperSingleton;

import java.io.IOException;

/**
 * Created by Hrishikesh Kadam on 20/04/2018.
 */
@JsonPropertyOrder({"bookId", "chapterHref"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookmarkImpl implements Bookmark, Parcelable {

    public static final Creator<BookmarkImpl> CREATOR = new Creator<BookmarkImpl>() {
        @Override
        public BookmarkImpl createFromParcel(Parcel in) {
            return new BookmarkImpl(in);
        }

        @Override
        public BookmarkImpl[] newArray(int size) {
            return new BookmarkImpl[size];
        }
    };

    private static final String LOG_TAG = BookmarkImpl.class.getSimpleName();
    private String bookId;
    private String bookFile;
    private String chapterHref;
    private String value;
    private int isKavya = 0;

    public BookmarkImpl() {
    }

    public BookmarkImpl(String bookId,String file, String chapterHref, String value,int isKavya) {
        this.bookId = bookId;
        this.bookFile = file;
        this.chapterHref = chapterHref;
        this.value = value;
        this.isKavya = isKavya;
    }

    public static Bookmark createInstance(String jsonString) {

        BookmarkImpl readPosition = null;
        try {
            readPosition = ObjectMapperSingleton.getObjectMapper()
                    .reader()
                    .forType(BookmarkImpl.class)
                    .readValue(jsonString);
        } catch (IOException e) {
            Log.e(LOG_TAG, "-> ", e);
        }
        return readPosition;
    }

    protected BookmarkImpl(Parcel in) {
        bookId = in.readString();
        bookFile = in.readString();
        chapterHref = in.readString();
        value = in.readString();
        isKavya = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bookId);
        dest.writeString(bookFile);
        dest.writeString(chapterHref);
        dest.writeString(value);
        dest.writeInt(isKavya);
    }

    @Override
    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    @Override
    public String getBookFile() {
        return bookFile;
    }

    public void setBookFile(String bookId) {
        this.bookFile = bookId;
    }

    @Override
    public String getChapterHref() {
        return chapterHref;
    }

    public void setChapterHref(String chapterHref) {
        this.chapterHref = chapterHref;
    }

    @Override
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int getIsKavya() {
        return isKavya;
    }

    public void setIsKavya(int value) {
        this.isKavya = value;
    }

    @Override
    public String toJson() {
        try {
            ObjectWriter objectWriter = ObjectMapperSingleton.getObjectMapper().writer();
            return objectWriter.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            Log.e(LOG_TAG, "-> ", e);
            return null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
