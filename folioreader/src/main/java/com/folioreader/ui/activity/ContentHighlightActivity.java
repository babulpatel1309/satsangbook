package com.folioreader.ui.activity;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.folioreader.Config;
import com.folioreader.Constants;
import com.folioreader.FolioReader;
import com.folioreader.R;
import com.folioreader.ui.adapter.TabAdapter;
import com.folioreader.ui.fragment.BookmarkFragment;
import com.folioreader.ui.fragment.HighlightFragment;
import com.folioreader.ui.fragment.TableOfContentFragment;
import com.folioreader.util.AppUtil;

import org.readium.r2.shared.Publication;

public class ContentHighlightActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean mIsNightMode;
    private Publication publication;
    private Button btn_all_book;
    private Button btn_my_book;
    private Button btn_bookmark;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_content_highlight);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);

        toolbar.setNavigationIcon(drawable);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));

        publication = (Publication) getIntent().getSerializableExtra(Constants.PUBLICATION);
        if (actionBar != null) {
            actionBar.setTitle(publication.getMetadata().getTitle());
        }

        Config mConfig = AppUtil.getSavedConfig(this);
        mIsNightMode = mConfig != null && mConfig.isNightMode();
        if (mConfig != null) {
            int brightnessValue = mConfig.getBrightness();
            if (brightnessValue >= 0 && brightnessValue <= 255) {
                WindowManager.LayoutParams layoutpars = getWindow().getAttributes();
                layoutpars.screenBrightness = (float) brightnessValue / 255;
                getWindow().setAttributes(layoutpars);
            }
        }

        initViews();

        viewPager = findViewById(R.id.viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        btn_all_book.setTypeface(null, Typeface.BOLD);
                        btn_my_book.setTypeface(null, Typeface.NORMAL);
                        btn_bookmark.setTypeface(null, Typeface.NORMAL);
                        break;
                    case 1:
                        btn_my_book.setTypeface(null, Typeface.BOLD);
                        btn_all_book.setTypeface(null, Typeface.NORMAL);
                        btn_bookmark.setTypeface(null, Typeface.NORMAL);
                        break;
                    case 2:
                        btn_bookmark.setTypeface(null, Typeface.BOLD);
                        btn_all_book.setTypeface(null, Typeface.NORMAL);
                        btn_my_book.setTypeface(null, Typeface.NORMAL);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private void initViews() {
        btn_all_book = findViewById(R.id.btn_all_books);
        btn_my_book = findViewById(R.id.btn_my_books);
        btn_bookmark = findViewById(R.id.btn_bookmark);
        btn_all_book.setOnClickListener(this);
        btn_my_book.setOnClickListener(this);
        btn_bookmark.setOnClickListener(this);

        if (mIsNightMode) {
            findViewById(R.id.toolbar).setBackgroundColor(Color.BLACK);

            findViewById(R.id.contentMain).setBackgroundColor(ContextCompat.getColor(this, R.color.night));
            findViewById(R.id.roundedTabLayout).setBackgroundResource(R.drawable.rounded_shape_night);
            btn_all_book.setTextColor(ContextCompat.getColor(this, R.color.app_gray));
            btn_my_book.setTextColor(ContextCompat.getColor(this, R.color.app_gray));
            findViewById(R.id.pipeLine).setBackgroundColor(ContextCompat.getColor(this, R.color.app_gray));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int color;
            if (mIsNightMode) {
                color = ContextCompat.getColor(this, R.color.black);
            } else {
                int[] attrs = {android.R.attr.navigationBarColor};
                TypedArray typedArray = getTheme().obtainStyledAttributes(attrs);
                color = typedArray.getColor(0, ContextCompat.getColor(this, R.color.black));
            }
            getWindow().setNavigationBarColor(color);
        }

        //Fragment with tab
        TableOfContentFragment contentFrameLayout = TableOfContentFragment.newInstance(publication,
                getIntent().getStringExtra(Constants.CHAPTER_SELECTED));

        HighlightFragment highlightFragment = HighlightFragment.newInstance(
                getIntent().getStringExtra(FolioReader.EXTRA_BOOK_ID));

        BookmarkFragment bookmarkFragment = BookmarkFragment.newInstance(
                getIntent().getStringExtra(FolioReader.EXTRA_BOOK_ID));

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(contentFrameLayout, "Contents");
        adapter.addFragment(highlightFragment, "Highlights");
        adapter.addFragment(bookmarkFragment, "Bookmark");

        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_all_books) {
            viewPager.setCurrentItem(0);
            btn_all_book.setTypeface(null, Typeface.BOLD);
            btn_my_book.setTypeface(null, Typeface.NORMAL);
            btn_bookmark.setTypeface(null, Typeface.NORMAL);
        } else if (v.getId() == R.id.btn_my_books) {
            viewPager.setCurrentItem(1);
            btn_my_book.setTypeface(null, Typeface.BOLD);
            btn_all_book.setTypeface(null, Typeface.NORMAL);
            btn_bookmark.setTypeface(null, Typeface.NORMAL);
        } else if (v.getId() == R.id.btn_bookmark) {
            viewPager.setCurrentItem(2);
            btn_bookmark.setTypeface(null, Typeface.BOLD);
            btn_all_book.setTypeface(null, Typeface.NORMAL);
            btn_my_book.setTypeface(null, Typeface.NORMAL);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
