package us.gurukul.satsangbooks.screens;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

public class FrameImageView extends AppCompatImageView {
    public FrameImageView(Context context) {
        super(context);
    }

    public FrameImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FrameImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth() + (getMeasuredWidth() / 4)); //portriat
        //setMeasuredDimension(getMeasuredWidth() , getMeasuredWidth() / 2); //landscap
    }
}