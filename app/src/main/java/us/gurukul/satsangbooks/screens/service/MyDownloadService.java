package us.gurukul.satsangbooks.screens.service;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.folioreader.model.Book;
import com.folioreader.model.sqlite.BookTable;

import androidx.annotation.Nullable;
import us.gurukul.satsangbooks.screens.Common;

import static us.gurukul.satsangbooks.screens.Common.fileDir;

public class MyDownloadService extends Service {
    public static final String ID = "id";
    public static final String FILENAME = "filename";
    public static final String PROGRESS = "progress";
    public static final String RESULT = "result";
    public static final String FAILED = "failed";
    private int downloadIdOne = 0;
    private String LOG_TAG = null;
    private Book book;

    @Override
    public void onCreate() {
        super.onCreate();
        LOG_TAG = this.getClass().getSimpleName();
        Log.d(LOG_TAG, "In onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "In onStartCommand");

        if (intent != null) {
            book = intent.getParcelableExtra("bookItem");
            //String urlPath = intent.getStringExtra(URL);
            final String fileName = book.getBooks_file();

            downloadIdOne = PRDownloader.download(Common.filePath + fileName, fileDir.getPath(), fileName)
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {
                            Log.d("Download Service", "onStartOrResume");
                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {
                            Log.d("Download Service", "onPause");
                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {
                            Log.d("Download Service", "onCancel");
                            downloadIdOne = 0;
                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {
                            long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                            publishResults((int) progressPercent, fileName);
                        }
                    })
                    .start(new OnDownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            Log.d("Download Service", "Completed");
                            downloadCompleted(fileName);

                            //Add Book in db
                            book.setIs_downloaded(1);
                            book.setIs_update_book(0);
                            BookTable.updatedBook(book); // update audio
                            stopSelf();
                        }

                        @Override
                        public void onError(Error error) {
                            Log.d("Download Service", "Failed");
                            downloadFailed(fileName);
                            //downloadIdOne = 0;
                            stopSelf();
                        }
                    });
        }

        return START_REDELIVER_INTENT;
    }

    private void publishResults(int progress, String fileName) {
        Intent intent = new Intent();
        intent.setAction(PROGRESS);
        intent.putExtra(ID, downloadIdOne);
        intent.putExtra(FILENAME, fileName);
        intent.putExtra(PROGRESS, progress);
        sendBroadcast(intent);
    }

    private void downloadCompleted(String fileName) {
        Intent intent = new Intent();
        intent.setAction(RESULT);
        intent.putExtra(ID, downloadIdOne);
        intent.putExtra(FILENAME, fileName);
        sendBroadcast(intent);
    }

    private void downloadFailed(String fileName) {
        Intent intent = new Intent();
        intent.setAction(FAILED);
        intent.putExtra(ID, downloadIdOne);
        intent.putExtra(FILENAME, fileName);
        sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "In onBind");
        return null;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d(LOG_TAG, "In onTaskRemoved");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "In onDestroy");
    }
}
