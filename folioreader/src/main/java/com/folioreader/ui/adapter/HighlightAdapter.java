package com.folioreader.ui.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.folioreader.Config;
import com.folioreader.R;
import com.folioreader.model.HighlightImpl;
import com.folioreader.ui.view.UnderlinedTextView;
import com.folioreader.util.AppUtil;
import com.folioreader.util.UiUtil;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HighlightAdapter extends RecyclerView.Adapter<HighlightAdapter.HighlightHolder> {
    private List<HighlightImpl> highlights;
    private HighLightAdapterCallback callback;
    private Context context;
    private Config config;

    public HighlightAdapter(Context context, List<HighlightImpl> highlights, HighLightAdapterCallback callback, Config config) {
        this.context = context;
        this.highlights = highlights;
        this.callback = callback;
        this.config = config;
    }

    @NotNull
    @Override
    public HighlightHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new HighlightHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_highlight, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull final HighlightHolder holder, final int position) {
        final HighlightImpl highlight = getItem(position);
        holder.content.setText(Html.fromHtml(highlight.getContent()));
        UiUtil.setBackColorToTextView(holder.content,
                highlight.getType());
        holder.date.setText(AppUtil.formatDate(highlight.getDate()));
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemClick(highlight);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                highlights.remove(position);
                notifyDataSetChanged();
                callback.deleteHighlight(highlight.getId());

            }
        });
        holder.editNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.editNote(highlight, position);
            }
        });
        if (highlight.getNote() != null) {
            if (highlight.getNote().isEmpty()) {
                holder.note.setVisibility(View.GONE);//INVISIBLE
                holder.editNote.setVisibility(View.GONE);
            } else {
                holder.editNote.setVisibility(View.VISIBLE);
                holder.note.setVisibility(View.VISIBLE);
                holder.note.setText(highlight.getNote());
            }
        } else {
            holder.note.setVisibility(View.GONE);//INVISIBLE
            holder.editNote.setVisibility(View.GONE);
        }

        if (config.isNightMode()) {
            holder.lyt_container.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_black_shape));
            holder.date.setTextColor(ContextCompat.getColor(context, R.color.app_gray));
            holder.note.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.content.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    private HighlightImpl getItem(int position) {
        return highlights.get(position);
    }

    @Override
    public int getItemCount() {
        return highlights.size();
    }

    public void editNote(String note, int position) {
        highlights.get(position).setNote(note);
        notifyDataSetChanged();
    }

    static class HighlightHolder extends RecyclerView.ViewHolder {
        private RelativeLayout lyt_container;
        private RelativeLayout container;
        private UnderlinedTextView content;
        private TextView date;
        private TextView note;
        private LinearLayout delete, editNote;

        HighlightHolder(View itemView) {
            super(itemView);
            lyt_container = itemView.findViewById(R.id.lyt_container);
            container = itemView.findViewById(R.id.container);
            content = itemView.findViewById(R.id.utv_highlight_content);
            date = itemView.findViewById(R.id.tv_highlight_date);
            delete = itemView.findViewById(R.id.iv_delete);
            editNote = itemView.findViewById(R.id.iv_edit_note);
            note = itemView.findViewById(R.id.tv_note);
        }
    }

    public interface HighLightAdapterCallback {
        void onItemClick(HighlightImpl highlightImpl);

        void deleteHighlight(int id);

        void editNote(HighlightImpl highlightImpl, int position);
    }
}
