package com.folioreader.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Book implements Parcelable {
    @SerializedName("id")
    private String id;
    @SerializedName("added_by")
    private String added_by;
    @SerializedName("title")
    private String title;
    @SerializedName("author")
    private String author;
    @SerializedName("front_img")
    private String front_img;
    @SerializedName("books_file")
    private String books_file;
    @SerializedName("description")
    private String description;
    @SerializedName("is_book_audio")
    private int is_book_audio;
    @SerializedName("is_kavya")
    private int is_kavya;
    @SerializedName("extra_fields")
    private String extraArray;
    @SerializedName("status")
    private String status;
    @SerializedName("book_updated_at")
    private String book_updated_at;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;
    @SerializedName("tags")
    private String tagsArray;
    //
    private int is_downloaded;
    private int is_fav;
    private int is_update_book;
    private int b_order;

    public Book() {
    }

    public Book(String id, String added_by, String title, String author, String front_img, String books_file,
                String description, int is_book_audio,int is_kavya, String extraArray, String status, String book_updated_at,
                String created_at, String updated_at, String tagsArray, int is_downloaded, int is_fav, int is_update_book, int b_order) {
        this.id = id;
        this.added_by = added_by;
        this.title = title;
        this.author = author;
        this.front_img = front_img;
        this.books_file = books_file;
        this.description = description;
        this.is_book_audio = is_book_audio;
        this.is_kavya = is_kavya;
        this.extraArray = extraArray;
        this.status = status;
        this.book_updated_at = book_updated_at;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.tagsArray = tagsArray;
        this.is_downloaded = is_downloaded;
        this.is_fav = is_fav;
        this.is_update_book = is_update_book;
        this.b_order = b_order;
    }

    protected Book(Parcel in) {
        id = in.readString();
        added_by = in.readString();
        title = in.readString();
        author = in.readString();
        front_img = in.readString();
        books_file = in.readString();
        description = in.readString();
        is_book_audio = in.readInt();
        is_kavya = in.readInt();
        extraArray = in.readString();
        status = in.readString();
        book_updated_at = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        tagsArray = in.readString();
        is_downloaded = in.readInt();
        is_fav = in.readInt();
        is_update_book = in.readInt();
        b_order = in.readInt();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getFront_img() {
        return front_img;
    }

    public void setFront_img(String front_img) {
        this.front_img = front_img;
    }

    public String getBooks_file() {
        return books_file;
    }

    public void setBooks_file(String books_file) {
        this.books_file = books_file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIs_book_audio() {
        return is_book_audio;
    }

    public void setIs_book_audio(int is_book_audio) {
        this.is_book_audio = is_book_audio;
    }

    public int getIs_book_kavya() {
        return is_kavya;
    }

    public void setIs_book_kavya(int is_kavya) {
        this.is_kavya = is_kavya;
    }

    public String getExtraArray() {
        return extraArray;
    }

    public void setExtraArray(String extraArray) {
        this.extraArray = extraArray;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBook_updated_at() {
        return book_updated_at;
    }

    public void setBook_updated_at(String book_updated_at) {
        this.book_updated_at = book_updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getTagsArray() {
        return tagsArray;
    }

    public void setTagsArray(String tagsArray) {
        this.tagsArray = tagsArray;
    }

    public int getIs_downloaded() {
        return is_downloaded;
    }

    public void setIs_downloaded(int is_downloaded) {
        this.is_downloaded = is_downloaded;
    }

    public int getIs_fav() {
        return is_fav;
    }

    public void setIs_fav(int is_fav) {
        this.is_fav = is_fav;
    }

    public int getIs_update_book() {
        return is_update_book;
    }

    public void setIs_update_book(int is_update_book) {
        this.is_update_book = is_update_book;
    }

    public int getB_order() {
        return b_order;
    }

    public void setB_order(int b_order) {
        this.b_order = b_order;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(added_by);
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(front_img);
        dest.writeString(books_file);
        dest.writeString(description);
        dest.writeInt(is_book_audio);
        dest.writeInt(is_kavya);
        dest.writeString(extraArray);
        dest.writeString(status);
        dest.writeString(book_updated_at);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(tagsArray);
        dest.writeInt(is_downloaded);
        dest.writeInt(is_fav);
        dest.writeInt(is_update_book);
        dest.writeInt(b_order);
    }

}
