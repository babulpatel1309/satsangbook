package us.gurukul.satsangbooks.screens.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.folioreader.FolioReader;
import com.folioreader.model.Book;
import com.folioreader.model.BookmarkImpl;
import com.folioreader.model.Page;
import com.folioreader.model.sqlite.BookTable;
import com.folioreader.util.Prefs;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import us.gurukul.satsangbooks.screens.Common;
import us.gurukul.satsangbooks.screens.R;
import us.gurukul.satsangbooks.screens.SampleApp;
import us.gurukul.satsangbooks.screens.adapter.BookSearchAdapter;
import us.gurukul.satsangbooks.screens.adapter.ViewPagerAdapter;
import us.gurukul.satsangbooks.screens.fragments.AllBooksFragment;
import us.gurukul.satsangbooks.screens.fragments.MyBooksFragment;
import us.gurukul.satsangbooks.screens.listener.FragmentListener;
import us.gurukul.satsangbooks.screens.listener.RetrofitInterface;

import static us.gurukul.satsangbooks.screens.Common.BASE_URL;
import static us.gurukul.satsangbooks.screens.Common.PREF_USER_EMAIL;
import static us.gurukul.satsangbooks.screens.Common.PREF_USER_NAME;
import static us.gurukul.satsangbooks.screens.Common.share_fileDir;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, FragmentListener, EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks, SearchView.OnQueryTextListener {

    private ViewPager viewPager;
    private Button btn_all_book;
    private Button btn_my_book;
    private int REQ_FAV = 123;
    private int REQ_BOOKMARK = 456;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private BookSearchAdapter searchAdapter;
    private ListView listView;
    private GoogleApiClient googleApiClient;
    public static ArrayList<String> listing = new ArrayList<>();
    private Boolean doubleBackToExitPressedOnce = false;
    public static String email;
    public static String adminEmail;
    private Prefs preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        preferences = SampleApp.getInstance().pref;

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        TextView nav_user = hView.findViewById(R.id.userName);
        nav_user.setText(getIntent().getStringExtra("name"));

        listing.clear();
        listing = getIntent().getStringArrayListExtra("bookTags");

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                Log.e("MainActivity", "onPageScrolled->");
            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        btn_all_book.setTypeface(null, Typeface.BOLD);
                        btn_my_book.setTypeface(null, Typeface.NORMAL);
                        break;
                    case 1:
                        btn_my_book.setTypeface(null, Typeface.BOLD);
                        btn_all_book.setTypeface(null, Typeface.NORMAL);
                        break;
                }
                Log.e("MainActivity", "onPageSelected->");
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                Log.e("MainActivity", "onPageScrollStateChanged->");
            }
        });

        btn_all_book = findViewById(R.id.btn_all_books);
        btn_my_book = findViewById(R.id.btn_my_books);
        btn_all_book.setOnClickListener(this);
        btn_my_book.setOnClickListener(this);

        isPermissionGranted();
        //search hatu

        displayWelcomeMessage(this, getIntent().getStringExtra("app_version"));

        //login Data
        if (!SampleApp.token.equals("")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();
            RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
            Call<ResponseBody> request = retrofitInterface.loginData(getIntent().getStringExtra("name"), getIntent().getStringExtra("email"), SampleApp.token);
            request.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.body() != null)
                        Log.e("onResponse", response.body().toString());
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }

        navigationView.inflateMenu(R.menu.activity_main_drawer_1);
        Menu menu = navigationView.getMenu();
        ArrayList<Page> pages = getIntent().getParcelableArrayListExtra("pages");
        for (int i = 0; i < pages.size(); i++) {
            Page page = pages.get(i);
            menu.add(100, i, 0, page.getTitle()).setIcon(R.drawable.ic_pets_black_24dp);
        }
        navigationView.inflateMenu(R.menu.activity_main_drawer_2);

    }

    private void displayWelcomeMessage(Context context, String currentVersion) {
        String appVersion = getAppVersion(context);
        if (!TextUtils.equals(currentVersion, appVersion)) {
            String updateUrl = Common.APP_STORE_URL + getPackageName();
            setUpdateNeeded(updateUrl);
        }
    }

    private String getAppVersion(Context context) {
        String result = "";
        try {
            result = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Version Error ", e.getMessage());
        }
        return result;
    }

    private void setUpdateNeeded(final String updateUrl) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.StyledDialog);
        builder.setTitle("New version available");
        builder.setMessage("Please, update app to new version to continue reposting.");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setExtraField(updateUrl);
            }
        });
        builder.setNegativeButton("No, Thanks", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog sortDialog = builder.create();
        sortDialog.show();

    }

    private void handelListItemClick(Book book) {
        // close search view if its visible
        if (searchView.isShown()) {
            searchMenuItem.collapseActionView();
            searchView.setQuery("", false);
        }

        setOnClickItem(book);
    }

    /**
     * Signs the user out and resets the sign-in button to visible.
     */
    private void signOut() {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            Log.e("MainActivity", "Google API client not initialized or not connected");
            return;
        }
        googleApiClient.connect();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // ...
                        Toast.makeText(getApplicationContext(), "Logged Out", Toast.LENGTH_SHORT).show();
                        preferences.setStringDetail(PREF_USER_NAME,"");
                        preferences.setStringDetail(PREF_USER_EMAIL,"");

                        finish();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    }
                });
        //Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(this);
    }

    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        googleApiClient.connect();
        super.onStart();
    }

    private void isPermissionGranted() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            Log.e("Permission", "All granted");
        } else {
            // Do not have permissions, request them now
            Log.e("Permission", "Don't All granted");
            EasyPermissions.requestPermissions(this, getString(R.string.app_name) + " " + getString(R.string.rationale_storage),
                    111, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.e("Permission", "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Log.e("Permission", "onPermissionsDenied:" + requestCode + ":" + perms.size());
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            Log.e("Permission", "onPermissionsDenied: " + perms.get(0));
            new AppSettingsDialog.Builder(this)
                    .setPositiveButton("Ok")
                    .setRationale(getString(R.string.app_name) + " " + getString(R.string.rationale_storage)).build().show();
        }
    }

    @Override
    public void onRationaleAccepted(int requestCode) {
        Log.e("Permission", "onRationaleAccepted:" + requestCode);
    }

    @Override
    public void onRationaleDenied(int requestCode) {
        Log.e("Permission", "onRationaleDenied:" + requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            String yes = getString(R.string.yes);
            String no = getString(R.string.no);

            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(this,
                    getString(R.string.returned_from_app_settings_to_activity, hasStoragePermission() ? yes : no),
                    Toast.LENGTH_SHORT).show();
        } else if (requestCode == REQ_FAV && resultCode == RESULT_OK) {
            Book item = data.getParcelableExtra("bookItem");
            setOnClickItem(item);
        } else if (requestCode == REQ_BOOKMARK && resultCode == RESULT_OK) {
            BookmarkImpl readPosition = data.getParcelableExtra("bookmark");
            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
            intent.putExtra("isBookmark", true);
            intent.putExtra("isNotify", false);
            intent.putExtra("book_file", readPosition.getBookFile());
            intent.putExtra("book_href", readPosition.getChapterHref());
            intent.putExtra("book_isKavya", readPosition.getIsKavya());
            startActivity(intent);
        }
    }

    private boolean hasStoragePermission() {
        return EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        email = getIntent().getStringExtra("email");
        adminEmail = getIntent().getStringExtra("adminEmail");

        adapter.addFragment(AllBooksFragment.newInstance(), "All Books");
        adapter.addFragment(MyBooksFragment.newInstance(), "My Books");
        viewPager.setAdapter(adapter);
    }

    private void callActivityFavourite() {
        Intent intent = new Intent(this, FavouriteBookActivity.class);
        startActivityForResult(intent, REQ_FAV);
    }

    private void callActivityBookmark() {
        Intent intent = new Intent(this, BookmarkActivity.class);
        startActivityForResult(intent, REQ_BOOKMARK);
    }

    private void shareApp() {
        Intent localIntent = new Intent("android.intent.action.SEND");
        localIntent.setType("text/plain");
        String app_name = getString(R.string.app_name);
        localIntent.putExtra("android.intent.extra.SUBJECT", app_name);
        localIntent.putExtra(
                "android.intent.extra.TEXT",
                "Hey!\nTry this awesome app is '"
                        + app_name
                        + "' at\nhttps://play.google.com/store/apps/details?id="
                        + getPackageName()
                        + "\nThis app has been shared with you...\n");
        startActivity(Intent.createChooser(localIntent, "Share Via"));
    }

    private void appInfo() {
        startActivity(new Intent(this, AppIntroActivity.class));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        } else {
            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        Log.e("Menu", "onCreateOptionsMenu");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setQueryHint(getResources().getString(R.string.action_search));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        searchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                Log.e("Menu", "onMenuItemActionCollapse " + item.getItemId());
                Log.e("searchAdapter", "clear");
                //searchAdapter.clearData();
                findViewById(R.id.searchLayout).setVisibility(View.GONE);
                menu.findItem(R.id.action_filter).setVisible(true);
                menu.findItem(R.id.action_bookmark).setVisible(true);
                menu.findItem(R.id.action_share).setVisible(true);
                menu.findItem(R.id.action_info).setVisible(true);
                invalidateOptionsMenu();
                return true; // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                Log.e("Menu", "onMenuItemActionExpand " + item.getItemId());
                searchAdapter = new BookSearchAdapter(MainActivity.this, BookTable.getAllBooks(getIntent().getStringExtra("email"),
                        getIntent().getStringExtra("adminEmail")));
                listView = findViewById(R.id.book_list);
                listView.setAdapter(searchAdapter);
                listView.setTextFilterEnabled(false);
                // set up click listener
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        handelListItemClick((Book) searchAdapter.getItem(position));
                    }
                });
                findViewById(R.id.searchLayout).setVisibility(View.VISIBLE);
                menu.findItem(R.id.action_filter).setVisible(false);
                menu.findItem(R.id.action_bookmark).setVisible(false);
                menu.findItem(R.id.action_share).setVisible(false);
                menu.findItem(R.id.action_info).setVisible(false);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_bookmark:
                callActivityBookmark();
                break;
            case R.id.action_share:
                shareApp();
                break;
            case R.id.action_info:
                appInfo();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NotNull final MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_home:
                        clickOnId(R.id.btn_all_books);
                        break;
                    case R.id.nav_my_book:
                        clickOnId(R.id.btn_my_books);
                        break;
                    case R.id.nav_fav:
                        callActivityFavourite();
                        break;
                    case R.id.nav_feedback:
                        startActivity(new Intent(MainActivity.this, FeedbackActivity.class).putExtra("email", getIntent().getStringExtra("email")));
                        break;
                    case R.id.nav_signOut:
                        signOut();
                        break;
                    default:
                        showPage(id);
                        break;
                }
            }
        }, 300);


        return true;
    }

    private void showPage(int position) {
        Log.e("position", "" + position);
        ArrayList<Page> pages = getIntent().getParcelableArrayListExtra("pages");
        Page page = pages.get(position);
        startActivity(new Intent(MainActivity.this, PageActivity.class).putExtra("pageItem", page));
    }

    @Override
    public void onClick(View v) {
        clickOnId(v.getId());
    }

    private void clickOnId(int id) {
        switch (id) {
            case R.id.btn_all_books:
                viewPager.setCurrentItem(0);
                btn_all_book.setTypeface(null, Typeface.BOLD);
                btn_my_book.setTypeface(null, Typeface.NORMAL);
                break;
            case R.id.btn_my_books:
                viewPager.setCurrentItem(1);
                btn_my_book.setTypeface(null, Typeface.BOLD);
                btn_all_book.setTypeface(null, Typeface.NORMAL);
                break;
        }
    }

    @Override
    public void setOnClickItem(Book book) {
        Log.e("Selected Book", book.getTitle());
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("bookItemId", book.getId());
        intent.putExtra("isBookmark", false);
        intent.putExtra("isNotify", false);
        startActivity(intent);
    }

    @Override
    public void setExtraField(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } catch (ActivityNotFoundException e) {
            Log.e("TAG", "Something wrong in url or browser not in device.");
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchAdapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.e("Text", newText);

        // use to enable search view popup text
        /*if (TextUtils.isEmpty(newText)) {
            Log.e("searchAdapter", "clear");
            searchAdapter.clearData();
        } else {
            searchAdapter.getFilter().filter(newText);
        }*/
        searchAdapter.getFilter().filter(newText);

        /*if (searchAdapter.getItemCount()<1)
            findViewById(R.id.search_warning).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.search_warning).setVisibility(View.GONE);*/

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FolioReader.clear();
        // so we can list all files and delete it
        if (share_fileDir.exists()) {
            File[] filenames = share_fileDir.listFiles();
            // loop through each file and delete
            for (File tmpf : filenames) {
                tmpf.delete();
            }
        }
    }

}