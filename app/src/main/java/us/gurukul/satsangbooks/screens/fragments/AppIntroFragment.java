package us.gurukul.satsangbooks.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

import us.gurukul.satsangbooks.screens.R;

public class AppIntroFragment extends Fragment {
    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private static final String ARG_DATA = "data";
    private static final String ARG_IMAGE = "image";
    private int layoutResId;
    private int imageId = R.drawable.splash_bg;
    private String data = "";

    public static AppIntroFragment newInstance(int layoutResId, String data, int imageId) {
        AppIntroFragment appIntroFragment = new AppIntroFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        args.putString(ARG_DATA, data);
        args.putInt(ARG_IMAGE, imageId);
        appIntroFragment.setArguments(args);

        return appIntroFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(ARG_LAYOUT_RES_ID))
                layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);

            if (getArguments().containsKey(ARG_DATA))
                data = getArguments().getString(ARG_DATA);

            if (getArguments().containsKey(ARG_IMAGE))
                imageId = getArguments().getInt(ARG_IMAGE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoutResId, container, false);
        WebView webView = view.findViewById(R.id.webView);
        if (!data.isEmpty())
            webView.loadData(data, "text/html", "UTF-8");

        ImageView imageView = view.findViewById(R.id.imageView);
        imageView.setImageResource(imageId);

        return view;
    }
}
