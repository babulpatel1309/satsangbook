package com.folioreader.model.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DbAdapter {
    private static final String TAG = "DBAdapter";
    private static SQLiteDatabase mDatabase;

    public static void initialize(Context mContext) {
        mDatabase = FolioDatabaseHelper.getInstance(mContext).getMyWritableDatabase();
    }

    public static void terminate() {
        FolioDatabaseHelper.clearInstance();
    }

    public static boolean insert(String table, ContentValues contentValues) {
        return mDatabase.insert(table, null, contentValues) > 0;
    }

    public static boolean update(String table, String key, String value, ContentValues contentValues) {
        return mDatabase.update(table, contentValues, key + "=?", new String[]{value}) > 0;
    }

    public static Cursor getHighLightsForBookId(String bookId) {
        return mDatabase.rawQuery("SELECT * FROM " + HighLightTable.TABLE_NAME + " WHERE " + HighLightTable.COL_BOOK_ID + " = \"" + bookId + "\"", null);
    }

    public boolean deleteAll(String table) {
        return mDatabase.delete(table, null, null) > 0;
    }

    public boolean deleteAll(String table, String whereClause, String[] whereArgs) {
        return mDatabase.delete(table, whereClause + "=?", whereArgs) > 0;
    }

    public Cursor getAll(String table, String[] projection, String selection,
                         String[] selectionArgs, String orderBy) {
        return mDatabase.query(table, projection, selection, selectionArgs, null, null, orderBy);
    }

    public Cursor getAll(String table) {
        return getAll(table, null, null, null, null);
    }

    public Cursor get(String table, long id, String[] projection, String key) throws SQLException {
        return mDatabase.query(table, projection,
                key + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
    }

    public static Cursor getAllByKey(String table, String[] projection, String key, String value) throws SQLException {
        return mDatabase.query(table, projection,
                key + "=?", new String[]{value}, null, null, null, null);
    }

    public Cursor get(String table, long id) throws SQLException {
        return get(table, id, null, FolioDatabaseHelper.KEY_ID);
    }

    public static boolean deleteById(String table, String key, String value) {
        return mDatabase.delete(table, key + "=?", new String[]{value}) > 0;
    }

    public Cursor getMaxId(String tableName, String key) {
        return mDatabase.rawQuery("SELECT MAX(" + key + ") FROM " + tableName, null);
    }

    public static long saveHighLight(ContentValues highlightContentValues) {
        return mDatabase.insert(HighLightTable.TABLE_NAME, null, highlightContentValues);
    }

    public static boolean updateHighLight(ContentValues highlightContentValues, String id) {
        return mDatabase.update(HighLightTable.TABLE_NAME, highlightContentValues, HighLightTable.ID + " = " + id, null) > 0;
    }

    public static Cursor getHighlightsForPageId(String query, String pageId) {
        return mDatabase.rawQuery(query, null);
    }

    public static int getIdForQuery(String query) {
        Cursor c = mDatabase.rawQuery(query, null);

        int id = -1;
        while (c.moveToNext()) {
            id = c.getInt(c.getColumnIndex(HighLightTable.ID));
        }
        c.close();
        return id;
    }

    public static Cursor getHighlightsForId(int id) {
        return mDatabase.rawQuery("SELECT * FROM " + HighLightTable.TABLE_NAME + " WHERE " + HighLightTable.ID + " = \"" + id + "\"", null);
    }

    public static void removeAllHighLight(String file) {
        mDatabase.delete(HighLightTable.TABLE_NAME, HighLightTable.COL_PAGE_ID + " LIKE ?", new String[]{"%" + file + "%"});
    }

    //Bookmark
    static void saveBookmarkChapter(ContentValues highlightContentValues) {
        mDatabase.insert(BookMarkTable.TABLE_NAME, null, highlightContentValues);
    }

    static void removeBookmarkChapter(ContentValues highlightContentValues, String id) {
        mDatabase.delete(BookMarkTable.TABLE_NAME, BookMarkTable.ID + " = " + id, null);
    }

    public static void removeAllBookmarkChapter(String file) {
        mDatabase.delete(BookMarkTable.TABLE_NAME, BookMarkTable.COL_BOOK_FILE + " = \"" + file + "\"", null);
    }

    static Cursor getAllBookmarkChapter(String query) {
        return mDatabase.rawQuery(query, null);
    }

    //------------- Seen Chapter
    public static void saveSeenChapter(ContentValues highlightContentValues) {
        mDatabase.insert(SeenChapterTable.TABLE_NAME, null, highlightContentValues);
    }

    public static void updateSeenChapter(ContentValues highlightContentValues, String id) {
        mDatabase.update(SeenChapterTable.TABLE_NAME, highlightContentValues, SeenChapterTable.ID + " = " + id, null);
    }

    public static void removeAllSeenChapter(String file) {
        mDatabase.delete(SeenChapterTable.TABLE_NAME, SeenChapterTable.COL_BOOK_FILE + " = \"" + file + "\"", null);
    }

    //------------- ReadLocator Chapter
    public static void saveReadLocator(ContentValues highlightContentValues) {
        mDatabase.insert(ReadLocatorTable.TABLE_NAME, null, highlightContentValues);
    }

    public static void updateReadLocator(ContentValues highlightContentValues, String id) {
        mDatabase.update(ReadLocatorTable.TABLE_NAME, highlightContentValues, ReadLocatorTable.ID + " = " + id, null);
    }

    public static Cursor getReadLocatorForQuery(String query) {
        return mDatabase.rawQuery(query, null);
    }

    public static void removeAllReadLocator(String file) {
        mDatabase.delete(ReadLocatorTable.TABLE_NAME, ReadLocatorTable.COL_BOOK_ID + " = \"" + file + "\"", null);
    }

    //------------- Book
    public static Cursor getIdForBookData(String query) {
        return mDatabase.rawQuery(query, null);
    }

    public static void saveBook(ContentValues highlightContentValues) {
        mDatabase.insert(BookTable.TABLE_NAME, null, highlightContentValues);
    }

    public static void updateBook(ContentValues highlightContentValues, String id) {
        mDatabase.update(BookTable.TABLE_NAME, highlightContentValues, BookTable.COL_BOOK_ID + " = " + id, null);
    }

    public static Cursor getBookForQuery(String query) {
        return mDatabase.rawQuery(query, null);
    }

    public static Cursor getFilterBooks(String email, String adminEmail, ArrayList<String> list, int val) {
        String query = "SELECT * FROM " + BookTable.TABLE_NAME;
        String str_or = "";
        for (String s : list) {
            if (str_or.equals("")) {
                str_or += "(" + BookTable.COL_TAGS + " LIKE '%" + s + "%'";
            } else {
                str_or += " OR " + BookTable.COL_TAGS + " LIKE '%" + s + "%'";
            }
        }

        if (!str_or.equals("")) {
            query += " WHERE " + str_or + ")";//
        }

        if (!email.equals(adminEmail)) {
            if (str_or.equals(""))
                query += " WHERE ";
            else
                query += " AND ";

            if (val == 0) {
                query += "(" + BookTable.COL_STATUS + " = \"" + 1 + "\"" + " OR " + BookTable.COL_DOWNLOADED + " = \"" + 1 + "\"" + ")";
                query += " ORDER BY " + BookTable.COL_B_ORDER + " ASC";
            }else
                query += "(" + BookTable.COL_DOWNLOADED + " = \"" + 1 + "\"" + ")";


        }

        Log.e("Query", "Filter-> " + query);
        return mDatabase.rawQuery(query, null);
    }

    public static Cursor getAllBooks(String email, String adminEmail) {
        String query = "SELECT * FROM " + BookTable.TABLE_NAME;
        if (!email.equals(adminEmail))
            query += " WHERE " + BookTable.COL_STATUS + " = \"" + 1 + "\"" + " OR " + BookTable.COL_DOWNLOADED + " = \"" + 1 + "\"" + "ORDER BY " + BookTable.COL_B_ORDER + " ASC";
        Log.e("Query", query);
        return mDatabase.rawQuery(query, null);
    }

    public static Cursor getMyBooks() {
        String query = "SELECT * FROM " + BookTable.TABLE_NAME + " WHERE " + BookTable.COL_DOWNLOADED + " = \"" + 1 + "\"";
        return mDatabase.rawQuery(query, null);
    }

    public static Cursor getFavBooks() {
        String query = "SELECT * FROM " + BookTable.TABLE_NAME + " WHERE " + BookTable.COL_FAV + " = \"" + 1 + "\"";
        return mDatabase.rawQuery(query, null);
    }

    public static int getIdForBook(String query) {
        Cursor c = mDatabase.rawQuery(query, null);

        int id = -1;
        while (c.moveToNext()) {
            id = c.getInt(c.getColumnIndex(BookTable.COL_BOOK_ID));
        }
        c.close();
        return id;
    }

    public static void removeBook(String id) {
        mDatabase.delete(BookTable.TABLE_NAME, BookTable.COL_BOOK_ID + " = " + id, null);
    }


}
