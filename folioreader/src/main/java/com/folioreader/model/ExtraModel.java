package com.folioreader.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ExtraModel implements Parcelable {
    @SerializedName("title")
    private String title;
    @SerializedName("value")
    private String value;

    public ExtraModel(String title, String value) {
        this.title = title;
        this.value = value;
    }

    private ExtraModel(Parcel in) {
        title = in.readString();
        value = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ExtraModel> CREATOR = new Creator<ExtraModel>() {
        @Override
        public ExtraModel createFromParcel(Parcel in) {
            return new ExtraModel(in);
        }

        @Override
        public ExtraModel[] newArray(int size) {
            return new ExtraModel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

