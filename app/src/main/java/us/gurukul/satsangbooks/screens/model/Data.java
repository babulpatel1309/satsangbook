
package us.gurukul.satsangbooks.screens.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("grand_father_name")
    @Expose
    private String grandFatherName;
    @SerializedName("last_name_gu")
    @Expose
    private String lastNameGu;
    @SerializedName("first_name_gu")
    @Expose
    private String firstNameGu;
    @SerializedName("middle_name_gu")
    @Expose
    private String middleNameGu;
    @SerializedName("grand_father_name_gu")
    @Expose
    private String grandFatherNameGu;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("birth_date")
    @Expose
    private Integer birthDate;
    @SerializedName("marriage_date")
    @Expose
    private Object marriageDate;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("education2")
    @Expose
    private Object education2;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("job_title2")
    @Expose
    private Object jobTitle2;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("visiting_card")
    @Expose
    private Object visitingCard;
    @SerializedName("blood_group_id")
    @Expose
    private Integer bloodGroupId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("is_gurukul_student")
    @Expose
    private Integer isGurukulStudent;
    @SerializedName("reference_id")
    @Expose
    private String referenceId;
    @SerializedName("skill_id")
    @Expose
    private String skillId;
    @SerializedName("role_id")
    @Expose
    private Integer roleId;
    @SerializedName("country_id")
    @Expose
    private Integer countryId;
    @SerializedName("auth_key")
    @Expose
    private Object authKey;
    @SerializedName("password_hash")
    @Expose
    private Object passwordHash;
    @SerializedName("password_reset_token")
    @Expose
    private Object passwordResetToken;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("is_deleted")
    @Expose
    private Integer isDeleted;
    @SerializedName("is_deceased")
    @Expose
    private Integer isDeceased;
    @SerializedName("deceased_date")
    @Expose
    private Object deceasedDate;
    @SerializedName("gurukul_id")
    @Expose
    private Integer gurukulId;
    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("trust_access")
    @Expose
    private Object trustAccess;
    @SerializedName("section_access")
    @Expose
    private String sectionAccess;
    @SerializedName("purpose_access")
    @Expose
    private String purposeAccess;
    @SerializedName("sub_purpose_access")
    @Expose
    private String subPurposeAccess;
    @SerializedName("donations_save_and_sms")
    @Expose
    private Integer donationsSaveAndSms;
    @SerializedName("donations_save_and_print")
    @Expose
    private Integer donationsSaveAndPrint;
    @SerializedName("donations_save_and_new")
    @Expose
    private Integer donationsSaveAndNew;
    @SerializedName("magazine_save_and_sms")
    @Expose
    private Integer magazineSaveAndSms;
    @SerializedName("allow_free")
    @Expose
    private Integer allowFree;
    @SerializedName("allow_branch")
    @Expose
    private Integer allowBranch;
    @SerializedName("assigned_to")
    @Expose
    private Integer assignedTo;
    @SerializedName("lead_source_id")
    @Expose
    private Integer leadSourceId;
    @SerializedName("assigned_sabha")
    @Expose
    private String assignedSabha;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("created_at")
    @Expose
    private Integer createdAt;
    @SerializedName("updated_at")
    @Expose
    private Integer updatedAt;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("magazine_save_and_print")
    @Expose
    private Integer magazineSaveAndPrint;
    @SerializedName("magazine_save_and_new")
    @Expose
    private Integer magazineSaveAndNew;
    @SerializedName("donation_receipt_change")
    @Expose
    private Integer donationReceiptChange;
    @SerializedName("donation_date_change")
    @Expose
    private Integer donationDateChange;
    @SerializedName("magazine_receipt_change")
    @Expose
    private Integer magazineReceiptChange;
    @SerializedName("magazine_date_change")
    @Expose
    private Integer magazineDateChange;
    @SerializedName("autologout")
    @Expose
    private Object autologout;
    @SerializedName("login_attempt")
    @Expose
    private Integer loginAttempt;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("otp_required")
    @Expose
    private Integer otpRequired;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("finger")
    @Expose
    private Object finger;
    @SerializedName("finger_iso")
    @Expose
    private Object fingerIso;
    @SerializedName("today_inserted")
    @Expose
    private Integer todayInserted;
    @SerializedName("logout_time")
    @Expose
    private Object logoutTime;
    @SerializedName("mac_address")
    @Expose
    private String macAddress;
    @SerializedName("short_date")
    @Expose
    private Integer shortDate;
    @SerializedName("isForeigner")
    @Expose
    private Integer isForeigner;
    @SerializedName("invite_date")
    @Expose
    private Integer inviteDate;
    @SerializedName("last_login")
    @Expose
    private Integer lastLogin;
    @SerializedName("last_update")
    @Expose
    private Integer lastUpdate;
    @SerializedName("family_id")
    @Expose
    private Integer familyId;
    @SerializedName("last_approved")
    @Expose
    private Integer lastApproved;
    @SerializedName("approve_by")
    @Expose
    private Integer approveBy;
    @SerializedName("otp_date")
    @Expose
    private Integer otpDate;
    @SerializedName("otp_counter")
    @Expose
    private Integer otpCounter;
    @SerializedName("otp_login_counter")
    @Expose
    private Integer otpLoginCounter;
    @SerializedName("resend_otp_counter")
    @Expose
    private Integer resendOtpCounter;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getGrandFatherName() {
        return grandFatherName;
    }

    public void setGrandFatherName(String grandFatherName) {
        this.grandFatherName = grandFatherName;
    }

    public String getLastNameGu() {
        return lastNameGu;
    }

    public void setLastNameGu(String lastNameGu) {
        this.lastNameGu = lastNameGu;
    }

    public String getFirstNameGu() {
        return firstNameGu;
    }

    public void setFirstNameGu(String firstNameGu) {
        this.firstNameGu = firstNameGu;
    }

    public String getMiddleNameGu() {
        return middleNameGu;
    }

    public void setMiddleNameGu(String middleNameGu) {
        this.middleNameGu = middleNameGu;
    }

    public String getGrandFatherNameGu() {
        return grandFatherNameGu;
    }

    public void setGrandFatherNameGu(String grandFatherNameGu) {
        this.grandFatherNameGu = grandFatherNameGu;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Integer birthDate) {
        this.birthDate = birthDate;
    }

    public Object getMarriageDate() {
        return marriageDate;
    }

    public void setMarriageDate(Object marriageDate) {
        this.marriageDate = marriageDate;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public Object getEducation2() {
        return education2;
    }

    public void setEducation2(Object education2) {
        this.education2 = education2;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Object getJobTitle2() {
        return jobTitle2;
    }

    public void setJobTitle2(Object jobTitle2) {
        this.jobTitle2 = jobTitle2;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Object getVisitingCard() {
        return visitingCard;
    }

    public void setVisitingCard(Object visitingCard) {
        this.visitingCard = visitingCard;
    }

    public Integer getBloodGroupId() {
        return bloodGroupId;
    }

    public void setBloodGroupId(Integer bloodGroupId) {
        this.bloodGroupId = bloodGroupId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getIsGurukulStudent() {
        return isGurukulStudent;
    }

    public void setIsGurukulStudent(Integer isGurukulStudent) {
        this.isGurukulStudent = isGurukulStudent;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getSkillId() {
        return skillId;
    }

    public void setSkillId(String skillId) {
        this.skillId = skillId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Object getAuthKey() {
        return authKey;
    }

    public void setAuthKey(Object authKey) {
        this.authKey = authKey;
    }

    public Object getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(Object passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Object getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(Object passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeceased() {
        return isDeceased;
    }

    public void setIsDeceased(Integer isDeceased) {
        this.isDeceased = isDeceased;
    }

    public Object getDeceasedDate() {
        return deceasedDate;
    }

    public void setDeceasedDate(Object deceasedDate) {
        this.deceasedDate = deceasedDate;
    }

    public Integer getGurukulId() {
        return gurukulId;
    }

    public void setGurukulId(Integer gurukulId) {
        this.gurukulId = gurukulId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Object getTrustAccess() {
        return trustAccess;
    }

    public void setTrustAccess(Object trustAccess) {
        this.trustAccess = trustAccess;
    }

    public String getSectionAccess() {
        return sectionAccess;
    }

    public void setSectionAccess(String sectionAccess) {
        this.sectionAccess = sectionAccess;
    }

    public String getPurposeAccess() {
        return purposeAccess;
    }

    public void setPurposeAccess(String purposeAccess) {
        this.purposeAccess = purposeAccess;
    }

    public String getSubPurposeAccess() {
        return subPurposeAccess;
    }

    public void setSubPurposeAccess(String subPurposeAccess) {
        this.subPurposeAccess = subPurposeAccess;
    }

    public Integer getDonationsSaveAndSms() {
        return donationsSaveAndSms;
    }

    public void setDonationsSaveAndSms(Integer donationsSaveAndSms) {
        this.donationsSaveAndSms = donationsSaveAndSms;
    }

    public Integer getDonationsSaveAndPrint() {
        return donationsSaveAndPrint;
    }

    public void setDonationsSaveAndPrint(Integer donationsSaveAndPrint) {
        this.donationsSaveAndPrint = donationsSaveAndPrint;
    }

    public Integer getDonationsSaveAndNew() {
        return donationsSaveAndNew;
    }

    public void setDonationsSaveAndNew(Integer donationsSaveAndNew) {
        this.donationsSaveAndNew = donationsSaveAndNew;
    }

    public Integer getMagazineSaveAndSms() {
        return magazineSaveAndSms;
    }

    public void setMagazineSaveAndSms(Integer magazineSaveAndSms) {
        this.magazineSaveAndSms = magazineSaveAndSms;
    }

    public Integer getAllowFree() {
        return allowFree;
    }

    public void setAllowFree(Integer allowFree) {
        this.allowFree = allowFree;
    }

    public Integer getAllowBranch() {
        return allowBranch;
    }

    public void setAllowBranch(Integer allowBranch) {
        this.allowBranch = allowBranch;
    }

    public Integer getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(Integer assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Integer getLeadSourceId() {
        return leadSourceId;
    }

    public void setLeadSourceId(Integer leadSourceId) {
        this.leadSourceId = leadSourceId;
    }

    public String getAssignedSabha() {
        return assignedSabha;
    }

    public void setAssignedSabha(String assignedSabha) {
        this.assignedSabha = assignedSabha;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getMagazineSaveAndPrint() {
        return magazineSaveAndPrint;
    }

    public void setMagazineSaveAndPrint(Integer magazineSaveAndPrint) {
        this.magazineSaveAndPrint = magazineSaveAndPrint;
    }

    public Integer getMagazineSaveAndNew() {
        return magazineSaveAndNew;
    }

    public void setMagazineSaveAndNew(Integer magazineSaveAndNew) {
        this.magazineSaveAndNew = magazineSaveAndNew;
    }

    public Integer getDonationReceiptChange() {
        return donationReceiptChange;
    }

    public void setDonationReceiptChange(Integer donationReceiptChange) {
        this.donationReceiptChange = donationReceiptChange;
    }

    public Integer getDonationDateChange() {
        return donationDateChange;
    }

    public void setDonationDateChange(Integer donationDateChange) {
        this.donationDateChange = donationDateChange;
    }

    public Integer getMagazineReceiptChange() {
        return magazineReceiptChange;
    }

    public void setMagazineReceiptChange(Integer magazineReceiptChange) {
        this.magazineReceiptChange = magazineReceiptChange;
    }

    public Integer getMagazineDateChange() {
        return magazineDateChange;
    }

    public void setMagazineDateChange(Integer magazineDateChange) {
        this.magazineDateChange = magazineDateChange;
    }

    public Object getAutologout() {
        return autologout;
    }

    public void setAutologout(Object autologout) {
        this.autologout = autologout;
    }

    public Integer getLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(Integer loginAttempt) {
        this.loginAttempt = loginAttempt;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getOtpRequired() {
        return otpRequired;
    }

    public void setOtpRequired(Integer otpRequired) {
        this.otpRequired = otpRequired;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Object getFinger() {
        return finger;
    }

    public void setFinger(Object finger) {
        this.finger = finger;
    }

    public Object getFingerIso() {
        return fingerIso;
    }

    public void setFingerIso(Object fingerIso) {
        this.fingerIso = fingerIso;
    }

    public Integer getTodayInserted() {
        return todayInserted;
    }

    public void setTodayInserted(Integer todayInserted) {
        this.todayInserted = todayInserted;
    }

    public Object getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Object logoutTime) {
        this.logoutTime = logoutTime;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Integer getShortDate() {
        return shortDate;
    }

    public void setShortDate(Integer shortDate) {
        this.shortDate = shortDate;
    }

    public Integer getIsForeigner() {
        return isForeigner;
    }

    public void setIsForeigner(Integer isForeigner) {
        this.isForeigner = isForeigner;
    }

    public Integer getInviteDate() {
        return inviteDate;
    }

    public void setInviteDate(Integer inviteDate) {
        this.inviteDate = inviteDate;
    }

    public Integer getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Integer lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Integer lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getFamilyId() {
        return familyId;
    }

    public void setFamilyId(Integer familyId) {
        this.familyId = familyId;
    }

    public Integer getLastApproved() {
        return lastApproved;
    }

    public void setLastApproved(Integer lastApproved) {
        this.lastApproved = lastApproved;
    }

    public Integer getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(Integer approveBy) {
        this.approveBy = approveBy;
    }

    public Integer getOtpDate() {
        return otpDate;
    }

    public void setOtpDate(Integer otpDate) {
        this.otpDate = otpDate;
    }

    public Integer getOtpCounter() {
        return otpCounter;
    }

    public void setOtpCounter(Integer otpCounter) {
        this.otpCounter = otpCounter;
    }

    public Integer getOtpLoginCounter() {
        return otpLoginCounter;
    }

    public void setOtpLoginCounter(Integer otpLoginCounter) {
        this.otpLoginCounter = otpLoginCounter;
    }

    public Integer getResendOtpCounter() {
        return resendOtpCounter;
    }

    public void setResendOtpCounter(Integer resendOtpCounter) {
        this.resendOtpCounter = resendOtpCounter;
    }

}
